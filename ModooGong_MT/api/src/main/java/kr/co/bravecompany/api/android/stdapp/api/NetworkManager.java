package kr.co.bravecompany.api.android.stdapp.api;

import android.content.Context;
import android.os.Build;
import android.os.Looper;

import com.facebook.stetho.okhttp3.StethoInterceptor;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.File;
import java.net.CookieManager;
import java.net.CookiePolicy;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.concurrent.TimeUnit;

import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import kr.co.bravecompany.api.android.stdapp.api.utils.APIManager;
import okhttp3.Cache;
import okhttp3.JavaNetCookieJar;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;


public class NetworkManager {
    public static boolean isDebug = false;
    protected static Context mContext;

    public static final String CHARSET = "UTF-8";

    private static final int DEFAULT_CACHE_SIZE = 50 * 1024 * 1024;
    private static final String DEFAULT_CACHE_DIR = "miniapp";
    protected OkHttpClient mClient;

    protected NetworkManager() {
        OkHttpClient.Builder builder = new OkHttpClient.Builder();
        CookieManager cookieManager = new CookieManager(new PersistentCookieStore(mContext), CookiePolicy.ACCEPT_ALL);
        builder.cookieJar(new JavaNetCookieJar(cookieManager));

        File dir = new File(mContext.getExternalCacheDir(), DEFAULT_CACHE_DIR);
        if (!dir.exists()) {
            dir.mkdirs();
        }
        builder.cache(new Cache(dir, DEFAULT_CACHE_SIZE));

        builder.connectTimeout(30, TimeUnit.SECONDS);
        builder.readTimeout(30, TimeUnit.SECONDS);
        builder.writeTimeout(30, TimeUnit.SECONDS);

        if (isDebug) {
            HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
            logging.setLevel(HttpLoggingInterceptor.Level.BODY);
            builder.addInterceptor(logging);

            builder.addNetworkInterceptor(new StethoInterceptor());
        }

        //Lee Android 5 (API 21), HTTPS 오류에 대한 대응

        try {
            TrustManager[] trustAllCerts = getTrustAllCerts();

            SSLContext sc = SSLContext.getInstance("SSL");
            sc.init(null, trustAllCerts, new java.security.SecureRandom());

            builder.sslSocketFactory(sc.getSocketFactory(), (X509TrustManager) trustAllCerts[0] );

        } catch (Exception e) {
            e.printStackTrace();
        }

        mClient = builder.build();

        //!Lee Android 5 (API 21)
        // Make and set trust manager(s) to solve SSLHandshakeException exception.
        // The exception occurs from Kit Kat or lower versions of Android.
        /*
        if ((Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP)) {
            try {
                TrustManager[] trustAllCerts = getTrustAllCerts();
                SSLContext sc = SSLContext.getInstance("TLS");
                sc.init(null, trustAllCerts, new java.security.SecureRandom());
                mClient.sslSocketFactory().setSslSocketFactory(sc.getSocketFactory());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        */
    }

    /**
     * Get trust manager to permit all of certifications.
     * @return TrustManager array with one X509TrustManager element
     */
    private static TrustManager[] getTrustAllCerts() {
        return new TrustManager[] { new X509TrustManager() {
            @Override
            public void checkClientTrusted(X509Certificate[] chain, String authType)
                    throws CertificateException {
            }

            @Override
            public void checkServerTrusted(X509Certificate[] chain, String authType)
                    throws CertificateException {
            }

            public X509Certificate[] getAcceptedIssuers() {
                return new X509Certificate[] {};
            }
        }};
    }

    public static void init(Context context, boolean debug, String host){
        if (context == null) {
            throw new IllegalArgumentException("Non-null context required.");
        }
        if (host == null) {
            throw new IllegalArgumentException("Non-null host required.");
        }
        mContext = context;
        isDebug = debug;
        APIManager.isDebug = debug;
        APIManager.host = host;
    }

    protected NetworkHandler mHandler = new NetworkHandler(Looper.getMainLooper());

    protected Gson gson = new GsonBuilder().serializeNulls().create();

    protected APIManager api = APIManager.getInstance();

}

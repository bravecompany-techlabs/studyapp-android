package kr.co.bravecompany.player.android.stdapp.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.kollus.sdk.media.content.KollusBookmark;

import java.util.ArrayList;
import java.util.List;

import kr.co.bravecompany.player.android.stdapp.R;
import kr.co.bravecompany.player.android.stdapp.viewholder.BookmarkItemViewHolder;
import kr.co.bravecompany.player.android.stdapp.viewholder.OnItemClickListener;
import kr.co.bravecompany.player.android.stdapp.data.BookmarkData;


/**
 * Created by BraveCompany on 2017. 5. 22..
 */

public class BookmarkAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<BookmarkData> items = new ArrayList<BookmarkData>();

    public boolean isDeleteMode = false;

    public BookmarkData getItem(int position){
        return items.get(position);
    }

    public int getItemWithTime(int timeMilli){
        for(int i=0; i<items.size(); i++){
            KollusBookmark bookmark = items.get(i).getBookmark();
            int itemTimeSec = bookmark.getTime()/1000;
            int addTimeSec = timeMilli/1000;
            if(itemTimeSec == addTimeSec && bookmark.getLevel() == KollusBookmark.USER_LEVEL) {
                return i;
            }
        }
        return -1;
    }

    public int getAddIndex(int timeMilli) {
        int index = 0;
        for(BookmarkData item : items) {
            KollusBookmark bookmark = item.getBookmark();
            int iterTimeSec = bookmark.getTime()/1000;
            int addTimeSec = timeMilli/1000;
            if(iterTimeSec > addTimeSec) {
                break;
            }
            index++;
        }
        return index;
    }

    public void clear() {
        items.clear();
        notifyDataSetChanged();
    }

    public void remove(int index) {
        items.remove(index);
        notifyDataSetChanged();
    }

    public void add(BookmarkData item) {
        items.add(item);
        notifyDataSetChanged();
    }

    public void add(int index, BookmarkData item) {
        if(index == -1){
            return;
        }
        items.add(index, item);
        notifyDataSetChanged();
    }

    public void addAll(List<BookmarkData> items) {
        this.items.addAll(items);
        notifyDataSetChanged();
    }

    private OnItemClickListener mListener;
    public void setOnBookmarkItemClickListener(OnItemClickListener listener) {
        mListener = listener;
    }

    private OnItemClickListener mDeleteListener;
    public void setOnDeleteClickListener(OnItemClickListener listener) {
        mDeleteListener = listener;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.view_bookmark_item, parent, false);
        return new BookmarkItemViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        BookmarkItemViewHolder h = (BookmarkItemViewHolder)holder;
        h.updateDeleteMode(isDeleteMode);
        h.setBookmarkItem(items.get(position));
        h.setOnItemClickListener(mListener);
        h.setOnDeleteClickListener(mDeleteListener);
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public int getUserLevelCount(){
        int count = 0;
        for(BookmarkData item : items) {
            KollusBookmark bookmark = item.getBookmark();
            if(bookmark.getLevel() == KollusBookmark.USER_LEVEL)
                count++;
        }
        return count;
    }

    public void updateDeleteMode(boolean isDeleteMode){
        this.isDeleteMode = isDeleteMode;
        notifyDataSetChanged();
    }
}

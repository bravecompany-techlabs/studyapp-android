package kr.co.bravecompany.player.android.stdapp.video;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.BitmapRegionDecoder;
import android.graphics.Rect;
import android.graphics.drawable.GradientDrawable;
import android.media.AudioManager;
import android.net.Uri;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.view.KeyEvent;
import android.view.Surface;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.RelativeLayout;
import android.widget.SeekBar;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.kollus.sdk.media.KollusPlayerBookmarkListener;
import com.kollus.sdk.media.KollusPlayerContentMode;
import com.kollus.sdk.media.KollusPlayerDRMListener;
import com.kollus.sdk.media.KollusPlayerLMSListener;
import com.kollus.sdk.media.KollusStorage;
import com.kollus.sdk.media.MediaPlayerBase;
import com.kollus.sdk.media.MediaPlayerFactory;
import com.kollus.sdk.media.VideoWindowImpl;
import com.kollus.sdk.media.VideoWindowListener;
import com.kollus.sdk.media.content.KollusBookmark;
import com.kollus.sdk.media.content.KollusContent;
import com.kollus.sdk.media.util.ErrorCodes;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import kr.co.bravecompany.player.android.stdapp.R;
import kr.co.bravecompany.player.android.stdapp.adapter.BookmarkAdapter;
import kr.co.bravecompany.player.android.stdapp.config.KollusConstant;
import kr.co.bravecompany.player.android.stdapp.manager.PlayerPropertyManager;
import kr.co.bravecompany.player.android.stdapp.utils.BraveUtils;
import kr.co.bravecompany.player.android.stdapp.utils.log;
import kr.co.bravecompany.player.android.stdapp.viewholder.OnItemClickListener;
import kr.co.bravecompany.player.android.stdapp.data.BookmarkData;

import static com.kollus.sdk.media.MediaPlayerBase.MEDIA_ERROR_UNKNOWN;

/**
 * Created by BraveCompany on 2017. 3. 28..
 */

public class VideoPlayer implements OnVideoPlayerListener {
    private static final int STATE_ERROR              = -1;
    private static final int STATE_IDLE               = 0;
    private static final int STATE_PREPARING          = 1;
    private static final int STATE_PREPARED           = 2;
    private static final int STATE_PLAYING            = 3;
    private static final int STATE_PAUSED             = 4;
    private static final int STATE_PLAYBACK_COMPLETED = 5;
    private static final int STATE_BUFFERING          = 6;
    private static final int STATE_BUFFEREND          = 7;

    private int mCurrentState = STATE_IDLE;

    private Activity mContext;
    private View mRootView;

    private KollusStorage mStorage;

    private VideoView mVideoView;
    private SurfaceView mSurfaceView;

    private VideoWindowImpl mVideoWindowImpl;
    private MediaPlayerBase mMediaPlayer;
    private MediaPlayerFactory mMediaPlayerFactory;

    private String mPlayTitle;
    private Uri mUri;
    private String mMediaContentKey;

    private KollusContent mKollusContent;

    private BitmapRegionDecoder mScreenShot = null;
    private BitmapFactory.Options mScreenShotOption;
    private int mScreenShotWidth;
    private int mScreenShotHeight;
    private int mScreenShotCount;
    private float mScreenShotInterval;

    private boolean mDragging = false;
    private float mPlayingRate = 1.0f;

    private VideoControllerView mVideoControllerView;

    private int mSeekScreenShotTimeMs = -1;

    private boolean isPaused = false;

    private AudioManager mAudioManager;
    private static final int BRIGHTNESS_MIN = 15;
    private static final int BRIGHTNESS_MAX = 255;
    private static final int BRIGHTNESS_UNIT = 24;
    private int mBrightness = BRIGHTNESS_MAX;

    private boolean	mVideoSurfaceReady = false;
    private boolean mFirstSurfaceChanged = false;

    private int mSeekWhenPrepared = 0;

    private BookmarkAdapter mBookmarkAdapter;


    public VideoPlayer(Activity context, View rootView, String playTitle, Uri uri, String mediaContentKey) {
        mContext = context;
        mRootView = rootView;
        mPlayTitle = playTitle;
        mUri = uri;
        mMediaContentKey = mediaContentKey;

        mAudioManager = (AudioManager)context.getSystemService(Context.AUDIO_SERVICE);

        initLayout();
        initListener();
        initData();
    }

    private void initLayout() {
        mStorage = KollusStorage.getInstance(mContext, true);

        //Surface View
        RelativeLayout layout = (RelativeLayout)mRootView.findViewById(R.id.surface_view);
        mVideoView = new VideoView(mContext.getApplicationContext());
        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(
                RelativeLayout.LayoutParams.MATCH_PARENT,
                RelativeLayout.LayoutParams.MATCH_PARENT);
        SurfaceView surfaceView = new SurfaceView(mContext.getApplicationContext());
        mVideoView.addView(surfaceView, params);
        mVideoView.initVideoView(surfaceView);
        layout.addView(mVideoView, params);

        mSurfaceView = surfaceView;

        mVideoWindowImpl = new VideoWindowImpl(mSurfaceView);
        mVideoWindowImpl.init();

        mKollusContent = new KollusContent();

        mVideoControllerView = new VideoControllerView(mContext, mRootView);

        mBookmarkAdapter = new BookmarkAdapter();
        mVideoControllerView.setBookmarkAdapter(mBookmarkAdapter);
    }

    private void initListener() {
        mVideoWindowImpl.setListener(mVideoWindowListener);
        mVideoControllerView.setOnSeekBarChangeListener(mOnSeekBarChangeListener);
        mVideoControllerView.setOnVolumeSeekBarChangeListener(mOnVolumeSeekBarChangeListener);
        mVideoControllerView.setOnVideoPlayerListener(this);
        mBookmarkAdapter.setOnBookmarkItemClickListener(mBookmarkItemClickListener);
        mBookmarkAdapter.setOnDeleteClickListener(mBookmarkDeleteLisener);
    }

    private void initData(){
        mVideoControllerView.startLoading();
        mVideoControllerView.startBookLoading();

        //initScreenRotateLock();
        initScreenRotate();
        initFullScreen();
        initVolumeSeek();
        initBrightness();
    }

    private void openVideo() {
        if ((mUri == null && (mMediaContentKey == null || mMediaContentKey.isEmpty())) || !mVideoSurfaceReady) {
            // not ready for playback just yet, will try again later
            return;
        }

        Intent i = new Intent("com.android.music.musicservicecommand");
        i.putExtra("command", "pause");
        mContext.sendBroadcast(i);

        // we shouldn't clear the target state, because somebody might have
        // called start() previously
        mMediaPlayerFactory = new MediaPlayerFactory(KollusConstant.PORT, new MediaPlayerFactory.OnCreateListener() {
            @Override
            public void onCreate(MediaPlayerBase mp) {
                try {
                    log.d("createMediaPlayer >>> received onCreate");
                    mMediaPlayer = mp;
                    mMediaPlayer.setOnPreparedListener(mPreparedListener);
                    mMediaPlayer.setOnVideoSizeChangedListener(mSizeChangedListener);
                    mMediaPlayer.setOnCompletionListener(mCompletionListener);
                    mMediaPlayer.setOnErrorListener(mErrorListener);
                    mMediaPlayer.setOnInfoListener(mInfoListener);
                    mMediaPlayer.setKollusPlayerBookmarkListener(mKollusPlayerBookmarkListener);
                    mMediaPlayer.setKollusPlayerLMSListener(mKollusPlayerLMSListener);
                    mMediaPlayer.setOnExternalDisplayDetectListener(mOnExternalDisplayDetectListener);
                    log.i("Player Version:"+mMediaPlayer.getVersion());

                    if(mSurfaceView != null)
                        mMediaPlayer.setDisplay(mSurfaceView.getHolder());

                    mMediaPlayer.setScreenOnWhilePlaying(true);
                    mMediaPlayer.prepareAsync();
                    // we don't set the target state here either, but preserve the
                    // target state that was there before.
                    mCurrentState = STATE_PREPARING;
                } catch (IllegalArgumentException ex) {
                    log.w("Unable to open content: " + mUri+":"+ex);
                    mErrorListener.onError(mMediaPlayer, MEDIA_ERROR_UNKNOWN, 0);
                }
            }

            @Override
            public void onError(int nErrorCode) {
                mErrorListener.onError(mMediaPlayer, MEDIA_ERROR_UNKNOWN, nErrorCode);
            }
        });

        if(mUri == null) {
            mMediaPlayerFactory.createMediaPlayer(mContext, mVideoWindowImpl,
                    MediaPlayerBase.RENDER_MODE.RENDER_MODEL, true, mStorage, mMediaContentKey);
            log.d("mMediaContentKey: " + mMediaContentKey);
        }else {
            mMediaPlayerFactory.createMediaPlayer(mContext, mVideoWindowImpl,
                    MediaPlayerBase.RENDER_MODE.RENDER_MODEL, true, mStorage, mUri);
            log.d("mUri: " + mUri);
        }
    }

    MediaPlayerBase.OnPreparedListener mPreparedListener = new MediaPlayerBase.OnPreparedListener() {
        @Override
        public void onPrepared(MediaPlayerBase mp) {
            if(mCurrentState == STATE_ERROR)
                return;
            mCurrentState = STATE_PREPARED;

            handleKollusContent();
            mVideoControllerView.stopLoading();
            if(!mVideoControllerView.isSelectedControlLayer()) {
                toggleControlView();
            }

            if(mMediaPlayer.getPlayAt() > 0) {
                if(mKollusContent.getForceNScreen()) {
                    seekToExact(mMediaPlayer.getPlayAt());
                    onPlay(true);
                }
                else {
                    if(PlayerPropertyManager.getInstance(mContext).isAutoContinuePlay()){
                        seekToExact(mMediaPlayer.getPlayAt());
                        onPlay(true);
                    }else{
                        showResumeDialog(mMediaPlayer.getPlayAt());
                    }
                }
            }
            else {
                onPlay(true);
            }
            //mHandler.post(mProgressChecker);
        }
    };

    private void handleKollusContent() {
        if(mMediaPlayer.getKollusContent(mKollusContent)) {

            String title;
            if(mPlayTitle != null && mPlayTitle.length() != 0){
                title = mPlayTitle;
            }else{
                String cource = mKollusContent.getCourse();
                String subcource = mKollusContent.getSubCourse();

                if(cource != null && cource.length() > 0) {
                    if(subcource != null && subcource.length() > 0)
                        title = cource+"("+subcource+")";
                    else
                        title  = cource;
                }
                else
                    title = subcource;
            }
            mVideoControllerView.setTitle(title);

            String screenShotPath = mKollusContent.getScreenShotPath();
            log.d("screenShotPath - " + screenShotPath);
            int end = screenShotPath.lastIndexOf('.');
            int start = screenShotPath.lastIndexOf('.', end-1);

            if(start < end) {
                String info = screenShotPath.substring(start+1, end);
                Scanner sc = new Scanner(info);
                sc.useDelimiter("x");
                mScreenShotWidth = sc.nextInt();
                mScreenShotHeight = sc.nextInt();
                mScreenShotCount = sc.nextInt();
                mScreenShotInterval = (float) (mKollusContent.getDuration()/mScreenShotCount/1000.);
                sc.close();

                try {
                    mScreenShot = BitmapRegionDecoder.newInstance(screenShotPath, true);
                    if(mScreenShot != null) {
                        log.d(String.format("ScreenShot width %d height %d",
                                mScreenShot.getWidth(), mScreenShot.getHeight()));
                        mScreenShotOption = new BitmapFactory.Options();
                    }
                    else
                        log.e("ScreenShot null");
                } catch (Exception e) {
                    log.e("ScreenShot Exception");
                    log.e(log.getStackTraceString(e));
                } catch (OutOfMemoryError e) {
                    log.e("ScreenShot OutOfMemoryError");
                    log.e(log.getStackTraceString(e));
                }


                log.d(String.format("ScreenShot w %d h %d count %d duration %d",
                        mScreenShotWidth, mScreenShotHeight, mScreenShotCount, mKollusContent.getDuration()));
            }
            setupPlayer();
        }
    }

    private Handler mHandler = new Handler();
    private final Runnable mProgressChecker = new Runnable() {
        @Override
        public void run() {
            if(isInPlaybackState()) {
                int pos = mMediaPlayer.getCurrentPosition();
                if(!mDragging) {
                    mVideoControllerView.setSeekBarProgress(pos);
                }
                mHandler.postDelayed(mProgressChecker, 1000L - (pos % 1000));
                log.d("mProgressChecker pos - " + pos);
            }
        }
    };

    private boolean isInPlaybackState() {
        return (mMediaPlayer != null &&
                mCurrentState != STATE_ERROR &&
                mCurrentState != STATE_IDLE &&
                mCurrentState != STATE_PREPARING);
    }

    private void showResumeDialog(final int playAt) {
        String title = mContext.getString(R.string.kollus_resume_dialog_title);
        String message = String.format(mContext.getString(R.string.kollus_resume_dialog_msg),
                BraveUtils.formattingTime(playAt/1000));
        String positive = mContext.getString(R.string.kollus_resume_dialog_ok);
        String negative = mContext.getString(R.string.kollus_resume_dialog_cancel);

        BraveUtils.showAlertDialog(mContext, title, message, positive, negative,
                new MaterialDialog.SingleButtonCallback(){
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        seekToExact(playAt);
                        onPlay(true);
                    }
                },
                new MaterialDialog.SingleButtonCallback(){
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        seekToExact(0);
                        onPlay(true);
                    }
                });
    }

    // =============================================================================
    // Control Play
    // =============================================================================

    private void setupPlayer(){
        if(!isInPlaybackState()){
            return;
        }
        mVideoControllerView.setSeekBarMax(mMediaPlayer.getDuration());
        mMediaPlayer.setVolumeLevel(mAudioManager.getStreamVolume(AudioManager.STREAM_MUSIC));
        onMute(mKollusContent.getMute());
        //setPlayingRate(mPlayingRate);
        initPlayingRate();
    }

    private void seekTo(int msec){
        if(!isPaused && isInPlaybackState()){
            mMediaPlayer.seekTo(msec);
            mVideoControllerView.setSeekBarProgress(msec);
            mSeekWhenPrepared = 0;
        }else{
            mSeekWhenPrepared = msec;
        }
    }

    private void seekToExact(int msec){
        if(!isPaused && isInPlaybackState()){
            mMediaPlayer.seekToExact(msec);
            mVideoControllerView.setSeekBarProgress(msec);
            mSeekWhenPrepared = 0;
        }else{
            mSeekWhenPrepared = msec;
        }
    }

    @Override
    public void onPlay(){
        onPlay(mCurrentState != STATE_PLAYING);
    }

    private void onPlay(boolean play){
        if(!isPaused && isInPlaybackState()){
            if(play){
                mMediaPlayer.start();
                mCurrentState = STATE_PLAYING;
                mHandler.post(mProgressChecker);
            }else{
                mMediaPlayer.pause();
                mCurrentState = STATE_PAUSED;
                mHandler.removeCallbacks(mProgressChecker);
            }
            mVideoControllerView.setPlayPauseActivated(play);
        }
    }

    @Override
    public void onMute(boolean mute){
        if(!isInPlaybackState()){
            return;
        }
        mMediaPlayer.setMute(mute);
        mVideoControllerView.setMuteActivated(mute);
    }

    private void initScreenRotateLock() {
        unlockScreenOrientation();
        mVideoControllerView.setRotationLockActivated(false);
    }

    @Override
    public void onScreenRotateLock(boolean lock) {
        if(!isInPlaybackState()){
            return;
        }
        if(lock)
            lockScreenOrientation();
        else
            unlockScreenOrientation();
        mVideoControllerView.setRotationLockActivated(lock);
    }

    private void lockScreenOrientation() {
        final int orientation = mContext.getResources().getConfiguration().orientation;
        final int rotation = mContext.getWindowManager().getDefaultDisplay().getOrientation();

        if (rotation == Surface.ROTATION_0 || rotation == Surface.ROTATION_90) {
            if (orientation == Configuration.ORIENTATION_PORTRAIT) {
                mContext.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
            }
            else if (orientation == Configuration.ORIENTATION_LANDSCAPE) {
                mContext.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
            }
        }
        else if (rotation == Surface.ROTATION_180 || rotation == Surface.ROTATION_270) {
            if (orientation == Configuration.ORIENTATION_PORTRAIT) {
                mContext.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_REVERSE_PORTRAIT);
            }
            else if (orientation == Configuration.ORIENTATION_LANDSCAPE) {
                mContext.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_REVERSE_LANDSCAPE);
            }
        }
    }

    private void unlockScreenOrientation() {
        mContext.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED);
    }

    private void initScreenRotate() {
        if(PlayerPropertyManager.getInstance(mContext).getScreenOrientation()
                == Configuration.ORIENTATION_LANDSCAPE) {
            final int rotation = mContext.getWindowManager().getDefaultDisplay().getOrientation();

            if (rotation == Surface.ROTATION_270) {
                mContext.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_REVERSE_LANDSCAPE);
            } else {
                mContext.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
            }
        }else{
            mContext.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }
    }

    @Override
    public void onScreenRotate() {
        if(!isInPlaybackState()){
            return;
        }
        final int rotation = mContext.getWindowManager().getDefaultDisplay().getOrientation();

        if(rotation == Surface.ROTATION_0) {
            mContext.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_REVERSE_LANDSCAPE);
        }else if(rotation == Surface.ROTATION_90){
            mContext.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }else if(rotation == Surface.ROTATION_270){
            mContext.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        }
    }

    private void initFullScreen() {
        if(mVideoView == null){
            return;
        }
        mVideoView.updateVideoSize(KollusPlayerContentMode.ScaleAspectFit);
        mVideoControllerView.setFullScreenActivated(true);
    }

    @Override
    public void onFullScreen(boolean full) {
        if(!isInPlaybackState()){
            return;
        }
        if(mVideoView == null){
            return;
        }
        if(full){
            mVideoView.updateVideoSize(KollusPlayerContentMode.ScaleAspectFit);
        }else{
            mVideoView.updateVideoSize(KollusPlayerContentMode.ScaleCenter);
        }
        mVideoControllerView.setFullScreenActivated(full);
    }

    private void initPlayingRate(){
        setPlayingRate(PlayerPropertyManager.getInstance(mContext).getPlayRate());
    }

    private void setPlayingRate(float rate){
        if(!isInPlaybackState()){
            return;
        }
        mMediaPlayer.setPlayingRate(rate);
        mVideoControllerView.setTextRate(String.format(mContext.getString(R.string.kollus_playing_rate), rate));
        mPlayingRate = rate;
        /*
        PlayerPropertyManager instance = PlayerPropertyManager.getInstance(mContext);
        if(instance.isSaveRate()){
            instance.setPlayRate(rate);
        }
        */
    }

    @Override
    public void onPlayingRate(boolean up){
        if(!isInPlaybackState()) {
            return;
        }
        float rate = Math.round(mPlayingRate*100f)/100f;
        if(up){
            if(rate < 4.0f){
                rate += 0.1f;
            }
        }else{
            if(rate > 0.5f){
                rate -= 0.1f;
            }
        }
        setPlayingRate(rate);
    }

    public void toggleControlView(){
        mVideoControllerView.toggleControlView();
    }

    public boolean isLoading(){
        return mVideoControllerView.isLoading();
    }

    public boolean canMoveVideoScreen() {
        if(mVideoView != null) {
            return mVideoView.canMoveVideoScreen();
        }
        return false;
    }

    public void moveVideoFrame(float x, float y) {
        if(mVideoView != null) {
            mVideoView.moveVideoFrame(x, y);
        }
    }

    public void setSeekLabel(int mountMs, boolean bShow) {
        if(!isInPlaybackState()){
            return;
        }
        int seekTimeMs = 0;

        if(mSeekScreenShotTimeMs < 0) {
            mSeekScreenShotTimeMs = mMediaPlayer.getCurrentPosition();
        }
        seekTimeMs = mSeekScreenShotTimeMs + mountMs;

        if(seekTimeMs < 0) {
            seekTimeMs = 0;
        }else if(seekTimeMs > mMediaPlayer.getDuration()){
            seekTimeMs = mMediaPlayer.getDuration();
        }

        if(!bShow) {
            //resetRepeatAB(seekTimeMs);
            seekToExact(seekTimeMs);
            mSeekScreenShotTimeMs = -1;
        }else{
            mVideoControllerView.setSeekLabel(seekTimeMs, mountMs);
        }
    }

    public void setVolumeControl(boolean volumeUp) {
        if(volumeUp) {
            mAudioManager.adjustStreamVolume(AudioManager.STREAM_MUSIC,
                    AudioManager.ADJUST_RAISE, AudioManager.RINGER_MODE_SILENT);
        }else{
            mAudioManager.adjustStreamVolume(AudioManager.STREAM_MUSIC,
                    AudioManager.ADJUST_LOWER, AudioManager.RINGER_MODE_SILENT);
        }
        onMute(false);
        mVideoControllerView.setVolumeLabel(mAudioManager.getStreamVolume(AudioManager.STREAM_MUSIC));
    }

    public void setBrightnessControl(boolean brightUp) {
        int brightness = mBrightness;

        if(brightUp) {
            brightness += BRIGHTNESS_UNIT;
        }else{
            brightness -= BRIGHTNESS_UNIT;
        }

        if(brightness > BRIGHTNESS_MAX) {
            brightness = BRIGHTNESS_MAX;
        }else if(brightness < BRIGHTNESS_MIN) {
            brightness = BRIGHTNESS_MIN;
        }
        setBrightness(brightness);

        int nBrightnessLevel = brightness / BRIGHTNESS_UNIT;
        int nBrightness = nBrightnessLevel * BRIGHTNESS_UNIT + BRIGHTNESS_MIN;

        mVideoControllerView.setBrightnessLabel(nBrightnessLevel);
        mBrightness = nBrightness;

        PlayerPropertyManager instance = PlayerPropertyManager.getInstance(mContext);
        if(instance.isSaveBrightness()) {
            instance.setScreenBrightness(nBrightness);
        }
    }

    private void initBrightness(){
        mBrightness = PlayerPropertyManager.getInstance(mContext).getScreenBrightness();
        setBrightness(mBrightness);
    }

    private void setBrightness(int brightness){
        Window win = mContext.getWindow();
        WindowManager.LayoutParams lp = win.getAttributes();
        lp.screenBrightness = (float)brightness / (float)BRIGHTNESS_MAX;
        win.setAttributes(lp);
    }

    private void initVolumeSeek(){
        mVideoControllerView.setMaxVolumeSeek(mAudioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC));
        mVideoControllerView.setCurrentVolumeSeek(mAudioManager.getStreamVolume(AudioManager.STREAM_MUSIC));
    }

    private void setVolumeSeek(int level) {
        mAudioManager.setStreamVolume(AudioManager.STREAM_MUSIC, level, AudioManager.RINGER_MODE_SILENT);
        onMute(false);
    }

    @Override
    public void onPlaySkipPrev() {
        setSeekLabel(-10 * 1000, false);
    }

    @Override
    public void onPlaySkipNext() {
        setSeekLabel(10 * 1000, false);
    }

    public void setVisibilityTimeView(int orientation){
        mVideoControllerView.setVisibilityTimeView(orientation);
    }

    private Bitmap getScreenShotBitmap(int time) {
        Bitmap bm = null;
        int bitmapIndex = Math.round((time+3000)/1000/mScreenShotInterval);
        if(bitmapIndex >= mScreenShotCount) {
            bitmapIndex = mScreenShotCount - 1;
        }
        if(mScreenShot != null) {
            int column = 0, row = 0, x = 0, y = 0;
            try {
                column = bitmapIndex%10;
                row = bitmapIndex/10;
                x = mScreenShotWidth*column;
                y = mScreenShotHeight*row;
                Rect rect = new Rect(x, y, x+mScreenShotWidth, y+mScreenShotHeight);
                bm = mScreenShot.decodeRegion(rect, new BitmapFactory.Options());
            } catch (Exception e) {
                log.e(String.format("BookmarkAdapter interval %f nTime %d count %d index %d col %d row %d x %d y %d w %d h %d",
                        mScreenShotInterval, time, mScreenShotCount, bitmapIndex, column, row, x, y, mScreenShotWidth, mScreenShotHeight));
            }
        }
        return bm;
    }

    // =============================================================================
    // Bookmark
    // =============================================================================

    @Override
    public void onShowBookmarkView(boolean show) {
        if(!isInPlaybackState()){
            return;
        }
        mVideoControllerView.showBookmarkView(show);
    }

    @Override
    public void onBookmarkAdd() {
        if(!isInPlaybackState()){
            return;
        }
        if(mVideoControllerView.isBookLoading()){
            BraveUtils.showToast(mContext, mContext.getString(R.string.toast_kollus_bookmark_setup_ing));
            return;
        }
        int count = mBookmarkAdapter.getUserLevelCount();
        if(count >= KollusBookmark.MAX_BOOKMARK){
            BraveUtils.showToast(mContext, String.format(mContext.getString(R.string.toast_kollus_bookmark_add_err_max),
                    KollusBookmark.MAX_BOOKMARK));
            return;
        }

        int position = (mMediaPlayer.getCurrentPosition()/1000)*1000;
        int index = mBookmarkAdapter.getItemWithTime(position);
        if(index != -1){
            mVideoControllerView.setBookmarkSelected(index);
            BraveUtils.showToast(mContext, String.format(mContext.getString(R.string.toast_kollus_bookmark_add_err_already),
                    BraveUtils.formattingTime(position/1000)));
            return;
        }

        mVideoControllerView.startBookLoading();
        try {
            mMediaPlayer.updateKollusBookmark(mMediaPlayer.getCurrentPosition());
        } catch(IllegalStateException e) {
            mVideoControllerView.stopBookLoading();
            BraveUtils.showToast(mContext, mContext.getString(R.string.toast_kollus_bookmark_add_failed));
            log.e(log.getStackTraceString(e));
        }
    }

    @Override
    public void onBookmarkDelete(boolean delete) {
        if(!isInPlaybackState()){
            return;
        }
        mBookmarkAdapter.updateDeleteMode(delete);
    }

    private OnItemClickListener mBookmarkItemClickListener = new OnItemClickListener() {
        @Override
        public void onItemClick(View view, int position) {
            BookmarkData bookmark = mBookmarkAdapter.getItem(position);
            KollusBookmark item = bookmark.getBookmark();
            if(item != null){
                seekToExact(item.getTime());
            }
        }
    };

    private OnItemClickListener mBookmarkDeleteLisener = new OnItemClickListener() {
        @Override
        public void onItemClick(View view, int position) {
            BookmarkData bookmark = mBookmarkAdapter.getItem(position);
            KollusBookmark item = bookmark.getBookmark();
            if(item != null){
                mVideoControllerView.startBookLoading();
                try {
                    mMediaPlayer.deleteKollusBookmark(item.getTime());
                } catch(IllegalStateException e) {
                    mVideoControllerView.stopBookLoading();
                    BraveUtils.showToast(mContext, mContext.getString(R.string.toast_kollus_bookmark_delete_failed));
                    log.e(log.getStackTraceString(e));
                }
            }
        }
    };

    private KollusPlayerBookmarkListener mKollusPlayerBookmarkListener = new KollusPlayerBookmarkListener() {

        @Override
        public void onBookmark(List<KollusBookmark> bookmarks, boolean bWritable) {
            log.d("onBookmark");
            mVideoControllerView.stopBookLoading();
            if(bookmarks != null && bookmarks.size() != 0) {
                ArrayList<BookmarkData> bookmarkDatas = new ArrayList<>();
                for (KollusBookmark iter : bookmarks) {
                    BookmarkData bookmarkData = new BookmarkData();
                    Bitmap bm = getScreenShotBitmap(iter.getTime());
                    if (bm != null) {
                        iter.setThumbnail(bm);
                    }
                    bookmarkData.setBookmark(iter);
                    bookmarkDatas.add(bookmarkData);
                }
                mBookmarkAdapter.addAll(bookmarkDatas);
                mVideoControllerView.setBookmarkCount(bookmarkDatas.size());
            }else{
                mVideoControllerView.setBookmarkCount(0);
            }
            mVideoControllerView.setBookmarkWritable(bWritable);
        }

        @Override
        public void onGetBookmarkError(int nErrorCode) {
            log.d("onGetBookmarkError");
            mVideoControllerView.stopBookLoading();
            mVideoControllerView.showBookmarkDefault(true, nErrorCode);
            mVideoControllerView.setBookmarkWritable(false);
        }

        @Override
        public void onBookmarkDeleted(int position, boolean bDeleted) {
            log.d("onBookmarkDeleted");
            mVideoControllerView.stopBookLoading();
            if(bDeleted){
                position = position * 1000;
                int index = mBookmarkAdapter.getItemWithTime(position);
                if(index != -1){
                    mBookmarkAdapter.remove(index);
                    mVideoControllerView.setBookmarkCount(mBookmarkAdapter.getItemCount());
                }
            }else{
                BraveUtils.showToast(mContext, mContext.getString(R.string.toast_kollus_bookmark_delete_failed));
            }
        }

        @Override
        public void onBookmarkUpdated(int position, boolean bUpdated) {
            log.d("onBookmarkUpdated");
            mVideoControllerView.stopBookLoading();
            if(bUpdated){
                position = position * 1000;
                int index = mBookmarkAdapter.getItemWithTime(position);
                if(index == -1){
                    BookmarkData bookmarkData = new BookmarkData();
                    KollusBookmark bookmark = new KollusBookmark();
                    bookmark.setLevel(KollusBookmark.USER_LEVEL);
                    bookmark.setLabel(mContext.getString(R.string.kollus_bookmark_user_level));
                    bookmark.setTime(position);
                    Bitmap bm = getScreenShotBitmap(position);
                    if (bm != null) {
                        bookmark.setThumbnail(bm);
                    }
                    bookmarkData.setBookmark(bookmark);

                    index = mBookmarkAdapter.getAddIndex(position);
                    mBookmarkAdapter.add(index, bookmarkData);
                    mVideoControllerView.setBookmarkCount(mBookmarkAdapter.getItemCount());
                }
                mVideoControllerView.setBookmarkSelected(index);
                if(!mVideoControllerView.isSelectedBookmarklLayer()){
                    BraveUtils.showToast(mContext, String.format(mContext.getString(R.string.toast_kollus_bookmark_add),
                            BraveUtils.formattingTime(position/1000)));
                }
            }else{
                BraveUtils.showToast(mContext, mContext.getString(R.string.toast_kollus_bookmark_add_failed));
            }
        }

    };

    // =============================================================================
    // View Listener
    // =============================================================================

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        log.d("keyCode, event.getRepeatCount() - " +  keyCode + ", " + event.getRepeatCount());

        switch (keyCode) {
            case KeyEvent.KEYCODE_VOLUME_UP:
                setVolumeControl(true);
                return true;
            case KeyEvent.KEYCODE_VOLUME_DOWN:
                setVolumeControl(false);
                return true;
        }

        // Some headsets will fire off 7-10 events on a single click
        if (event.getRepeatCount() > 0) {
            return isMediaKey(keyCode);
        }

        switch (keyCode) {
            case KeyEvent.KEYCODE_HEADSETHOOK:
            case KeyEvent.KEYCODE_MEDIA_PLAY_PAUSE:
                onPlay();
                return true;
            case KeyEvent.KEYCODE_MEDIA_PLAY:
                if(mCurrentState != STATE_PLAYING) {
                    onPlay(true);
                }
                return true;
            case KeyEvent.KEYCODE_MEDIA_PAUSE:
                if(mCurrentState == STATE_PLAYING) {
                    onPlay(false);
                }
                return true;
            case KeyEvent.KEYCODE_MEDIA_PREVIOUS:
            case KeyEvent.KEYCODE_MEDIA_NEXT:
                return true;
        }
        return false;
    }

    private static boolean isMediaKey(int keyCode) {
        return keyCode == KeyEvent.KEYCODE_HEADSETHOOK
                || keyCode == KeyEvent.KEYCODE_MEDIA_PREVIOUS
                || keyCode == KeyEvent.KEYCODE_MEDIA_NEXT
                || keyCode == KeyEvent.KEYCODE_MEDIA_PLAY_PAUSE
                || keyCode == KeyEvent.KEYCODE_MEDIA_PLAY
                || keyCode == KeyEvent.KEYCODE_MEDIA_PAUSE;
    }

    private VideoWindowListener mVideoWindowListener = new VideoWindowListener() {

        @Override
        public void onVideoSurfaceChanged(SurfaceHolder holder, int format, int width,
                                          int height) {
            log.i("onVideoSurfaceChanged");
            if(mFirstSurfaceChanged){
                mFirstSurfaceChanged = false;
                if(mMediaPlayer != null) {
                    if (mVideoView != null && mVideoView.getVideoWidth() == 0 && mVideoView.getVideoHeight() == 0) {
                        mErrorListener.onError(mMediaPlayer, MEDIA_ERROR_UNKNOWN, ErrorCodes.ERROR_UNDEFINED_CODE);
                    }else{
                        if(mSeekWhenPrepared != 0){
                            seekTo(mSeekWhenPrepared);
                        }else {
                            seekTo(mMediaPlayer.getCurrentPosition());
                        }
                    }
                }
            }
        }

        @Override
        public void onVideoSurfaceCreated(SurfaceHolder holder) {
            log.i("onVideoSurfaceCreated");
            mVideoSurfaceReady = true;
            if(mCurrentState == STATE_IDLE) {
                openVideo();
            }else{
                if(mMediaPlayer != null) {
                    mMediaPlayer.setDisplay(holder);
                }
            }
        }

        @Override
        public void onVideoSurfaceDestroyed(SurfaceHolder holder) {
            log.i("onVideoSurfaceDestroyed");
            mVideoSurfaceReady = false;
            mFirstSurfaceChanged = true;
            if(mMediaPlayer != null) {
                mMediaPlayer.destroyDisplay();
            }
        }
    };

    private SeekBar.OnSeekBarChangeListener mOnSeekBarChangeListener = new SeekBar.OnSeekBarChangeListener() {

        @Override
        public void onStopTrackingTouch(SeekBar seekBar) {
            if(isInPlaybackState()){
                mMediaPlayer.seekTo(seekBar.getProgress());
            }
            mDragging = false;
        }

        @Override
        public void onStartTrackingTouch(SeekBar seekBar) {
            mDragging = true;
        }

        @Override
        public void onProgressChanged(SeekBar seekBar, int progress,
                                      boolean fromUser) {
            if(!isInPlaybackState()){
                return;
            }
            mVideoControllerView.setCurrentTime(progress, mMediaPlayer.getDuration());
            if(fromUser) {
                mVideoControllerView.startControllerHiding();
            }
        }
    };

    private SeekBar.OnSeekBarChangeListener mOnVolumeSeekBarChangeListener = new SeekBar.OnSeekBarChangeListener() {

        @Override
        public void onStopTrackingTouch(SeekBar seekBar) {
        }

        @Override
        public void onStartTrackingTouch(SeekBar seekBar) {
        }

        @Override
        public void onProgressChanged(SeekBar seekBar, int progress,
                                      boolean fromUser) {
            mVideoControllerView.setTextVolume(progress);
            if(fromUser) {
                setVolumeSeek(progress);
                mVideoControllerView.startControllerHiding();
            }
        }
    };

    // =============================================================================
    // Kollus Listener
    // =============================================================================

    private int ALIGN(int x, int y) {
        return (x + y - 1) & ~(y - 1);
    }

    MediaPlayerBase.OnVideoSizeChangedListener mSizeChangedListener = new MediaPlayerBase.OnVideoSizeChangedListener() {
        @Override
        public void onVideoSizeChanged(MediaPlayerBase mp, int width, int height) {
            int videoWidth = ALIGN(mp.getVideoWidth(), 4);
            int videoHeight = ALIGN(mp.getVideoHeight(), 4);
            mVideoView.videoSizeChanged(videoWidth, videoHeight);
            log.d("onVideoSizeChanged videoWidth, videoHeight - " + videoWidth + ", " + videoHeight);
        }
    };

    private MediaPlayerBase.OnCompletionListener mCompletionListener = new MediaPlayerBase.OnCompletionListener() {
        @Override
        public void onCompletion(MediaPlayerBase mp) {
            seekToExact(0);
            onPlay(false);
            mCurrentState = STATE_PLAYBACK_COMPLETED;
        }
    };

    private MediaPlayerBase.OnErrorListener mErrorListener = new MediaPlayerBase.OnErrorListener() {
        @Override
        public boolean onError(MediaPlayerBase mp, int framework_err, int impl_err) {
            if(mCurrentState == STATE_ERROR) {
                return true;
            }

            String title = mContext.getString(R.string.kollus_error_dialog_title);
            if(framework_err != ErrorCodes.ERROR_EXPIRATION_DATE &&
                    framework_err != ErrorCodes.ERROR_EXPIRATION_COUNT)
                title += " ("+impl_err+")";

            log.d("framework_err: " + framework_err);
            log.d("impl_err: " + impl_err);

            String message = null;
            if(framework_err == ErrorCodes.ERROR_EXPIRATION_DATE){
                message = String.format(ErrorCodes.getInstance(mContext).getErrorString(framework_err),
                        mMediaPlayer.getErrorString(impl_err));
            } else if (framework_err == ErrorCodes.ERROR_EXPIRATION_COUNT) {
                message = String.format(ErrorCodes.getInstance(mContext).getErrorString(framework_err), impl_err);
            }else if (impl_err == ErrorCodes.ERROR_ABNORMAL_DRM_INFO){
                message = mContext.getString(R.string.dialog_error_impl_err_8655);
            } else {
                if(mMediaPlayer != null)
                    message = mMediaPlayer.getErrorString(impl_err);

                if(message == null)
                    message = ErrorCodes.getInstance(mContext).getErrorString(impl_err);
            }

            BraveUtils.showAlertDialog(mContext, title, message, mContext.getString(R.string.common_ok), null,
                    new MaterialDialog.SingleButtonCallback(){
                        @Override
                        public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                            mContext.finish();
                        }
                    }, null);

            if(mMediaPlayer != null){
                mMediaPlayer.release();
                mMediaPlayer = null;
            }
            mCurrentState = STATE_ERROR;

            return true;
        }
    };

    private MediaPlayerBase.OnInfoListener mInfoListener = new MediaPlayerBase.OnInfoListener() {
        @Override
        public void onBufferingEnd(MediaPlayerBase mp) {
            mVideoControllerView.stopLoading();
        }

        @Override
        public void onBufferingStart(MediaPlayerBase mp) {
            mVideoControllerView.startLoading();
        }

        @Override
        public void onFrameDrop(MediaPlayerBase mp) {
        }

        @Override
        public boolean onInfo(MediaPlayerBase mp, int what, int extra) {
            return false;
        }
    };

    private KollusPlayerLMSListener mKollusPlayerLMSListener = new KollusPlayerLMSListener() {
        @Override
        public void onLMS(String request, String response) {
            log.i(String.format("onLMS request '%s' response '%s", request, response));
        }
    };

    private KollusPlayerDRMListener mKollusPlayerDRMListener = new KollusPlayerDRMListener() {
        @Override
        public void onDRM(String request, String response) {
            log.i(String.format("onDRM request '%s' response '%s'", request, response));
        }

        @Override
        public void onDRMInfo(KollusContent content, int nInfoCode) {
            log.i(String.format("onDRMInfo index %d nInfoCode %d message %s", content.getUriIndex(), nInfoCode, content.getServiceProviderMessage()));
        }
    };

    private MediaPlayerBase.OnExternalDisplayDetectListener mOnExternalDisplayDetectListener = new MediaPlayerBase.OnExternalDisplayDetectListener() {

        @Override
        public void onExternalDisplayDetect(int type, boolean plugged) {
            if(plugged && mKollusContent.getDisableTvOut()) {
                String message = "";
                if(type == MediaPlayerBase.EXTERNAL_HDMI_DISPLAY)
                    message = mContext.getString(R.string.toast_kollus_restart_after_remove_hdmi);
                else if(type == MediaPlayerBase.EXTERNAL_WIFI_DISPLAY)
                    message = mContext.getString(R.string.toast_kollus_restart_off_wifi_display);

                BraveUtils.showToast(mContext, message);
            }
        }
    };

    // =============================================================================
    // Activity State
    // =============================================================================

    public void releasePlayer(){
        if(mMediaPlayer == null)
            return;

        if (mMediaPlayer.isPlaying()) {
            mMediaPlayer.stop();
        }
        mMediaPlayer.release();
        mMediaPlayer = null;

        if (mHandler != null && mProgressChecker != null) {
            mHandler.removeCallbacks(mProgressChecker);
            mHandler = null;
        }
    }

    public void onResume(){
        if(isPaused){
            if(!mVideoControllerView.isSelectedControlLayer()) {
                toggleControlView();
            }else{
                mVideoControllerView.startControllerHiding();
            }
            isPaused = false;
        }
    }

    public void onPause(){
        if(mCurrentState == STATE_PLAYING) {
            onPlay(false);
        }
        isPaused = true;
    }

    public void onDestroy(){
        mVideoControllerView.onDestroy();
        releasePlayer();
    }

    public void onUserLeaveHint(){
        mVideoControllerView.onUserLeaveHint();
        releasePlayer();
    }
}

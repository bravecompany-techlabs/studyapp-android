package kr.co.bravecompany.modoogong.android.stdapp.data;

import kr.co.bravecompany.api.android.stdapp.api.data.FreeStudyItemVO;

/**
 * Created by BraveCompany on 2016. 10. 25..
 */

public class FreeStudyData {
    private FreeStudyItemVO freeStudyItemVO;

    public FreeStudyItemVO getFreeStudyItemVO() {
        return freeStudyItemVO;
    }

    public void setFreeStudyItemVO(FreeStudyItemVO freeStudyItemVO) {
        this.freeStudyItemVO = freeStudyItemVO;
    }
}

package kr.co.bravecompany.modoogong.android.stdapp.viewholder;

import android.support.v7.widget.RecyclerView;
import android.view.View;

/**
 * Created by BraveCompany on 2017. 1. 23..
 */

public class CommonFooterViewHolder extends RecyclerView.ViewHolder{

    public CommonFooterViewHolder(View itemView) {
        super(itemView);
    }
}

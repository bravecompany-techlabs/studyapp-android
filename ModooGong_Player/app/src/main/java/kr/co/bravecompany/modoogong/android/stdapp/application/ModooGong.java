package kr.co.bravecompany.modoogong.android.stdapp.application;

import android.app.Application;
import android.content.Context;

import com.kollus.sdk.media.util.Utils;

import java.io.File;

import kr.co.bravecompany.api.android.stdapp.api.NetworkManager;
import kr.co.bravecompany.modoogong.android.stdapp.config.Tags;
import kr.co.bravecompany.modoogong.android.stdapp.drmplayer.DRMSystem;
import kr.co.bravecompany.modoogong.android.stdapp.utils.log;

/**
 * Created by BraveCompany on 2016. 10. 11..
 */

public class ModooGong extends Application{
    private static ModooGong instance;
    private static Context context;

    public static final boolean isDebug = false;

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
        context = this;

        //// Application Setting ///////////////////////////////////////////////////////
        NetworkManager.init(context, isDebug, host);

        // true : develop, false : release
       // KollusConstant.init("9a31d34c8d4499fa5ec27b1b4faee4485619d758", 7492, isDebug);

        DRMSystem.getInstance().InitKey("9a31d34c8d4499fa5ec27b1b4faee4485619d758", 7492, isDebug);

        // true : show log, false : hide log
        log.init(isDebug);

        // default: -1000
        Tags.STUDY_QA_MENU_INDEX.QA_MENU_SUBJECT = -1000;
        Tags.STUDY_QA_MENU_INDEX.QA_MENU_TYPE = -1000;
        Tags.STUDY_QA_MENU_INDEX.QA_MENU_TEACHER = -1000;
        Tags.STUDY_QA_MENU_INDEX.QA_MENU_LECTURE = -1000;
        Tags.STUDY_QA_MENU_INDEX.QA_MENU_BOOK = -1000;
        Tags.STUDY_QA_MENU_INDEX.QA_MENU_PAGE = -1000;

        // default: "-1000"
        Tags.QA_TYPE_CD.QA_TYPE_LECTURE = "-1000"; //학습상담
        Tags.QA_TYPE_CD.QA_TYPE_STUDY = "-1000"; //수업(강의)내용
        Tags.QA_TYPE_CD.QA_TYPE_ETC = "-1000"; //기타
        Tags.QA_TYPE_CD.QA_TYPE_FEEDBACK = "-1000"; //답변첨삭

        if(hasStudyQA){
            Tags.MENU_INDEX.MENU_STUDY_QA = 2;
        }
        if(hasStudyFile){
            Tags.MENU_INDEX.MENU_STUDY_FILE = 3;
        }
    }

    public static ModooGong getInstance(){
        return instance;
    }

    public static Context getContext() {
        return context;
    }

    public void clearApplicationData() {
        File cache = getCacheDir();
        File appDir = new File(cache.getParent());
        clearData(appDir);

        File sdCardDir = new File(Utils.getStoragePath(this));
        clearData(sdCardDir);
    }

    private void clearData(File dir){
        if(dir.exists()){
            String[] children = dir.list();
            for(String s : children){
                if(!s.equals("lib")){
                    if(deleteDir(new File(dir, s))){
                        log.i("File " + dir.getPath() + "/" + s +" DELETED");
                    }else {
                        log.i("File " + dir.getPath() + "/" + s +" DELETED FAILED");
                    }
                }
            }
        }
    }

    private static boolean deleteDir(File dir) {
        if (dir != null && dir.isDirectory()) {
            String[] children = dir.list();
            for (int i = 0; i < children.length; i++) {
                boolean success = deleteDir(new File(dir, children[i]));
                if (!success) {
                    return false;
                }
            }
        }
        if(dir != null){
            return dir.delete();
        }
        return false;
    }

    // =============================================================================
    // Application Setting
    // =============================================================================

    public static final String host = "modoogong.com";
    public static final String splashDescription = null;
    public static final boolean isShowCateName = true;
    public static final boolean isShowQAMenuSubject = false;
    public static final boolean isShowQAMenuType = false;
    public static final boolean isShowQAMenuTeacher = false;
    public static final boolean isRequiredQAMenuLecture = true;
    public static final boolean isShowQAMenuBook = false;
    public static final boolean isRequiredQAMenuBook = false;
    public static final boolean isShowQAVoiceRecoder = false;
    public static final boolean hasMypage = false;
    public static final boolean hasExplainStudy = false;
    public static final boolean hasExplainStudyBottom = false;
    public static final boolean hasExplainStudyNoTab = false;
    public static final boolean hasMobileWeb = false;
    public static final boolean isShowJoinLogin = true;
    public static final boolean hasStudyQA = false;
    public static final boolean isModoo = true;
    public static final boolean isShowMenuTextColorGray = false;
    public static final boolean hasStudyFile = false;
}

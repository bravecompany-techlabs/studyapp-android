package kr.co.bravecompany.modoogong.android.stdapp.drmplayer;

import android.app.Activity;
import android.os.Handler;
import android.os.Message;
import android.util.Log;

import com.afollestad.materialdialogs.MaterialDialog;

import kr.co.bravecompany.modoogong.android.stdapp.R;

public class DRMUtils
{
    public static boolean isDebug = false;
    public static String TAG = DRMUtils.class.getSimpleName();

    //play Tag
    public static final String TAG_PLAY = "play";
    public static final String TAG_MEDIA_CONTENT_KEY = "media_content_key";


    /**
     * Send a {@link #DEBUG} log message.
     *
     * @param msg The message you would like logged.
     */
    public static int d(String msg) {
        if (!isDebug) {
            return -1;
        }
        return Log.d(TAG, msg);
    }


    /**
     * Send an {@link #ERROR} log message.
     *
     * @param msg The message you would like logged.
     */
    public static int e(String msg) {
        if (!isDebug) {
            return -1;
        }
        return Log.e(TAG, msg);
    }

    /**
     * Send a {@link #WARN} log message.
     *
     * @param msg The message you would like logged.
     */
    public static int w(String msg) {
        if (!isDebug) {
            return -1;
        }
        return Log.w(TAG, msg);
    }

    /**
     * Send a {@link #WARN} log message.
     *
     * @param tag Used to identify the source of a log message.  It usually identifies
     *            the class or activity where the log call occurs.
     * @param msg The message you would like logged.
     */
    public static int w(String userTag, String msg) {
        if (!isDebug) {
            return -1;
        }
        return Log.w(TAG, msg);
    }


    public static Message MakeMessage(int what, int arg0, int arg1)
    {
        //! obtainMessage은 Handler 풀에서 받아온다.

        Message msg = new Message();
        msg.what = what;
        msg.arg1 = arg0;
        msg.arg2 = arg1;

        return msg;
    }


    public static void showAlertDialogOkCancel(Activity activity, String msg,
                                               MaterialDialog.SingleButtonCallback listenerOk,
                                               MaterialDialog.SingleButtonCallback listenerCancel){
        if(activity.isFinishing()) {
            return;
        }
        new MaterialDialog.Builder(activity)
                .content(msg)
                .positiveText(R.string.common_ok)
                .onPositive(listenerOk)
                .negativeText(R.string.common_cancel)
                .onNegative(listenerCancel)
                .cancelable(false)
                .show();
    }
}

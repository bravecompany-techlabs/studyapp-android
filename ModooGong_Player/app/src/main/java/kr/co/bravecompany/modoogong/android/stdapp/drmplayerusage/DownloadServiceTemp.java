package kr.co.bravecompany.modoogong.android.stdapp.drmplayerusage;

import android.os.Looper;

import com.crashlytics.android.answers.Answers;
import com.crashlytics.android.answers.CustomEvent;
import com.kollus.sdk.media.content.KollusContent;

import java.util.ArrayList;
import java.util.concurrent.ExecutorService;

import kr.co.bravecompany.api.android.stdapp.api.OnResultListener;
import kr.co.bravecompany.api.android.stdapp.api.data.LectureItemVO;
import kr.co.bravecompany.api.android.stdapp.api.data.StudyItemVO;
import kr.co.bravecompany.api.android.stdapp.api.data.StudyVodResult;
import kr.co.bravecompany.api.android.stdapp.api.requests.StudyRequests;
import kr.co.bravecompany.modoogong.android.stdapp.config.AnalysisTags;
import kr.co.bravecompany.modoogong.android.stdapp.config.Tags;
import kr.co.bravecompany.modoogong.android.stdapp.data.StudyData;
import kr.co.bravecompany.modoogong.android.stdapp.drmplayerusage.db.DataManager;
import kr.co.bravecompany.modoogong.android.stdapp.drmplayerusage.db.model.Study;
import kr.co.bravecompany.modoogong.android.stdapp.utils.BraveUtils;
import kr.co.bravecompany.modoogong.android.stdapp.utils.log;
import kr.co.bravecompany.player.android.stdapp.activity.DRMDownloadCore;
import kr.co.bravecompany.player.android.stdapp.activity.DRMEvent;
import kr.co.bravecompany.player.android.stdapp.activity.DRMSystemManager;
import kr.co.bravecompany.player.android.stdapp.activity.OnDRMDownloadEventListener;
import okhttp3.Request;

public class DownloadServiceTemp implements OnDRMDownloadEventListener {


    private static final int MAXIMUM_POOL_SIZE = 1;
    private static final long THREAD_WAIT_TIME = 30 * 1000L; //wait 30 seconds

    public static final int DATA_ERROR = -1000;
    private ExecutorService mDataThreadPool;
    private DownloadDataHandler mHandler = new DownloadDataHandler(Looper.getMainLooper());

    public static final String ACTION_TASK_REMOVED = "kr.co.bravecompany.bravespk.android.stdapp.action.TASKREMOVED";

    DRMDownloadCore exService;

    public void InitServiceTemp()
    {
        exService = DRMSystemManager.getInstance().getDownloadCore();
    }

    public void DeinitServiceTemp()
    {

    }

    /**************************************
     *
     * Command!
     *
     * ************************************/

    public void startDownload(LectureItemVO lecture, ArrayList<StudyData> studies, OnDownloadDataListener<Integer> listener){
        mDataThreadPool.execute(new DownloadServiceTemp.DownloadStarter(lecture, studies, listener));
    }

    public void removeDownloadAll(OnDownloadDataListener<String> listener){
        mDataThreadPool.execute(new DownloadServiceTemp.DownloadAllRemover(listener));
    }

    ///* Move To Manager
    public void loadVodData(int studyLectureNo, int lctCode, final String studyKey){
        downloadStateChanged(studyKey, Tags.STUDY_DOWN_STATE_TYPE.STUDY_DOWN_API_ING);
        StudyRequests.getInstance().requestStudyVod(studyLectureNo, lctCode, false,
                new OnResultListener<StudyVodResult>(){

                    @Override
                    public void onSuccess(Request request, StudyVodResult result) {
                        setVodData(result, studyKey);
                    }

                    @Override
                    public void onFail(Request request, Exception exception) {
                        downloadStateChanged(studyKey, Tags.STUDY_DOWN_STATE_TYPE.STUDY_DOWN_API_ERROR);

                        if(exception != null && exception.getMessage() != null) {
                            Answers.getInstance().logCustom(new CustomEvent(AnalysisTags.REQUEST)
                                    .putCustomAttribute(AnalysisTags.ERROR, AnalysisTags.DOWNLOAD + ":: " + exception.getMessage()));
                        }
                    }
                });
    }

    //Lee private > public
    private void setVodData(StudyVodResult result, String studyKey){
        if(result == null){
            downloadStateChanged(studyKey, Tags.STUDY_DOWN_STATE_TYPE.STUDY_DOWN_API_ERROR);
            return;
        }
        String vodInfo = result.getVodInfo();
        if(vodInfo != null){
            startDownload(BraveUtils.fromHTML(vodInfo), studyKey);
            onUpdateVodInfo(studyKey, vodInfo);
        }else{
            downloadStateChanged(studyKey, Tags.STUDY_DOWN_STATE_TYPE.STUDY_DOWN_API_ERROR);
        }
    }




    // =============================================================================
    //
    // Lee, Command Wrapper
    //
    // =============================================================================

    public KollusContent getDownloadContent(String mediaContentKey){
        return exService.getDownloadContent(mediaContentKey);
    }

    public void removeDownloadContents(ArrayList<String> mediaContentKeys){
        exService.removeDownloadContents(mediaContentKeys);
    }

    public void pauseDownloadingContent(String mediaContentKey){
        exService.pauseDownloadingContent(mediaContentKey);
    }


    public String getStorageDownloadSize() {
        return exService.getStorageDownloadSize();
    }

    public String getStorageCacheSize() {
        return exService.getStorageCacheSize();
    }

    public void pauseDownloadingContents(ArrayList<String> mediaContentKeys)
    {
        exService.pauseDownloadingContents(mediaContentKeys);
    }

    public void removeDownloadContentAll()
    {
        exService.removeDownloadContentAll();
    }

    public synchronized void startDownload(String vodInfo, String studyKey)
    {
        exService.startDownload(vodInfo,studyKey);
    }


    // =============================================================================
    //
    // Event Listener
    //
    // =============================================================================

    private OnDownloadListener mDownloadListener;

    public void setOnDownloadListener(OnDownloadListener listener)
    {
        mDownloadListener = listener;
    }


    public void onDownloadProgress(String mediaContentKey, int percent)
    {
        mDownloadListener.onDownloadProgress(mediaContentKey, percent);
    }

    public void OnDownloadEventOccured(DRMEvent event)
    {
        switch (event.mEventCode)
        {
            case DRMEvent.DOWN_EVENT_TYPE.EVENT_STUDY_READY: {
                mDownloadListener.downloadStateChanged(event.studyKey, Tags.STUDY_DOWN_STATE_TYPE.STUDY_DOWN_API_COMPLETE, 0);
            }
            break;
            case DRMEvent.DOWN_EVENT_TYPE.EVENT_STUDY_PENDING: {
                mDownloadListener.downloadStateChanged(event.studyKey, Tags.STUDY_DOWN_STATE_TYPE.STUDY_DOWN_PENDING, 0);

            }
            break;
            case DRMEvent.DOWN_EVENT_TYPE.EVENT_STUDY_STARTING: {
                mDownloadListener.downloadStateChanged(event.studyKey, Tags.STUDY_DOWN_STATE_TYPE.STUDY_DOWN_ING, 0);

                DataManager.getInstance().updateMediaContentKey(event.studyKey, event.mediaKey);
            }
            break;
            case DRMEvent.DOWN_EVENT_TYPE.EVENT_STUDY_KEY_ERROR: {
                mDownloadListener.downloadStateChanged(event.studyKey, Tags.STUDY_DOWN_STATE_TYPE.STUDY_DOWN_ERROR, event.errorCode);

            }
            break;
            case DRMEvent.DOWN_EVENT_TYPE.EVENT_STUDY_URL_ERROR: {
                mDownloadListener.downloadStateChanged(event.studyKey, Tags.STUDY_DOWN_STATE_TYPE.STUDY_DOWN_ERROR, event.errorCode);
            }
            break;
            case DRMEvent.DOWN_EVENT_TYPE.EVENT_DOWN_PAUSED: {
                mDownloadListener.onDownloadPause(event.mediaKey, (int) event.progress);
            }
            break;
            case DRMEvent.DOWN_EVENT_TYPE.EVENT_DOWN_ERROR: {
                // onDownloadStateChangedContent(contentKey, DRMEvent.STUDY_DOWN_STATE_TYPE.STUDY_DOWN_ERROR, errorCode);
                mDownloadListener.downloadStateChangedContent(event.mediaKey,Tags.STUDY_DOWN_STATE_TYPE.STUDY_DOWN_ERROR, event.errorCode );
            }
            break;
            case DRMEvent.DOWN_EVENT_TYPE.EVENT_DOWN_COMPLETED:
            {
                mDownloadListener.downloadStateChangedContent(event.mediaKey, Tags.STUDY_DOWN_STATE_TYPE.STUDY_DOWN_COMPLETE,0);

            }
            break;
            case DRMEvent.DOWN_EVENT_TYPE.EVENT_DOWN_REMOVED:
            {
                DataManager.getInstance().removeStudy(event.studyKey);
            }
            break;
        }
    }


//
//
//    public void onDownloadPause(String mediaContentKey, int percent) {
//        mDownloadListener.onDownloadPause(mediaContentKey, percent);
//    }
//
//    public void downloadStateChanged(String studyKey, int state) {
//        downloadStateChanged(studyKey, state, -1);
//    }
//
//    public void downloadStateChangedContent(String mediaContentKey, int state) {
//        downloadStateChangedContent(mediaContentKey, state, -1);
//    }
//
//    public void downloadStateChanged(String studyKey, int state, int errorCode) {
//        mDownloadListener.downloadStateChanged(studyKey, state, errorCode);
//    }

//    public void downloadStateChangedContent(String mediaContentKey, int state, int errorCode) {
//        mDownloadListener.downloadStateChangedContent(mediaContentKey, state, errorCode);
//    }
//
//    public void onUpdateMediaContentKey(String studyKey, String mediaContentKey) {
//        DataManager.getInstance().updateMediaContentKey(studyKey, mediaContentKey);
//    }

    public void downloadStateChanged(String studyKey, int state)
    {
        mDownloadListener.downloadStateChanged(studyKey, state, -1);
    }

    public void onUpdateVodInfo(String studyKey, String vodInfo)
    {
        DataManager.getInstance().updateVodInfo(studyKey, vodInfo);
    }

//    public void onRemoveDownloadMedia(String studyKey) {
//        DataManager.getInstance().removeStudy(studyKey);
//    }
//
//    public void onRemoveDownloadMediaContent(String mediaContentKey) {
//        DataManager.getInstance().removeStudyContent(mediaContentKey);
//    }

    // =============================================================================
    // DB Query Runnables
    // =============================================================================

    private class DownloadStarter implements Runnable
    {
        private DownloadDataResult<Integer> result = new DownloadDataResult<>();

        private LectureItemVO lecture;
        private ArrayList<StudyData> studies;
        private OnDownloadDataListener<Integer> listener;

        public DownloadStarter(LectureItemVO lecture, ArrayList<StudyData> studies, OnDownloadDataListener<Integer> listener) {
            this.lecture = lecture;
            this.studies = studies;
            this.listener = listener;
        }

        @Override
        public void run() {
            result.listener = listener;

            DataManager instance = DataManager.getInstance();
            instance.refreshRealm();

            instance.createLectureStudyModels(lecture, studies, mOnDataChangeListener);
        }

        private DataManager.OnDataChangeListener mOnDataChangeListener = new DataManager.OnDataChangeListener() {
            @Override
            public void onDataChangeProgress(int progress) {
            }

            @Override
            public void onDataChangeComplete() {
                startDownload(lecture, studies);
                mHandler.sendMessage(mHandler.obtainMessage(DownloadDataHandler.MESSAGE_COMPLETE, result));
            }

            @Override
            public void onDataChangeError() {
                mHandler.sendMessage(mHandler.obtainMessage(DownloadDataHandler.MESSAGE_ERROR, DATA_ERROR));
            }
        };

        private void startDownload(LectureItemVO lecture, ArrayList<StudyData> studies){
            int studyLectureNo = lecture.getStudyLectureNo();
            for(StudyData study : studies){
                if(study != null){
                    StudyItemVO item = study.getStudyItemVO();
                    loadVodData(studyLectureNo, item.getLctCode(), study.getStudyKey());
                }
            }
            result.result = studies.size();
        }
    }

    private class DownloadRemover implements Runnable
    {
        private DownloadDataResult<String> result = new DownloadDataResult<>();

        private ArrayList<String> studyKeys;
        private ArrayList<String> mediaContentKeys;
        private OnDownloadDataListener<String> listener;

        public DownloadRemover(ArrayList<String> studyKeys, ArrayList<String> mediaContentKeys, OnDownloadDataListener<String> listener) {
            this.studyKeys = studyKeys;
            this.mediaContentKeys = mediaContentKeys;
            this.listener = listener;
        }

        @Override
        public void run() {
            result.listener = listener;

            DataManager instance = DataManager.getInstance();
            instance.refreshRealm();

            removeDownloadContents(mediaContentKeys);
            instance.removeStudys(studyKeys, mediaContentKeys, mOnDataChangeListener);
        }

        private DataManager.OnDataChangeListener mOnDataChangeListener = new DataManager.OnDataChangeListener() {
            @Override
            public void onDataChangeProgress(int progress) {
            }

            @Override
            public void onDataChangeComplete() {
                mHandler.sendMessage(mHandler.obtainMessage(DownloadDataHandler.MESSAGE_COMPLETE, result));
            }

            @Override
            public void onDataChangeError() {
                mHandler.sendMessage(mHandler.obtainMessage(DownloadDataHandler.MESSAGE_ERROR, DATA_ERROR));
            }
        };
    }

    private class DownloadFinisher implements Runnable
    {
        private DownloadDataResult<String> result = new DownloadDataResult<>();

        private OnDownloadDataListener<String> listener;

        private ArrayList<String> removeStudyKeys;
        private ArrayList<String> removeMediaContentKeys;

        public DownloadFinisher(OnDownloadDataListener<String> listener) {
            this.listener = listener;
        }

        @Override
        public void run() {
            result.listener = listener;

            DataManager instance = DataManager.getInstance();
            instance.refreshRealm();

            ArrayList<Study> downloadStudies = instance.getDownloadingStudyList();
            ArrayList<String> pauseMediaContentKeys = new ArrayList<>();
            removeStudyKeys = new ArrayList<>();
            removeMediaContentKeys = new ArrayList<>();
            if(downloadStudies != null && downloadStudies.size() != 0){
                for (int i = 0; i < downloadStudies.size(); i++) {
                    Study downloadStudy = downloadStudies.get(i);
                    int state = downloadStudy.getState();
                    String mediaContentKey = downloadStudy.getMediaContentKey();
                    switch (state) {
                        case Tags.STUDY_DOWN_STATE_TYPE.STUDY_DOWN_ING:
                            pauseMediaContentKeys.add(mediaContentKey);
                            break;
                        case Tags.STUDY_DOWN_STATE_TYPE.STUDY_DOWN_API_ING:
                        case Tags.STUDY_DOWN_STATE_TYPE.STUDY_DOWN_API_COMPLETE:
                        case Tags.STUDY_DOWN_STATE_TYPE.STUDY_DOWN_PENDING:
                            if (mediaContentKey == null) {
                                removeStudyKeys.add(downloadStudy.getStudyKey());
                            } else {
                                removeMediaContentKeys.add(mediaContentKey);
                            }
                            break;
                    }
                }
                pauseDownloadingContents(pauseMediaContentKeys);
                removeDownloadContents(removeMediaContentKeys);
                instance.updateDownStateContents(pauseMediaContentKeys,
                        Tags.STUDY_DOWN_STATE_TYPE.STUDY_DOWN_PAUSE, -1, mOnPauseDataChangeListener);
            }else{
                mHandler.sendMessage(mHandler.obtainMessage(DownloadDataHandler.MESSAGE_COMPLETE, result));
            }
        }

        private DataManager.OnDataChangeListener mOnPauseDataChangeListener = new DataManager.OnDataChangeListener() {
            @Override
            public void onDataChangeProgress(int progress) {
            }

            @Override
            public void onDataChangeComplete() {
                DataManager.getInstance().removeStudys(removeStudyKeys, removeMediaContentKeys, mOnRemoveDataChangeListener);
            }

            @Override
            public void onDataChangeError() {
                mHandler.sendMessage(mHandler.obtainMessage(DownloadDataHandler.MESSAGE_ERROR, DATA_ERROR));
            }
        };

        private DataManager.OnDataChangeListener mOnRemoveDataChangeListener = new DataManager.OnDataChangeListener() {
            @Override
            public void onDataChangeProgress(int progress) {
            }

            @Override
            public void onDataChangeComplete() {
                mHandler.sendMessage(mHandler.obtainMessage(DownloadDataHandler.MESSAGE_COMPLETE, result));
            }

            @Override
            public void onDataChangeError() {
                mHandler.sendMessage(mHandler.obtainMessage(DownloadDataHandler.MESSAGE_ERROR, DATA_ERROR));
            }
        };
    }

    private class DownloadAllRemover implements Runnable
    {
        private DownloadDataResult<String> result = new DownloadDataResult<>();

        private OnDownloadDataListener<String> listener;

        public DownloadAllRemover(OnDownloadDataListener<String> listener) {
            this.listener = listener;
        }

        @Override
        public void run() {
            result.listener = listener;

            DataManager instance = DataManager.getInstance();
            instance.refreshRealm();

            removeDownloadContentAll();
            instance.removeAllLectureStudyModels(mOnDataChangeListener);
        }

        private DataManager.OnDataChangeListener mOnDataChangeListener = new DataManager.OnDataChangeListener() {
            @Override
            public void onDataChangeProgress(int progress) {
            }

            @Override
            public void onDataChangeComplete() {
                mHandler.sendMessage(mHandler.obtainMessage(DownloadDataHandler.MESSAGE_COMPLETE, result));
            }

            @Override
            public void onDataChangeError() {
                mHandler.sendMessage(mHandler.obtainMessage(DownloadDataHandler.MESSAGE_ERROR, DATA_ERROR));
            }
        };
    }

    // =============================================================================
    // Process killed
    // =============================================================================

    public void forceFinishDownload(boolean taskRemoved, OnDownloadDataListener<String> listener){
        try{
            if(!taskRemoved) {
                exService.forceFinishDownload();
            }
            mDataThreadPool.execute(new DownloadServiceTemp.DownloadFinisher(listener));
        }catch (Exception e){
            log.e(log.getStackTraceString(e));
        }
    }

    public void removeDownload(ArrayList<Study> studies, OnDownloadDataListener<String> listener){
        if(studies != null && studies.size() != 0) {
            ArrayList<String> studyKeys = new ArrayList<>();
            ArrayList<String> mediaContentKeys = new ArrayList<>();
            for (Study study : studies) {
                String mediaContentKey = study.getMediaContentKey();
                if (mediaContentKey == null) {
                    studyKeys.add(study.getStudyKey());
                } else {
                    mediaContentKeys.add(mediaContentKey);
                }
            }
            mDataThreadPool.execute(new DownloadServiceTemp.DownloadRemover(studyKeys, mediaContentKeys, listener));
        }
    }

}


package kr.co.bravecompany.modoogong.android.stdapp.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import kr.co.bravecompany.modoogong.android.stdapp.R;
import kr.co.bravecompany.modoogong.android.stdapp.config.Tags;
import kr.co.bravecompany.modoogong.android.stdapp.viewholder.LectureEndItemViewHolder;
import kr.co.bravecompany.modoogong.android.stdapp.viewholder.LectureItemViewHolder;
import kr.co.bravecompany.modoogong.android.stdapp.viewholder.OnItemClickListener;
import kr.co.bravecompany.api.android.stdapp.api.data.LectureItemVO;
import kr.co.bravecompany.modoogong.android.stdapp.data.LectureData;

/**
 * Created by BraveCompany on 2016. 10. 14..
 */

public class LectureAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>{

    private List<LectureData> items = new ArrayList<LectureData>();

    public LectureData getItem(int position){
        return items.get(position);
    }


    public int getItemWithStudyLectureNo(int studyLectureNo){
        for(int i=0; i<items.size(); i++){
            LectureItemVO item = items.get(i).getLectureItemVO();
            if(studyLectureNo == item.getStudyLectureNo()){
                return i;
            }
        }
        return -1;
    }

    public void clear() {
        items.clear();
        notifyDataSetChanged();
    }

    public void add(LectureData LectureData) {
        items.add(LectureData);
        notifyDataSetChanged();
    }

    public void addAll(List<LectureData> items) {
        this.items.addAll(items);
        notifyDataSetChanged();
    }

    public void refresh(LectureItemVO lecture){
        int oldItem = getItemWithStudyLectureNo(lecture.getStudyLectureNo());
        if(oldItem != -1){
            items.get(oldItem).setLectureItemVO(lecture);
        }
        notifyDataSetChanged();
    }
    

    private OnItemClickListener mListener;
    public void setOnItemClickListener(OnItemClickListener listener) {
        mListener = listener;
    }

    @Override
    public int getItemViewType(int position) {
        return items.get(position).getType();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = null;

        switch (viewType){
            case Tags.LECTURE_VIEW_TYPE.LECTURE_ING_ITEM:
                view = inflater.inflate(R.layout.view_lecture_item, parent, false);
                return new LectureItemViewHolder(view);
            case Tags.LECTURE_VIEW_TYPE.LECTURE_END_ITEM:
                view = inflater.inflate(R.layout.view_lecture_end_item, parent, false);
                return new LectureEndItemViewHolder(view);
        }
        throw new IllegalArgumentException("invalid position");
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        int type = items.get(position).getType();
        switch (type){
            case Tags.LECTURE_VIEW_TYPE.LECTURE_ING_ITEM:
                LectureItemViewHolder holder1 = (LectureItemViewHolder)holder;
                holder1.setLectureItem(items.get(position).getLectureItemVO());
                holder1.setOnItemClickListener(mListener);
                break;
            case Tags.LECTURE_VIEW_TYPE.LECTURE_END_ITEM:
                LectureEndItemViewHolder holder2 = (LectureEndItemViewHolder)holder;
                holder2.setLectureEndItem(items.get(position).getLectureItemVO());
                //holder2.setOnItemClickListener(mListener);
                break;
        }
    }

    @Override
    public int getItemCount() {
        return items.size();
    }
}

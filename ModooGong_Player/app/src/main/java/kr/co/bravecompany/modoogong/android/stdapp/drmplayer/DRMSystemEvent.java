package kr.co.bravecompany.modoogong.android.stdapp.drmplayer;

public class DRMSystemEvent
{
//    public static final int DEVICE_SETTING_START 	= 100;
//    public static final int DEVICE_SETTING_END 	= 101;
//    public static final int DEVICE_SETTING_ERROR 	= 102;
//
//    public static final int DOWNLOAD_CHECKING	= 200;
//    public static final int DOWNLOAD_CHECK_COMPLETE = 201;
//    public static final int DOWNLOAD_START 	= 202;
//    public static final int DOWNLOAD_PROGRESS = 203;
//    public static final int DOWNLOAD_COMPLETE = 204;
//    public static final int DOWNLOAD_ERROR 	= 205;
//    public static final int UNSUPPORT_DEVICE	= 206;

    public static final int EVENT_DEVICE_STOP_LOADING = 1;
    public static final int EVENT_DEVICE_START_LOADING = 2;
    public static final int EVENT_DEVICE_CHECK_ERROR = 3;
    public static final int EVENT_DEVICE_LOAD_COMPLETED =4;
    public static final  int EVENT_DEVICE_USER_CANCEL =5;
    public  static final int EVENT_DEVICE_MESSAGE = 6;

    public int mEventCode ;
  //  public int mStateCode;
  //  public int mArg0;
    public String mMsg;

//    public DRMSystemEvent(int eventCode, int state, int arg)
//    {
//        mEventCode = eventCode;
//        mStateCode = state;
//        mArg0 = arg;
//    }

    public DRMSystemEvent(int eventCode, String msg)
    {
        mEventCode = eventCode;
        mMsg = msg;
    }

    public DRMSystemEvent(int eventCode)
    {
        mEventCode = eventCode;
    }
}


package kr.co.bravecompany.modoogong.android.stdapp.drmplayerusage;

/**
 * Created by BraveCompany on 2016. 12. 6..
 */

public interface OnDownloadBindListener {
    void onBindComplete();
}

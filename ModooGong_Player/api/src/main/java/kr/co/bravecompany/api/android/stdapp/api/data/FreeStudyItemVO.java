package kr.co.bravecompany.api.android.stdapp.api.data;

/**
 * Created by BraveCompany on 2016. 11. 10..
 */

public class FreeStudyItemVO {
    private int castNo;
    private int cate;
    private int category;
    private String title;
    private String content;
    private String thumb;
    private String thumb2;
    private String type;
    private String video;
    private String videoKey;

    public int getCastNo() {
        return castNo;
    }

    public int getCate() {
        return cate;
    }

    public int getCategory() {
        return category;
    }

    public String getTitle() {
        return title;
    }

    public String getContent() {
        return content;
    }

    public String getThumb() {
        return thumb;
    }

    public String getThumb2() {
        return thumb2;
    }

    public String getType() {
        return type;
    }

    public String getVideo() {
        return video;
    }

    public String getVideoKey() {
        return videoKey;
    }
}

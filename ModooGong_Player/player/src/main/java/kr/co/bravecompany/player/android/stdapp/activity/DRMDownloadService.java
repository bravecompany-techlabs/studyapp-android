package kr.co.bravecompany.player.android.stdapp.activity;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.support.annotation.Nullable;

import com.kollus.sdk.media.content.KollusContent;

import java.util.ArrayList;

public class DRMDownloadService extends Service implements OnDRMDownloadEventListener {

    private final IBinder mBinder = new DRMDownloadService.LocalBinder();
    private DRMDownloadCore drmDownloadCore;

    public class LocalBinder extends Binder
    {
        public DRMDownloadService getService()
        {
            return DRMDownloadService.this;
        }
    }


    public DRMDownloadService() {
        super();
    }

    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId)
    {
        //drmDownloadCore = DRMSystemManager.getInstance().getDownloadCore();
        //drmDownloadCore.SetEventListener(this);

        return START_STICKY;
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent)
    {
        drmDownloadCore = DRMSystemManager.getInstance().getDownloadCore();
        drmDownloadCore.SetEventListener(this);

        return mBinder;
    }


    @Override
    public boolean onUnbind(Intent intent) {
        return super.onUnbind(intent);
    }

    @Override
    public void onRebind(Intent intent) {
        super.onRebind(intent);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }


    // =============================================================================
    //
    // Lee, Command
    //
    // =============================================================================

    public KollusContent getDownloadContent(String mediaContentKey){
        return drmDownloadCore.getDownloadContent(mediaContentKey);
    }

    public void removeDownloadContents(ArrayList<String> mediaContentKeys){
        drmDownloadCore.removeDownloadContents(mediaContentKeys);
    }

    public void pauseDownloadingContent(String mediaContentKey){
        drmDownloadCore.pauseDownloadingContent(mediaContentKey);
    }


    public String getStorageDownloadSize() {
        return drmDownloadCore.getStorageDownloadSize();
    }

    public String getStorageCacheSize() {
        return drmDownloadCore.getStorageCacheSize();
    }

    public void pauseDownloadingContents(ArrayList<String> mediaContentKeys)
    {
        drmDownloadCore.pauseDownloadingContents(mediaContentKeys);
    }

    public void removeDownloadContentAll()
    {
        drmDownloadCore.removeDownloadContentAll();
    }

    public synchronized void startDownload(String vodInfo, String studyKey)
    {
        drmDownloadCore.startDownload(vodInfo,studyKey);
    }


    // =============================================================================
    //
    // Lee, Event...
    //
    // =============================================================================

    public void onDownloadProgress(String mediaContentKey, int percent)
    {
        //mDownloadListener.onDownloadProgress(mediaContentKey, percent);
    }

    public void OnDownloadEventOccured(DRMEvent event)
    {
        switch (event.mEventCode)
        {
            case DRMEvent.DOWN_EVENT_TYPE.EVENT_STUDY_READY: {
                //mDownloadListener.downloadStateChanged(event.studyKey, Tags.STUDY_DOWN_STATE_TYPE.STUDY_DOWN_API_COMPLETE, 0);
            }
            break;
            case DRMEvent.DOWN_EVENT_TYPE.EVENT_STUDY_PENDING: {
               // mDownloadListener.downloadStateChanged(event.studyKey, Tags.STUDY_DOWN_STATE_TYPE.STUDY_DOWN_PENDING, 0);

            }
            break;
            case DRMEvent.DOWN_EVENT_TYPE.EVENT_STUDY_STARTING: {
               // mDownloadListener.downloadStateChanged(event.studyKey, Tags.STUDY_DOWN_STATE_TYPE.STUDY_DOWN_ING, 0);

               // DataManager.getInstance().updateMediaContentKey(event.studyKey, event.mediaKey);
            }
            break;
            case DRMEvent.DOWN_EVENT_TYPE.EVENT_STUDY_KEY_ERROR: {
               // mDownloadListener.downloadStateChanged(event.studyKey, Tags.STUDY_DOWN_STATE_TYPE.STUDY_DOWN_ERROR, event.errorCode);

            }
            break;
            case DRMEvent.DOWN_EVENT_TYPE.EVENT_STUDY_URL_ERROR: {
               // mDownloadListener.downloadStateChanged(event.studyKey, Tags.STUDY_DOWN_STATE_TYPE.STUDY_DOWN_ERROR, event.errorCode);
            }
            break;
            case DRMEvent.DOWN_EVENT_TYPE.EVENT_DOWN_PAUSED: {
               // mDownloadListener.onDownloadPause(event.mediaKey, (int) event.progress);
            }
            break;
            case DRMEvent.DOWN_EVENT_TYPE.EVENT_DOWN_ERROR: {
                // onDownloadStateChangedContent(contentKey, DRMEvent.STUDY_DOWN_STATE_TYPE.STUDY_DOWN_ERROR, errorCode);
                //mDownloadListener.downloadStateChangedContent(event.mediaKey,Tags.STUDY_DOWN_STATE_TYPE.STUDY_DOWN_ERROR, event.errorCode );
            }
            break;
            case DRMEvent.DOWN_EVENT_TYPE.EVENT_DOWN_COMPLETED:
            {
                //mDownloadListener.downloadStateChangedContent(event.mediaKey, Tags.STUDY_DOWN_STATE_TYPE.STUDY_DOWN_COMPLETE,0);

            }
            break;
            case DRMEvent.DOWN_EVENT_TYPE.EVENT_DOWN_REMOVED:
            {
               // DataManager.getInstance().removeStudy(event.studyKey);
            }
            break;
        }
    }
}

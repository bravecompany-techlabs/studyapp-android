package kr.co.bravecompany.player.android.stdapp.activity;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.support.annotation.NonNull;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.kollus.sdk.media.KollusStorage;
import com.kollus.sdk.media.util.ErrorCodes;
import com.kollus.sdk.media.util.LibraryChecker;
import com.kollus.sdk.media.util.Utils;

import kr.co.bravecompany.player.android.stdapp.R;
import kr.co.bravecompany.player.android.stdapp.config.KollusConstant;


public class DRMSystemManager
{
    private final int DEVICE_SETTING_START 	= 100;
    private final int DEVICE_SETTING_END 	= 101;
    private final int DEVICE_SETTING_ERROR 	= 102;

    private final int DOWNLOAD_CHECKING	= 200;
    private final int DOWNLOAD_CHECK_COMPLETE = 201;
    private final int DOWNLOAD_START 	= 202;
    private final int DOWNLOAD_PROGRESS = 203;
    private final int DOWNLOAD_COMPLETE = 204;
    private final int DOWNLOAD_ERROR 	= 205;
    private final int UNSUPPORT_DEVICE	= 206;


    private static DRMSystemManager instance;

    private KollusStorage mStorage;
    private Context mContext;
    private Activity mSetupActivity;

    private LibraryChecker mLibraryChecker;

    private int mState = -1;

    OnDRMSystemEventListener onDrmSystemEventListener;

    boolean mBound;
    DRMDownloadService mDownloadService;
    DRMDownloadCore mDownloadCore=new DRMDownloadCore();


    //LibraryChecker mLibraryHandler;

    public static DRMSystemManager getInstance()
    {
        if(instance ==null)
        {
            instance = new DRMSystemManager();
        }

        return instance;
    }

    public DRMDownloadCore getDownloadCore()
    {
        return mDownloadCore;
    }


    public void InitKey(String key, int port, Boolean isDebug)
    {
        // true : develop, false : release
        KollusConstant.init(key, port, isDebug);
    }


    public void SetupSystem(Context context, Activity setupActivity, OnDRMSystemEventListener callback)
    {
        mContext = context;
        mSetupActivity = setupActivity;
        onDrmSystemEventListener = callback;

        mLibraryChecker = new LibraryChecker(mContext, mOnCheckerListener);
        mLibraryChecker.check(!KollusConstant.isDebug);
    }

    public void DestroySystem()
    {
        unBindDownloadService();
        mDownloadCore.deinit();
    }

    private void initStorage()
    {
        mStorage = KollusStorage.getInstance(mContext, true);

        mStorage.initialize(KollusConstant.KEY, KollusConstant.EXPIRE_DATE,
                mContext.getPackageName());

        mStorage.setDeviceASync(
                Utils.getStoragePath(mContext),
                Utils.createUUIDSHA1(mContext),
                Utils.createUUIDMD5(mContext),
                Utils.isTablet(mContext),
                mDeviceListener);

        mDownloadCore.init(mContext.getApplicationContext());

        bindDownloadService();
    }

    // =============================================================================
    // Device Listener
    // =============================================================================


    private KollusStorage.OnKollusStorageDeviceListener mDeviceListener = new KollusStorage.OnKollusStorageDeviceListener() {

        @Override
        public void onDeviceSettingEnd() {
            DRMUtils.d("Kollus Device Setting End");
            mLibraryHandler.sendEmptyMessage(DEVICE_SETTING_END);
        }

        @Override
        public void onDeviceSettingStart() {
            DRMUtils.d("Kollus Device Setting Start");

            mLibraryHandler.sendEmptyMessage(DEVICE_SETTING_START);
        }

        @Override
        public void onDeviceSettingError(int nErrorCode)
        {
            String message = String.format("ErrorCode(%d) -- %s", nErrorCode,
                    ErrorCodes.getInstance(mContext).getErrorString(nErrorCode));

            DRMUtils.d(message);

            mLibraryHandler.sendMessage(DRMUtils.MakeMessage(DEVICE_SETTING_ERROR, nErrorCode, 0));
        }
    };


    private Handler mLibraryHandler = new Handler() {

        @Override
        public void handleMessage(Message msg) {
            mState = msg.what;
            switch (msg.what) {
                case DOWNLOAD_CHECKING: {
                    //onDrmSystemEventListener.showMessage(mContext.getString(R.string.kollus_resource_check));

                    String progMsg = String.format((mContext.getString(R.string.kollus_resource_check)));
                    onDrmSystemEventListener.OnDRMSystemEvent(
                            new DRMEvent(DRMEvent.DEVICE_EVENT_TYPE.EVENT_DEVICE_MESSAGE, progMsg));
                }
                break;
                case DOWNLOAD_CHECK_COMPLETE: {
                   // onDrmSystemEventListener.showMessage(mContext.getString(R.string.kollus_resource_check_complete));
                    onDrmSystemEventListener.OnDRMSystemEvent(
                            new DRMEvent(DRMEvent.DEVICE_EVENT_TYPE.EVENT_DEVICE_MESSAGE, mContext.getString(R.string.kollus_resource_check_complete)));

                    int nCount = mLibraryChecker.getDownloadCount();
                    int nTotalSize = mLibraryChecker.getDownloadTotalSize();

                    if (nCount > 0 && nTotalSize > 0) {
                        String message = String.format(mContext.getString(R.string.dialog_kollus_resource_download),
                                nCount, nTotalSize / (1024. * 1024.));

                        DRMUtils.showAlertDialogOkCancel(mSetupActivity, message,
                                mLibraryOkDialogListener, mLibraryCancelDialogListener);
                    }
                    else {
                        initStorage();
                    }
                }
                break;
                case DOWNLOAD_START: {
                   // onDrmSystemEventListener.showMessage(mContext.getString(R.string.kollus_resource_download_ing));

                    String progMsg = String.format((mContext.getString(R.string.kollus_resource_download_ing)));
                    onDrmSystemEventListener.OnDRMSystemEvent(
                            new DRMEvent(DRMEvent.DEVICE_EVENT_TYPE.EVENT_DEVICE_MESSAGE, progMsg));

                }
                break;
                case DOWNLOAD_PROGRESS: {
//                    onDrmSystemEventListener.showMessage(
//                            String.format(mContext.getString(R.string.kollus_resource_download_ing_progress),
//                                    msg.arg1, mLibraryChecker.getDownloadCount(),
//                                    msg.arg2));

                    String progMsg = String.format(mContext.getString(R.string.kollus_resource_download_ing_progress),
                            msg.arg1, mLibraryChecker.getDownloadCount(),
                            msg.arg2);

                    onDrmSystemEventListener.OnDRMSystemEvent(
                            new DRMEvent(DRMEvent.DEVICE_EVENT_TYPE.EVENT_DEVICE_MESSAGE, progMsg));

                }
                    break;
                case DOWNLOAD_COMPLETE:
                    //setupStorage();
                    initStorage();
                    break;
                case DOWNLOAD_ERROR:
                    //onDrmSystemEventListener.stopLoading();
                   // onDrmSystemEventListener.showMessage(mContext.getString(R.string.kollus_resource_download_error));
                    //Answers.getInstance().logCustom(new CustomEvent(AnalysisTags.SPLASH)
                    //        .putCustomAttribute(AnalysisTags.ERROR, "resource_download_error"));


                    DRMUtils.d("deviice download error");

                    onDrmSystemEventListener.OnDRMSystemEvent(
                            new DRMEvent(DRMEvent.DEVICE_EVENT_TYPE.EVENT_DEVICE_STOP_LOADING, mContext.getString(R.string.kollus_resource_download_error)));



                    break;
                case UNSUPPORT_DEVICE:

                    //onDrmSystemEventListener.stopLoading();
                    //onDrmSystemEventListener.showMessage(mContext.getString(R.string.kollus_resource_not_support));


                 //   Answers.getInstance().logCustom(new CustomEvent(AnalysisTags.SPLASH)
                  //          .putCustomAttribute(AnalysisTags.ERROR, "not_supported_device"));

                    onDrmSystemEventListener.OnDRMSystemEvent(
                                new DRMEvent(DRMEvent.DEVICE_EVENT_TYPE.EVENT_DEVICE_STOP_LOADING, mContext.getString(R.string.kollus_resource_not_support)));


                    break;
                //Device Setting
                case DEVICE_SETTING_START:
                    //showMessage(getString(R.string.kollus_device_setting_start));
                    break;
                case DEVICE_SETTING_END:
                    //onDrmSystemEventListener.goActivity();
                    onDrmSystemEventListener.OnDRMSystemEvent(
                            new DRMEvent(DRMEvent.DEVICE_EVENT_TYPE.EVENT_DEVICE_LOAD_COMPLETED));

                    break;
                case DEVICE_SETTING_ERROR:
                    //onDrmSystemEventListener.finishActivity(mContext.getString(R.string.kollus_device_setting_error));
//                    Answers.getInstance().logCustom(new CustomEvent(AnalysisTags.SPLASH)
 //                           .putCustomAttribute(AnalysisTags.ERROR, "device_setting_error"));

                    DRMEvent event = new DRMEvent(DRMEvent.DEVICE_EVENT_TYPE.EVENT_DEVICE_CHECK_ERROR, mContext.getString(R.string.kollus_device_setting_error));
                    onDrmSystemEventListener.OnDRMSystemEvent(event);

                                      break;
            }
        }

    };


    private LibraryChecker.OnCheckerListener mOnCheckerListener = new LibraryChecker.OnCheckerListener() {

        @Override
        public void onDownloadChecking() {
            //log.d(String.format("LibraryChecker Checking (ver.%s)", Utils.getVersion()));
            DRMUtils.d("LibraryChecker Checking");
            mLibraryHandler.sendEmptyMessage(DOWNLOAD_CHECKING);
        }

        @Override
        public void onDownloadCheckComplete(int nCount, int nTotalSize) {
            DRMUtils.d(String.format("LibraryChecker CheckComplete (Count : %d, TotalSize : %dByte", nCount, nTotalSize));
            mLibraryHandler.sendEmptyMessage(DOWNLOAD_CHECK_COMPLETE);
        }

        @Override
        public void onDownloadStart() {
            DRMUtils.d("LibraryChecker Download Start");
            mLibraryHandler.sendEmptyMessage(DOWNLOAD_START);
        }

        @Override
        public void onDownloadComplete() {
            DRMUtils.d("LibraryChecker Download Complete");
            mLibraryHandler.sendEmptyMessage(DOWNLOAD_COMPLETE);
        }

        @Override
        public void onDownloadError() {
            DRMUtils.d("LibraryChecker Download Error");
            mLibraryHandler.sendEmptyMessage(DOWNLOAD_ERROR);
        }

        @Override
        public void onDownloadProgress(int index, int progress)
        {
            DRMUtils.d(String.format("LibraryChecker Downloading %dth --> %d%%", index, progress));
            Message msg = DRMUtils.MakeMessage(DOWNLOAD_PROGRESS, index, progress);
            mLibraryHandler.sendMessage(msg);
        }

        @Override
        public void onUnSupportDevice() {
            DRMUtils.e("LibraryChecker Unsupported Device!!");
            mLibraryHandler.sendEmptyMessage(UNSUPPORT_DEVICE);
        }
    };


    private MaterialDialog.SingleButtonCallback mLibraryOkDialogListener = new MaterialDialog.SingleButtonCallback() {
        @Override
        public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
            mLibraryChecker.download();
        }
    };

    private MaterialDialog.SingleButtonCallback mLibraryCancelDialogListener = new MaterialDialog.SingleButtonCallback() {
        @Override
        public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {

            //Lee
            // onDrmSystemEventListener.finishActivity(null);
            DRMEvent event = new DRMEvent(DRMEvent.DEVICE_EVENT_TYPE.EVENT_DEVICE_USER_CANCEL);
            onDrmSystemEventListener.OnDRMSystemEvent(event);
        }
    };



    // =============================================================================
    // Download Manager (Bind)
    // =============================================================================



    private void bindDownloadService(){
        Intent intent = new Intent(mContext, DRMDownloadService.class);
        mContext.bindService(intent, mConnection, Context.BIND_AUTO_CREATE);
    }

    private void unBindDownloadService(){
        if (mBound) {
            mContext.unbindService(mConnection);
        }
    }

    private ServiceConnection mConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName className,
                                       IBinder service) {
            DRMDownloadService.LocalBinder binder = (DRMDownloadService.LocalBinder) service;
            mDownloadService = binder.getService();
            mBound = true;

//            mService.setOnDownloadListener(mDownloadListener);
//            if(mDownloadBindListener != null) {
//                mDownloadBindListener.onBindComplete();
//            }
        }

        @Override
        public void onServiceDisconnected(ComponentName arg0) {
            mBound = false;
        }
    };

//
//    public void StartService()
//    {
//        Intent intent = new Intent(mContext, DRMDownloadService.class);
//        mContext.startService(intent);
//
//    }
//
//    public void StopService()
//    {
//        Intent intent = new Intent(mContext, DRMDownloadService.class);
//        mContext.stopService(intent);
//    }

    // =============================================================================
    // Download Manager (Command)
    // =============================================================================

}

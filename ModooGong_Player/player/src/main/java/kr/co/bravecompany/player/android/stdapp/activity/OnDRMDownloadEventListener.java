package kr.co.bravecompany.player.android.stdapp.activity;

public interface OnDRMDownloadEventListener {

    void OnDownloadEventOccured(DRMEvent event);
    void onDownloadProgress(String mediaKey, int percent);
}



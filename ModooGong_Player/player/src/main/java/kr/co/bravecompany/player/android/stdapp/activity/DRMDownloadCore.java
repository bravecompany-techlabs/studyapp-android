package kr.co.bravecompany.player.android.stdapp.activity;

import android.content.Context;
import android.net.Uri;

import com.kollus.sdk.media.KollusPlayerDRMListener;
import com.kollus.sdk.media.KollusStorage;
import com.kollus.sdk.media.content.KollusContent;
import com.kollus.sdk.media.util.ErrorCodes;
import com.kollus.sdk.media.util.Utils;

import java.util.ArrayList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

//import kr.co.bravecompany.modoogong.android.stdapp.config.Tags;
//import kr.co.bravecompany.modoogong.android.stdapp.manager.PropertyManager;
//import kr.co.bravecompany.modoogong.android.stdapp.utils.log;


/**
 * Created by BraveCompany on 2016. 12. 1..
 */


public class DRMDownloadCore {

   // private final IBinder mBinder = new LocalBinder();

    private ExecutorService mThreadPool;
    private static int NUMBER_OF_CORES = Runtime.getRuntime().availableProcessors();
    private static final int MAXIMUM_POOL_SIZE = 1;
    private static final long THREAD_WAIT_TIME = 30 * 1000L; //wait 30 seconds

    private KollusStorage mStorage;
    //List of Downloads
    private ArrayList<KollusContent> mContentsList;
    private ArrayList<String> mDownloadingList = new ArrayList<>();

    private Context mApplicationContext;

    OnDRMDownloadEventListener mDrmDownloadListener;


    // =============================================================================
    // Process Lifecycle
    // =============================================================================

    public void init(Context applicationContent)
    {
        mApplicationContext = applicationContent;

        mStorage = KollusStorage.getInstance(applicationContent, true);
        mStorage.setOnKollusStorageListener(mKollusStorageListener);
        mStorage.setKollusPlayerDRMListener(mKollusPlayerDRMListener);

        mContentsList = mStorage.getDownloadContentList();

        mThreadPool = Executors.newFixedThreadPool(MAXIMUM_POOL_SIZE);
       // mDataThreadPool = Executors.newFixedThreadPool(MAXIMUM_POOL_SIZE);

        //mDrmDownloadListener = listener;
        //return START_STICKY;
        //return super.onStartCommand(intent, flags, startId);
    }

    public void deinit() {

       // super.onDestroy();
        if(mStorage != null) {
            mStorage.finish();
        }
        if(mThreadPool != null){
            mThreadPool.shutdownNow();
        }
    }

    public void forceFinishDownload()
    {
        if(mThreadPool != null)
            mThreadPool.shutdownNow();
    }

    public void SetEventListener(OnDRMDownloadEventListener listener)
    {
        mDrmDownloadListener = listener;
    }

    // =============================================================================
    // Get Info
    // =============================================================================

    public String getStorageDownloadSize() {
        if(mStorage != null) {
            return Utils.getStringSize(mStorage.getUsedSize(KollusStorage.TYPE_DOWNLOAD));
        }
        return null;
    }

    public String getStorageCacheSize() {
        if(mStorage != null) {
            return Utils.getStringSize(mStorage.getUsedSize(KollusStorage.TYPE_CACHE));
        }
        return null;
    }

    // =============================================================================
    // Download Command!
    // =============================================================================

    public KollusContent getDownloadContent(String mediaContentKey){
        synchronized(mContentsList) {
            for(KollusContent iter : mContentsList) {
                if(iter.getMediaContentKey().equals(mediaContentKey)) {
                    return iter;
                }
            }
        }
        return null;
    }

    public void removeDownloadContents(ArrayList<String> mediaContentKeys){
        for(String mediaContentKey : mediaContentKeys) {
            synchronized (mContentsList) {
                for (KollusContent iter : mContentsList) {
                    if (iter.getMediaContentKey().equals(mediaContentKey)) {
                        removeDownloadingContent(mediaContentKey);
                        if (iter.isDownloading()) {
                            mStorage.unload(mediaContentKey);
                            iter.setDownloading(false);
                        }
                        mStorage.remove(mediaContentKey);
                        mContentsList.remove(iter);
                        break;
                    }
                }
            }
        }
    }

    public void pauseDownloadingContent(String mediaContentKey){
        synchronized(mContentsList) {
            for(KollusContent iter : mContentsList) {
                if(iter.getMediaContentKey().equals(mediaContentKey)) {
                    if(iter.isDownloading()) {
                        onDownloadPaused(mediaContentKey, iter.getDownloadPercent());
                        removeDownloadingContent(mediaContentKey);
                        mStorage.unload(mediaContentKey);
                        iter.setDownloading(false);
                    }
                    break;
                }
            }
        }
    }

    public void pauseDownloadingContents(ArrayList<String> mediaContentKeys){
        for(String mediaContentKey : mediaContentKeys) {
            synchronized (mContentsList) {
                for (KollusContent iter : mContentsList) {
                    if (iter.getMediaContentKey().equals(mediaContentKey)) {
                        if (iter.isDownloading()) {
                            removeDownloadingContent(mediaContentKey);
                            mStorage.unload(mediaContentKey);
                            iter.setDownloading(false);
                        }
                        break;
                    }
                }
            }
        }
    }

    public void removeDownloadContentAll()
    {
        mThreadPool.shutdownNow();
        mStorage.cancelLoad();

        synchronized(mContentsList) {
            for(KollusContent iter : mContentsList) {
                if(iter.isDownloading()){
                    mStorage.unload(iter.getMediaContentKey());
                    iter.setDownloading(false);
                }
                mStorage.remove(iter.getMediaContentKey());
            }
            mStorage.clearCache();
            mContentsList.clear();
            mDownloadingList.clear();
        }
    }

    // =============================================================================
    // Download Process
    // =============================================================================



    public synchronized void startDownload(String vodInfo, String studyKey)
    {
        onDownloadStudyAPIReady(studyKey);

        DownloadTask down = new DownloadTask(vodInfo, studyKey);
        mThreadPool.execute(down);
    }

    private class DownloadTask implements Runnable {

        private String vodInfo;
        private String studyKey;

        public DownloadTask(String vodInfo, String studyKey) {
            this.vodInfo = vodInfo;
            this.studyKey = studyKey;
        }

        @Override
        public void run() {
            onDownloadStudyPending(studyKey);

            Uri uri = Uri.parse(vodInfo);
            if(uri != null) {
                DRMUtils.d("handleIntent Intent Data : "+uri.toString());
                KollusContent content = new KollusContent();
                int index = -1;
                if(uri.getQueryParameterNames().isEmpty())
                    index = mStorage.load(uri.toString()+"?download", content);
                else
                    index = mStorage.load(uri.toString()+"&download", content);
                DRMUtils.d("handleIntent index "+index);
                if(index >= 0) {
                    boolean bExist = false;
                    mStorage.download(content.getMediaContentKey());
                    for(KollusContent tmp : mContentsList) {
                        if(tmp.getMediaContentKey().equals(content.getMediaContentKey())) {
                            tmp.setDownloading(true);
                            bExist = true;
                            break;
                        }
                    }

                    if(!bExist) {
                        mContentsList.add(content);
                        content.setDownloading(true);
                    }

                    onDownloadStudyStarting(studyKey,content.getMediaContentKey());

                   // onUpdateMediaContentKey(studyKey, content.getMediaContentKey());

                    mDownloadingList.add(content.getMediaContentKey());
                    DRMUtils.d("### content.getMediaContentKey() - " + content.getMediaContentKey());
                }
                else {
                    //showErrorMessage(index);
                   // showErrorMessage(index, studyKey);
                    onDownloadStudyKeyError(studyKey, index);
                }
            }else{
               // onDownloadStateChangedStudy(studyKey, DRMEvent.STUDY_DOWN_STATE_TYPE.STUDY_DOWN_ERROR);
                onDownloadStudyUrlError(studyKey, 0);
            }
        }
    }

    private void logErrorMessage(int errorCode) {
        String title = "Error" + " ("+errorCode+")";
        String message = mStorage.getLastError();
        if(message == null)
            message = ErrorCodes.getInstance(mApplicationContext).getErrorString(errorCode);

        DRMUtils.d("DownloadService showErrorMessage " + title + ": " + message);
    }

//
//    private void showErrorMessage(int errorCode, String studyKey)
//    {
//        onDownloadStateChangedStudy(studyKey, DRMEvent.STUDY_DOWN_STATE_TYPE.STUDY_DOWN_ERROR, errorCode);
//
//        logErrorMessage(errorCode);
//    }
//
//    private void showErrorMessageContent(int errorCode, String mediaContentKey) {
//        onDownloadStateChangedContent(mediaContentKey, DRMEvent.STUDY_DOWN_STATE_TYPE.STUDY_DOWN_ERROR, errorCode);
//
//        logErrorMessage(errorCode);
//    }


    // =============================================================================
    // Kollus Event Listener
    // =============================================================================

    private KollusStorage.OnKollusStorageListener mKollusStorageListener = new KollusStorage.OnKollusStorageListener() {
        @Override
        public void onComplete(KollusContent content) {
            synchronized (mContentsList) {
                for(KollusContent iter : mContentsList) {
                    if(iter.getMediaContentKey().equals(content.getMediaContentKey())) {
                        content.setDownloading(false);
                        content.setDownloadCompleted(true);

                        onDownloadCompleted(content.getMediaContentKey());

//                        onDownloadStateChangedContent(content.getMediaContentKey(),
//                                DRMEvent.STUDY_DOWN_STATE_TYPE.STUDY_DOWN_COMPLETE);

                        removeDownloadingContent(content.getMediaContentKey());
                        break;
                    }
                }
            }
        }

        @Override
        public void onProgress(KollusContent content) {
            synchronized (mContentsList) {
                for(KollusContent iter : mContentsList) {
                    if(iter.getMediaContentKey().equals(content.getMediaContentKey())) {
                        int percent = (int) (content.getReceivingSize()*100/content.getFileSize());
                        iter.setDownloading(true);
                        iter.setReceivedSize(content.getReceivingSize());
                        iter.setDownloadPercent(percent);

                        onDownloadProgress(content.getMediaContentKey(), percent);
                        break;
                    }
                }
            }
        }

        @Override
        public void onError(KollusContent content, int errorCode) {
            synchronized (mContentsList) {
                for(KollusContent iter : mContentsList) {
                    if(iter.getMediaContentKey().equals(content.getMediaContentKey())) {
                        iter.setDownloading(false);
                        iter.setDownloadError();

                        onDownloadContentError(content.getMediaContentKey(), errorCode);
                        removeDownloadingContent(content.getMediaContentKey());
                        break;
                    }
                }
            }
        }
    };

    private KollusPlayerDRMListener mKollusPlayerDRMListener = new KollusPlayerDRMListener() {
        @Override
        public void onDRM(String request, String response) {
            DRMUtils.d(String.format("onDRM request '%s' response '%s'", request, response));
        }

        @Override
        public void onDRMInfo(KollusContent content, int nInfoCode)
        {
            if(nInfoCode == KollusPlayerDRMListener.DCB_INFO_DELETE)
            {
                onDownloadRemoved(content.getMediaContentKey());

                synchronized(mContentsList) {
                    for(KollusContent iter : mContentsList) {
                        if(iter.getMediaContentKey().equals(content.getMediaContentKey())) {
                            removeDownloadingContent(content.getMediaContentKey());
                            mContentsList.remove(iter);
                            break;
                        }
                    }
                }
            }

            DRMUtils.d(String.format("onDRMInfo index %d info code %d message %s", content.getUriIndex(), nInfoCode, content.getServiceProviderMessage()));
        }
    };

    private void removeDownloadingContent(String mediaContentKey){
        synchronized(mDownloadingList) {
            for (int i = 0; i < mDownloadingList.size(); i++) {
                if (mDownloadingList.get(i).equals(mediaContentKey)) {
                    mDownloadingList.remove(i);
                    break;
                }
            }
        }
    }


    // =============================================================================
    // Event Fire (New)
    // =============================================================================

    void onDownloadStudyAPIReady(String studyKey)
    {
        //onDownloadStateChangedStudy(studyKey, DRMEvent.STUDY_DOWN_STATE_TYPE.STUDY_DOWN_API_COMPLETE);

        DRMEvent e = new DRMEvent(DRMEvent.DOWN_EVENT_TYPE.EVENT_STUDY_READY, studyKey);
        mDrmDownloadListener.OnDownloadEventOccured(e);
    }

    void onDownloadStudyPending(String studyKey)
    {
       // onDownloadStateChangedStudy(studyKey, DRMEvent.STUDY_DOWN_STATE_TYPE.STUDY_DOWN_PENDING);

        DRMEvent e = new DRMEvent(DRMEvent.DOWN_EVENT_TYPE.EVENT_STUDY_PENDING, studyKey);
        mDrmDownloadListener.OnDownloadEventOccured(e);
    }

    void onDownloadStudyKeyError(String studyKey, int errorCode)
    {
        logErrorMessage(errorCode);

       // onDownloadStateChangedStudy(studyKey, DRMEvent.STUDY_DOWN_STATE_TYPE.STUDY_DOWN_ERROR, errorCode);

        DRMEvent e = new DRMEvent(DRMEvent.DOWN_EVENT_TYPE.EVENT_STUDY_KEY_ERROR, studyKey, null, errorCode);
        mDrmDownloadListener.OnDownloadEventOccured(e);
    }

    void onDownloadStudyUrlError(String studyKey, int errorCode)
    {
   //     onDownloadStateChangedStudy(studyKey, DRMEvent.STUDY_DOWN_STATE_TYPE.STUDY_DOWN_ERROR, errorCode);

        logErrorMessage(errorCode);

        DRMEvent e = new DRMEvent(DRMEvent.DOWN_EVENT_TYPE.EVENT_STUDY_URL_ERROR, studyKey, null, errorCode);
        mDrmDownloadListener.OnDownloadEventOccured(e);
    }


    void onDownloadStudyStarting(String studyKey, String mediaContentKey)
    {
      //  onDownloadStateChangedStudy(studyKey, DRMEvent.STUDY_DOWN_STATE_TYPE.STUDY_DOWN_ING);
      //  mDrmDownloadListener.onUpdateMediaContentKey(studyKey, mediaContentKey);

        DRMEvent e = new DRMEvent(DRMEvent.DOWN_EVENT_TYPE.EVENT_STUDY_STARTING, studyKey, mediaContentKey);
        mDrmDownloadListener.OnDownloadEventOccured(e);
    }


    void onDownloadContentError(String contentKey,int errorCode)
    {
       // onDownloadStateChangedContent(contentKey, DRMEvent.STUDY_DOWN_STATE_TYPE.STUDY_DOWN_ERROR, errorCode);

        logErrorMessage(errorCode);

        DRMEvent e = new DRMEvent(DRMEvent.DOWN_EVENT_TYPE.EVENT_DOWN_ERROR,null, contentKey);
        mDrmDownloadListener.OnDownloadEventOccured(e);
    }

    void onDownloadCompleted(String contentKey)
    {
       // onDownloadStateChangedContent(contentKey, DRMEvent.STUDY_DOWN_STATE_TYPE.STUDY_DOWN_COMPLETE);
        DRMEvent e = new DRMEvent(DRMEvent.DOWN_EVENT_TYPE.EVENT_DOWN_COMPLETED,null,  contentKey);
        mDrmDownloadListener.OnDownloadEventOccured(e);
    }

//    void onDownloadError(String contentKey, int errorCode)
//    {
//        DRMEvent e = new DRMEvent(DRMEvent.DOWN_EVENT_TYPE.EVENT_DOWN_ERROR, null, contentKey);
//        mDrmDownloadListener.OnDownloadEventOccured(e);
//    }

    void onDownloadPaused(String contentKey, float percent)
    {
        DRMEvent e = new DRMEvent(DRMEvent.DOWN_EVENT_TYPE.EVENT_DOWN_PAUSED, null, contentKey, percent);
        mDrmDownloadListener.OnDownloadEventOccured(e);
    }

    void onDownloadRemoved(String contentKey)
    {
        DRMEvent e = new DRMEvent(DRMEvent.DOWN_EVENT_TYPE.EVENT_DOWN_REMOVED, contentKey);
        mDrmDownloadListener.OnDownloadEventOccured(e);
    }

    void onDownloadProgress(String mediaContentKey, int percent)
    {
        mDrmDownloadListener.onDownloadProgress(mediaContentKey, percent);
    }

    // =============================================================================
    // Event Fire
    // =============================================================================
    /*
    private void onDownloadProgress(String mediaContentKey, int percent) {
        mDrmDownloadListener.onDownloadProgress(mediaContentKey, percent);
    }

    private void onDownloadPause(String mediaContentKey, int percent) {
        mDrmDownloadListener.onDownloadPause(mediaContentKey, percent);
    }


    public void onDownloadStateChangedStudy(String studyKey, int state, int errorCode)
    {
        mDrmDownloadListener.downloadStateChanged(studyKey, state, errorCode);
    }

    public void onDownloadStateChangedStudy(String studyKey, int state)
    {
        mDrmDownloadListener.downloadStateChanged(studyKey, state, -1);
    }

    public void onDownloadStateChangedContent(String mediaContentKey, int state, int errorCode)
    {
        mDrmDownloadListener.downloadStateChangedContent(mediaContentKey, state, errorCode);
    }


    public void onDownloadStateChangedContent(String mediaContentKey, int state) {
        mDrmDownloadListener.downloadStateChangedContent(mediaContentKey, state, -1);
    }

    public void onUpdateMediaContentKey(String studyKey, String mediaContentKey)
    {

        mDrmDownloadListener.onUpdateMediaContentKey(studyKey, mediaContentKey);

        //DataManager.getInstance().updateMediaContentKey(studyKey, mediaContentKey);
    }

    public void updateVodInfo(String studyKey, String vodInfo) {
        //DataManager.getInstance().updateVodInfo(studyKey, vodInfo);
    }

    public void removeDownloadMedia(String studyKey) {
        //DataManager.getInstance().removeStudy(studyKey);
    }

    public void onRemoveDownloadContent(String mediaContentKey)
    {
        //DataManager.getInstance().removeStudyContent(mediaContentKey);
        mDrmDownloadListener.onRemoveDownloadMediaContent(mediaContentKey);
    }
*/

}

package kr.co.bravecompany.player.android.stdapp.activity;

public class DRMEvent
{
    public static class DEVICE_EVENT_TYPE
    {
        public static final int EVENT_DEVICE_STOP_LOADING = 1;
        public static final int EVENT_DEVICE_START_LOADING = 2;
        public static final int EVENT_DEVICE_CHECK_ERROR = 3;
        public static final int EVENT_DEVICE_LOAD_COMPLETED = 4;
        public static final int EVENT_DEVICE_USER_CANCEL = 5;
        public static final int EVENT_DEVICE_MESSAGE = 6;
    }


    public static class DOWN_EVENT_TYPE
    {
        public static final int EVENT_STUDY_READY       = 1;
        public static final int EVENT_STUDY_PENDING     = 2;
        public static final int EVENT_STUDY_STARTING    = 3;

        public static final int EVENT_STUDY_KEY_ERROR   = 4; //study key,
        public static final int EVENT_STUDY_URL_ERROR   = 5; //study key,


        public static final int EVENT_DOWN_PAUSED       = 6; //mediaKey, percent
        public static final int EVENT_DOWN_ERROR        = 7;
        public static final int EVENT_DOWN_COMPLETED    = 8;
        public static final int EVENT_DOWN_REMOVED      = 9;
    }


    public int mEventCode ;
    public String mMsg;

    public String studyKey;
    public String mediaKey;
    public int errorCode;
    public float progress;


    public DRMEvent(int eventCode)
    {
        mEventCode = eventCode;
    }

    public DRMEvent(int eventCode, String msg)
    {
        mEventCode = eventCode;
        mMsg = msg;
    }

    public DRMEvent(int downEvent, String studyKey, String mediaKeys)
    {
        mEventCode = downEvent;
        this.studyKey  = studyKey;
        this.mediaKey = mediaKeys;
        //this.errorCode = errorCode;
    }

    public DRMEvent(int downEvent, String studyKey, String mediaKey, int errorCode)
    {
        mEventCode = downEvent;
        this.studyKey  = studyKey;
        this.mediaKey = mediaKey;
        this.errorCode = errorCode;
    }

    public DRMEvent(int downEvent, String studyKey, String mediaKey, float progress)
    {
        mEventCode = downEvent;
        this.studyKey  = studyKey;
        this.mediaKey = mediaKey;
        this.progress = progress;
    }
}


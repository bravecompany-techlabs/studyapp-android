package kr.co.bravecompany.api.android.stdapp.api.data;

/**
 * Created by BraveCompany on 2016. 12. 16..
 */

public class FreeExplainStudyItemVO {
    private int examNo;
    private int examYear;
    private int examCate;
    private int examClass;
    private String examName;
    private String examScope;
    private String examRegDate;
    private String bestVodYN;
    private int vodNo;
    private String vodName;
    private int vodCate;
    private String vodPath;
    private String vodKey;
    private String vodThum;

    public int getExamNo() {
        return examNo;
    }

    public int getExamYear() {
        return examYear;
    }

    public int getExamCate() {
        return examCate;
    }

    public int getExamClass() {
        return examClass;
    }

    public String getExamName() {
        return examName;
    }

    public String getExamScope() {
        return examScope;
    }

    public String getExamRegDate() {
        return examRegDate;
    }

    public String getBestVodYN() {
        return bestVodYN;
    }

    public int getVodNo() {
        return vodNo;
    }

    public String getVodName() {
        return vodName;
    }

    public int getVodCate() {
        return vodCate;
    }

    public String getVodPath() {
        return vodPath;
    }

    public String getVodKey() {
        return vodKey;
    }

    public String getVodThum() {
        return vodThum;
    }
}

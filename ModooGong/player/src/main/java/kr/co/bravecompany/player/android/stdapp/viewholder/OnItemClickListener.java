package kr.co.bravecompany.player.android.stdapp.viewholder;

import android.view.View;

/**
 * Created by BraveCompany on 2016. 10. 13..
 */

public interface OnItemClickListener {
    void onItemClick(View view, int position);
}

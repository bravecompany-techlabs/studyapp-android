package kr.co.bravecompany.player.android.stdapp.video;

/**
 * Created by BraveCompany on 2017. 3. 28..
 */

public interface OnVideoPlayerListener {
    void onPlay();
    void onMute(boolean mute);
    void onScreenRotateLock(boolean lock);
    void onScreenRotate();
    void onFullScreen(boolean full);
    void onPlayingRate(boolean up);
    void onPlaySkipPrev();
    void onPlaySkipNext();
    void onShowBookmarkView(boolean show);
    void onBookmarkAdd();
    void onBookmarkDelete(boolean delete);
}

package kr.co.bravecompany.player.android.stdapp.viewholder;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.kollus.sdk.media.content.KollusBookmark;

import kr.co.bravecompany.player.android.stdapp.R;
import kr.co.bravecompany.player.android.stdapp.utils.BraveUtils;
import kr.co.bravecompany.player.android.stdapp.data.BookmarkData;

/**
 * Created by BraveCompany on 2017. 5. 22..
 */

public class BookmarkItemViewHolder extends RecyclerView.ViewHolder {

    private BookmarkData mBookmarkData;
    private Context mContext;

    private ImageView bookThumbnail;
    private TextView txtTime;
    private ImageView btnDelete;

    private boolean isDeleteMode = false;

    private OnItemClickListener mListener;
    public void setOnItemClickListener(OnItemClickListener listener) {
        mListener = listener;
    }

    private OnItemClickListener mDeleteListener;
    public void setOnDeleteClickListener(OnItemClickListener listener) {
        mDeleteListener = listener;
    }

    public BookmarkItemViewHolder(View itemView) {
        super(itemView);
        mContext = itemView.getContext();

        bookThumbnail = (ImageView) itemView.findViewById(R.id.bookThumbnail);
        bookThumbnail.setScaleType(ImageView.ScaleType.CENTER_CROP);
        txtTime = (TextView) itemView.findViewById(R.id.txtTime);
        btnDelete = (ImageView) itemView.findViewById(R.id.btnDelete);

        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mListener != null && !isDeleteMode){
                    mListener.onItemClick(v, getLayoutPosition());
                }
            }
        });

        btnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mDeleteListener != null) {
                    mDeleteListener.onItemClick(v, getLayoutPosition());
                }
            }
        });
    }

    public void setBookmarkItem(BookmarkData bookmarkItem) {
        if(bookmarkItem != null) {
            mBookmarkData = bookmarkItem;

            KollusBookmark bookmark = bookmarkItem.getBookmark();
            BraveUtils.setImage(bookThumbnail, bookmark.getThumbnail(),
                    mContext.getResources().getDimensionPixelSize(R.dimen.play_bookmark_thumbnail_width),
                    mContext.getResources().getDimensionPixelOffset(R.dimen.play_bookmark_thumbnail_height));
            txtTime.setText(BraveUtils.formattingTime(bookmark.getTime()/1000));

            if(isDeleteMode){
                btnDelete.setVisibility(View.VISIBLE);
            }else{
                btnDelete.setVisibility(View.GONE);
            }
        }
    }

    public void updateDeleteMode(boolean isDeleteMode){
        this.isDeleteMode = isDeleteMode;
    }

}

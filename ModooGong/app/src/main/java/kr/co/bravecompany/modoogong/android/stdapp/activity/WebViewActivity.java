package kr.co.bravecompany.modoogong.android.stdapp.activity;

import android.os.Bundle;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import kr.co.bravecompany.modoogong.android.stdapp.R;
import kr.co.bravecompany.modoogong.android.stdapp.config.Tags;

public class WebViewActivity extends BaseActivity {

    private WebView webView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web_view);
        initLayout();
        initListener();
        initData();
    }

    private void initLayout() {

        webView = (WebView) findViewById(R.id.webView);
        startLoading();
        webView.setWebViewClient(new WebViewClient() {
            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                stopLoading();
            }
        });
        webView.setWebChromeClient(new WebChromeClient());
        webView.setOverScrollMode(WebView.OVER_SCROLL_NEVER);
        // Long press block
        webView.setLongClickable(true);
        webView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                return true;
            }
        });

        webView.getSettings().setJavaScriptEnabled(true);          // Javascript를 실행할 수 있도록 설정
        webView.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);   // javascript 코드를 이용하여 윈도우를 새로 열기
        webView.getSettings().setBuiltInZoomControls(false);       // 안드로이드에서 제공하는 줌 아이콘을 사용할 수 있도록 설정
        webView.getSettings().setSupportZoom(false);               // 확대, 축소 기능을 사용할 수 있도록 설정
        webView.getSettings().setSupportMultipleWindows(false);    // 여러개의 윈도우를 사용할 수 있도록 설정

        webView.getSettings().setAllowFileAccess(true);            // webview 내에서 파일 접근 가능 여부
        webView.getSettings().setAppCacheEnabled(true);            // Cache API 사용 여부
        webView.getSettings().setCacheMode(WebSettings.LOAD_DEFAULT); //캐쉬 설정으로 첫 진입에 초수가 단축됨
        webView.getSettings().setDatabaseEnabled(true);            // HTML5 에서 DataBase 허용
        webView.getSettings().setDomStorageEnabled(true);          // HTML5 DOM storage 허용여부
        webView.getSettings().setSaveFormData(false);              // 비밀번호 저장 사용 안함

        webView.getSettings().setUseWideViewPort(true);            // wide viewport 사용하도록 설정
        webView.getSettings().setLoadWithOverviewMode(true);       // 항상 전체 화면으로 보이도록 함
        //webView.getSettings().setMediaPlaybackRequiresUserGesture(false);  // 웹에서 미디어를 재생하기 위해 사용자의 제스처가 필요한지 여부 설정

        webView.getSettings().setGeolocationEnabled(false);        // Geo Location 사용 여부
    }

    private void initListener(){
    }

    private void initData(){
        String url = getIntent().getStringExtra(Tags.TAG_URL);
        if(url != null){
            requestWebUrl(url);
        }
    }

    /**
     * request url
     *
     * @param url request url
     */
    public void requestWebUrl(final String url) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                webView.loadUrl(url);
            }
        });
    }

    @Override
    public void onBackPressed() {
        if (webView.canGoBack()) {
            webView.goBack();
            return;
        }
        super.onBackPressed();
    }
}

package kr.co.bravecompany.modoogong.android.stdapp.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import kr.co.bravecompany.modoogong.android.stdapp.view.ProgressBarDialog;

/**
 * Created by BraveCompany on 2016. 10. 12..
 */

public abstract class BaseActivity extends AppCompatActivity {

    private ProgressBarDialog mProgressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mProgressDialog = ProgressBarDialog.getInstance(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        stopLoading();
    }

    public void startLoading(){
        if(mProgressDialog != null && !mProgressDialog.isShowing() && !isFinishing()) {
            mProgressDialog.show();
        }
    }

    public void stopLoading(){
        if(mProgressDialog != null && mProgressDialog.isShowing() && !isFinishing()) {
            mProgressDialog.dismiss();
        }
    }

    ///////////////////////////////////////////////////////////////////////////
    // Abstract Method In Activity
    ///////////////////////////////////////////////////////////////////////////

}

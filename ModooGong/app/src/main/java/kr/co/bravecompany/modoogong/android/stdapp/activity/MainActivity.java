package kr.co.bravecompany.modoogong.android.stdapp.activity;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.crashlytics.android.answers.Answers;
import com.crashlytics.android.answers.CustomEvent;

import java.util.ArrayList;
import java.util.Arrays;

import kr.co.bravecompany.modoogong.android.stdapp.R;
import kr.co.bravecompany.modoogong.android.stdapp.adapter.MenuAdapter;
import kr.co.bravecompany.api.android.stdapp.api.OnResultListener;
import kr.co.bravecompany.api.android.stdapp.api.config.APIConfig;
import kr.co.bravecompany.api.android.stdapp.api.requests.StudyRequests;
import kr.co.bravecompany.api.android.stdapp.api.data.LectureItemVO;
import kr.co.bravecompany.api.android.stdapp.api.data.LectureResult;
import kr.co.bravecompany.api.android.stdapp.api.data.MainResult;
import kr.co.bravecompany.api.android.stdapp.api.data.NoticeItemVO;
import kr.co.bravecompany.modoogong.android.stdapp.application.ModooGong;
import kr.co.bravecompany.modoogong.android.stdapp.config.AnalysisTags;
import kr.co.bravecompany.modoogong.android.stdapp.config.RequestCode;
import kr.co.bravecompany.modoogong.android.stdapp.config.Tags;
import kr.co.bravecompany.modoogong.android.stdapp.download.DownloadService;
import kr.co.bravecompany.modoogong.android.stdapp.download.OnDownloadDataListener;
import kr.co.bravecompany.modoogong.android.stdapp.download.OnDownloadBindListener;
import kr.co.bravecompany.modoogong.android.stdapp.fragment.FreeExplainLectureFragment;
import kr.co.bravecompany.modoogong.android.stdapp.fragment.FreeExplainStudyFragment;
import kr.co.bravecompany.modoogong.android.stdapp.fragment.FreeStudyFragment;
import kr.co.bravecompany.modoogong.android.stdapp.fragment.LocalLectureFragment;
import kr.co.bravecompany.modoogong.android.stdapp.fragment.FreeLectureFragment;
import kr.co.bravecompany.modoogong.android.stdapp.fragment.LectureFragment;
import kr.co.bravecompany.modoogong.android.stdapp.fragment.NoLoginFragment;
import kr.co.bravecompany.modoogong.android.stdapp.fragment.NoNetworkFragment;
import kr.co.bravecompany.modoogong.android.stdapp.fragment.NoticeFragment;
import kr.co.bravecompany.modoogong.android.stdapp.fragment.OneToOneQAFragment;
import kr.co.bravecompany.modoogong.android.stdapp.fragment.StudyFileFragment;
import kr.co.bravecompany.modoogong.android.stdapp.fragment.StudyQAFragment;
import kr.co.bravecompany.modoogong.android.stdapp.manager.PropertyManager;
import kr.co.bravecompany.modoogong.android.stdapp.utils.BackPressedHandler;
import kr.co.bravecompany.modoogong.android.stdapp.utils.SystemUtils;
import kr.co.bravecompany.modoogong.android.stdapp.utils.BraveUtils;
import kr.co.bravecompany.modoogong.android.stdapp.viewholder.OnItemClickListener;
import kr.co.bravecompany.modoogong.android.stdapp.data.MenuData;
import okhttp3.Request;

public class MainActivity extends DownBaseActivity {

    ArrayList<Fragment> mMenus;
    ArrayList<String> mTitles;

    private Toolbar mToolbar;
    private TextView btnOk;

    private DrawerLayout mDrawer;

    private NavigationView mNavigationView;
    private TextView txtName;
    private LinearLayout layoutLogin;
    private LinearLayout layoutNoLogin;
    private TextView btnLogin;
    private LinearLayout layoutMenuRefresh;
    private ImageView btnMenuRefresh;

    private RecyclerView mListView;
    private MenuAdapter mAdapter;
    private LinearLayoutManager mLayoutManager;

    private FrameLayout btnSetting;

    private MainResult mMainResult = null;
    private LectureResult mLectureResult = null;


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if(requestCode == RequestCode.REQUEST_LOGIN){
                boolean isNewId = data.getBooleanExtra(Tags.TAG_NEW_ID, false);
                if(isNewId) {
                    startLoading();
                    mDownloadManager.removeDownloadAll(new OnDownloadDataListener<String>() {
                        @Override
                        public void onDataProgress(int progress) {
                        }

                        @Override
                        public void onDataComplete(String result) {
                            stopLoading();
                            PropertyManager.getInstance().removeSettings();
                            resetFragment();
                        }

                        @Override
                        public void onDataError(int error) {
                            stopLoading();
                            BraveUtils.showLoadOnFailToast(MainActivity.this);
                        }
                    });
                }else{
                    resetFragment();
                }
            }
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Intent intent = new Intent(this, DownloadService.class);
        startService(intent);

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (mDownloadManager != null) {
            mDownloadManager.setOnDownloadBindListener(new OnDownloadBindListener() {
                @Override
                public void onBindComplete() {
                    boolean taskRemoved = PropertyManager.getInstance().isTaskRemoved();
                    if(taskRemoved) {
                        startLoading();
                        mDownloadManager.forceFinishDownload(true, new OnDownloadDataListener<String>() {
                            @Override
                            public void onDataProgress(int progress) {
                            }

                            @Override
                            public void onDataComplete(String result) {
                                stopLoading();
                                PropertyManager.getInstance().setTaskRemoved(false);

                                initLayout();
                                initListener();
                                //initData();
                            }

                            @Override
                            public void onDataError(int error) {
                                stopLoading();
                                BraveUtils.showLoadOnFailToast(MainActivity.this);
                            }
                        });
                    }else{
                        initLayout();
                        initListener();
                        //initData();
                    }
                }
            });
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        Intent intent = new Intent(this, DownloadService.class);
        stopService(intent);
    }

    @Override
    protected void initLayout() {

        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        mToolbar.setTitle("");
        setSupportActionBar(mToolbar);

        btnOk = (TextView)mToolbar.findViewById(R.id.btnOk);

        mDrawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, mDrawer, mToolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        mDrawer.addDrawerListener(toggle);
        toggle.syncState();

        mNavigationView = (NavigationView) findViewById(R.id.nav_view);
        mNavigationView.setItemIconTintList(null);
        txtName = (TextView)mNavigationView.findViewById(R.id.txtName);
        layoutLogin = (LinearLayout)mNavigationView.findViewById(R.id.layoutLogin);
        layoutNoLogin = (LinearLayout)mNavigationView.findViewById(R.id.layoutNoLogin);
        btnLogin = (TextView)mNavigationView.findViewById(R.id.btnLogin);
        layoutMenuRefresh = (LinearLayout)mNavigationView.findViewById(R.id.layoutMenuRefresh);
        btnMenuRefresh = (ImageView)mNavigationView.findViewById(R.id.btnMenuRefresh);
        btnMenuRefresh.setActivated(false);

        mAdapter = new MenuAdapter();
        mListView = (RecyclerView)mNavigationView.findViewById(R.id.recyclerMenu);
        mListView.setAdapter(mAdapter);
        mLayoutManager = new LinearLayoutManager(getApplicationContext()){
            @Override
            public boolean canScrollVertically() {
                return false;
            }
        };
        mListView.setLayoutManager(mLayoutManager);

        btnSetting = (FrameLayout)mNavigationView.findViewById(R.id.btnSetting);
    }

    @Override
    protected void initListener() {

        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //hide keyboard
                SystemUtils.hideSoftKeyboard(MainActivity.this);

                mDrawer.openDrawer(GravityCompat.START);
            }
        });

        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mOnOkBtnClickListener.onClick(v);
            }
        });

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                BraveUtils.goLogin(MainActivity.this);
                closeDrawer();

                Answers.getInstance().logCustom(new CustomEvent(AnalysisTags.MAIN)
                        .putCustomAttribute(AnalysisTags.ACTION, "go_login"));
            }
        });

        btnMenuRefresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btnMenuRefresh.setActivated(true);
                resetFragment();

                Answers.getInstance().logCustom(new CustomEvent(AnalysisTags.MAIN)
                        .putCustomAttribute(AnalysisTags.ACTION, "refresh_menu"));
            }
        });

        mAdapter.setOnItemClickListener(new OnItemClickListener(){

            @Override
            public void onItemClick(View view, int position) {
                goMenuItemSelected(position);
            }
        });

        btnSetting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(BraveUtils.checkUserInfo()) {
                    startActivity(new Intent(MainActivity.this, SettingActivity.class));
                    closeDrawer();

                    Answers.getInstance().logCustom(new CustomEvent(AnalysisTags.MAIN)
                            .putCustomAttribute(AnalysisTags.ACTION, "go_setting"));
                }else{
                    BraveUtils.showToast(MainActivity.this, getString(R.string.toast_no_login));
                }
            }
        });

        if(mDownloadManager != null) {
            boolean isNewId = getIntent().getBooleanExtra(Tags.TAG_NEW_ID, false);
            if(isNewId) {
                startLoading();
                mDownloadManager.removeDownloadAll(new OnDownloadDataListener<String>() {
                    @Override
                    public void onDataProgress(int progress) {
                    }

                    @Override
                    public void onDataComplete(String result) {
                        stopLoading();
                        PropertyManager.getInstance().removeSettings();
                        initData();
                    }

                    @Override
                    public void onDataError(int error) {
                        stopLoading();
                        BraveUtils.showLoadOnFailToast(MainActivity.this);
                    }
                });
            }else {
                initData();
            }
        }
    }

    private void initData(){
        loadData();
    }

    private void initNavigationMenu(){
        ArrayList<MenuData> menus = new ArrayList<MenuData>();
        ArrayList<String> menu_icon = new ArrayList<>(Arrays.asList(getResources().getStringArray(R.array.menu_icon)));
        if(ModooGong.hasStudyQA){
            menu_icon.add(getString(R.string.menu_study_qa));
        }
        if(ModooGong.hasStudyFile){
            menu_icon.add(getString(R.string.menu_study_file));
        }
        for(int i=0; i<menu_icon.size(); i++){
            String menu = menu_icon.get(i);
            MenuData m = new MenuData();
            m.setType(Tags.MENU_VIEW_TYPE.MENU_ICON);
            m.setName(menu);
            if(i == Tags.MENU_INDEX.MENU_DOWN_LECTURE){
                m.setImage(R.drawable.down_icon);
            }else if(ModooGong.hasStudyFile && i == Tags.MENU_INDEX.MENU_STUDY_FILE){
                m.setImage(R.drawable.file_icon);
            }else{
                m.setImage(R.drawable.pen_icon);
            }
            menus.add(m);
        }
        ArrayList<String> menu_text = new ArrayList<>(Arrays.asList(getResources().getStringArray(R.array.menu_text)));
        if(ModooGong.hasExplainStudy){
            menu_text.add(0, getString(R.string.menu_explain_study));
            if(ModooGong.hasExplainStudyBottom) {
                Tags.FREE_MENU_ADD_POSITION = menus.size();
            }else{
                Tags.FREE_MENU_ADD_POSITION = menus.size() + 1;
            }
        }else{
            Tags.FREE_MENU_ADD_POSITION = menus.size();
        }
        for(String menu : menu_text){
            MenuData m = new MenuData();
            m.setType(Tags.MENU_VIEW_TYPE.MENU_TEXT);
            m.setName(menu);
            menus.add(m);
        }
        mAdapter.addAll(menus);
    }

    private void initNavigationMenu(ArrayList<MainResult.FreeLectureCate> cates,
                                        ArrayList<NoticeItemVO> notices){
        if(mAdapter.getItemCount() == 0 || Tags.FREE_MENU_ADD_POSITION == -1){
            return;
        }
        int freeCnt = 0;
        if(cates != null && cates.size() != 0) {
            ArrayList<MenuData> free_menus = new ArrayList<>();
            for (MainResult.FreeLectureCate cate : cates) {
                MenuData m = new MenuData();
                m.setType(Tags.MENU_VIEW_TYPE.MENU_TEXT);
                m.setName(cate.getFvodCateNm());
                free_menus.add(m);
            }
            mAdapter.addAll(Tags.FREE_MENU_ADD_POSITION, free_menus);
            freeCnt = free_menus.size();
        }

        if(ModooGong.hasExplainStudyBottom){
            Tags.MENU_INDEX.MENU_NOTICE = Tags.FREE_MENU_ADD_POSITION + freeCnt + 1;
        }else{
            Tags.MENU_INDEX.MENU_NOTICE = Tags.FREE_MENU_ADD_POSITION + freeCnt;
        }
        Tags.MENU_INDEX.MENU_ONE_TO_ONE_QA = Tags.MENU_INDEX.MENU_NOTICE + 1;

        int select = mAdapter.getSelect();
        if(select != -1 && select >= Tags.FREE_MENU_ADD_POSITION){
            mAdapter.setSelect(select + freeCnt);
        }

        if(notices != null && notices.size() != 0){
            mAdapter.updateNew(Tags.MENU_INDEX.MENU_NOTICE,
                                    BraveUtils.isNewBoard(notices.get(0).getWriteDate()));
        }
    }

    private void initMenuFragment(){
        if(mMenus == null){
            mMenus = new ArrayList<>();
        }else{
            mMenus.clear();
        }
        mMenus.add(LectureFragment.newInstance());
        mMenus.add(LocalLectureFragment.newInstance());
        if(ModooGong.hasStudyQA) {
            mMenus.add(StudyQAFragment.newInstance());
        }
        if(ModooGong.hasStudyFile){
            mMenus.add(StudyFileFragment.newInstance());
        }
        if(ModooGong.hasExplainStudy){
            if(ModooGong.hasExplainStudyNoTab){
                mMenus.add(FreeExplainStudyFragment.newInstance());
            }else{
                mMenus.add(FreeExplainLectureFragment.newInstance());
            }
        }
        mMenus.add(NoticeFragment.newInstance());
        mMenus.add(OneToOneQAFragment.newInstance());
    }

    private void initMenuFragment(ArrayList<MainResult.FreeLectureCate> cates){
        if(mMenus == null || Tags.FREE_MENU_ADD_POSITION == -1){
            return;
        }
        if(cates != null && cates.size() != 0) {
            ArrayList<Fragment> free_menus = new ArrayList<>();
            for (MainResult.FreeLectureCate cate : cates) {
                Fragment f;
                Bundle b = new Bundle();
                if(cate.getFvodScates() != null && cate.getFvodScates().size() != 0){
                    f = FreeLectureFragment.newInstance();
                    b.putString(Tags.TAG_CATE, BraveUtils.toJson(cate));
                }else{
                    f = FreeStudyFragment.newInstance();
                    b.putInt(Tags.TAG_CATE, cate.getFvodCate());
                }
                f.setArguments(b);
                free_menus.add(f);
            }
            mMenus.addAll(Tags.FREE_MENU_ADD_POSITION, free_menus);
        }
    }

    private void initTitle(){
        if(mTitles == null){
            mTitles = new ArrayList<>();
        }else{
            mTitles.clear();
        }
        ArrayList<String> toolbar_title = new ArrayList<>();
        for(int i=0; i<mAdapter.getItemCount(); i++){
            MenuData menu = mAdapter.getItem(i);
            toolbar_title.add(menu.getName());
        }
        mTitles.addAll(toolbar_title);
    }

    private void initTitle(ArrayList<MainResult.FreeLectureCate> cates){
        if(mTitles == null || Tags.FREE_MENU_ADD_POSITION == -1){
            return;
        }
        if(cates != null && cates.size() != 0) {
            ArrayList<String> free_titles = new ArrayList<>();
            for (MainResult.FreeLectureCate cate : cates) {
                free_titles.add(cate.getFvodCateNm());
            }
            mTitles.addAll(Tags.FREE_MENU_ADD_POSITION, free_titles);
        }
    }

    private void initUser(){
        if(mMainResult == null){
            setNavigationHeaderState(Tags.NAV_HEADER_TYPE.NAV_HEADER_MENU_REFRESH);
        }else{
            if(!BraveUtils.checkUserInfo()){
                setNavigationHeaderState(Tags.NAV_HEADER_TYPE.NAV_HEADER_NO_LOGIN);
            }else{
                setNavigationHeaderState(Tags.NAV_HEADER_TYPE.NAV_HEADER_LOGIN);
            }
        }
    }

    private void loadData(){
        startLoading();
        StudyRequests.getInstance().requestFreeLectureCateList(new OnResultListener<MainResult>(){

            @Override
            public void onSuccess(Request request, MainResult result) {
                stopLoading();
                setData(result);
            }

            @Override
            public void onFail(Request request, Exception exception) {
                stopLoading();
                BraveUtils.showRequestOnFailToast(MainActivity.this, exception);

                if(exception != null && exception.getMessage() != null) {
                    Answers.getInstance().logCustom(new CustomEvent(AnalysisTags.REQUEST)
                            .putCustomAttribute(AnalysisTags.ERROR, AnalysisTags.MAIN + ":: " + exception.getMessage()));
                }

                setData();
            }
        });
    }

    private void setData(){
        if(mAdapter.getItemCount() == 0) {
            initNavigationMenu();
        }
        if(mMenus == null) {
            initMenuFragment();
        }
        initTitle();
        /*
        if(mTitles == null) {
            initTitle();
        }
        */

        resetFragment(mAdapter.getSelect());
    }

    private void setData(MainResult result){
        if(result == null){
            return;
        }
        mMainResult = result;
        ArrayList<NoticeItemVO> notices = result.getNotiLists().getEpilogues();
        ArrayList<MainResult.FreeLectureCate> cates = result.getFvodCates();

        if(mAdapter.getItemCount() == 0){
            initNavigationMenu();
        }
        initNavigationMenu(cates, notices);
        if(mMenus == null || btnMenuRefresh.isActivated()){
            initMenuFragment();
        }
        initMenuFragment(cates);
        initTitle();
        /*
        if(mTitles == null){
            initTitle();
        }
        initTitle(cates);
        */

        loadLectureData();
    }

    private void loadLectureData(){
        if(!BraveUtils.checkUserInfo()){
            resetFragment(mAdapter.getSelect());
            return;
        }
        startLoading();
        StudyRequests.getInstance().requestLectureList(1, APIConfig.LECTURE_STATE_TYPE.LECTURE_ING_ITEM,
                1000, new OnResultListener<LectureResult>(){

            @Override
            public void onSuccess(Request request, LectureResult result) {
                stopLoading();
                setLectureData(result);
            }

            @Override
            public void onFail(Request request, Exception exception) {
                stopLoading();
                //BraveUtils.showToast(MainActivity.this, getString(R.string.toast_common_network_fail));
                if(exception != null && exception.getMessage() != null) {
                    Answers.getInstance().logCustom(new CustomEvent(AnalysisTags.REQUEST)
                            .putCustomAttribute(AnalysisTags.ERROR, AnalysisTags.MAIN + ":: " + exception.getMessage()));
                }

                resetFragment(mAdapter.getSelect());
            }
        });
    }

    private void setLectureData(LectureResult result) {
        if(result == null){
            return;
        }
        mLectureResult = result;
        if(mDownloadManager != null) {
            ArrayList<LectureItemVO> lectures = result.getStudies();
            mDownloadManager.updateDownloadLecture(lectures);
            updateDownloadViews();
        }

        resetFragment(mAdapter.getSelect());

        reStartDownloadWithDialog();
    }

    private void reStartDownloadWithDialog(){
        if(mDrawer.isDrawerOpen(GravityCompat.START)){
            return;
        }
        int position = mAdapter.getSelect();
        if(position == Tags.MENU_INDEX.MENU_LECTURE || position == Tags.MENU_INDEX.MENU_DOWN_LECTURE) {
            if (mDownloadManager != null) {
                int count = mDownloadManager.getDownloadPauseStudyCount();
                if (count != -1) {
                    String message = getString(R.string.dialog_restart_download);
                    BraveUtils.showAlertDialogOkCancel(MainActivity.this, message,
                            new MaterialDialog.SingleButtonCallback() {
                                @Override
                                public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                    mDownloadManager.reStartDownload();

                                    Answers.getInstance().logCustom(new CustomEvent(AnalysisTags.MAIN)
                                            .putCustomAttribute(AnalysisTags.ACTION, "restart_download"));
                                }
                            }, null);
                }
            }
        }
    }

    @Override
    public void onBackPressed() {
        if (mDrawer.isDrawerOpen(GravityCompat.START)) {
            mDrawer.closeDrawer(GravityCompat.START);
        }else {
            if(BraveUtils.checkUserInfo()) {
                if (mDownloadManager != null) {
                    int count = mDownloadManager.getDownloadingStudyCount();
                    if (count != -1) {
                        BraveUtils.showAlertDialogOkCancel(MainActivity.this, getString(R.string.dialog_download_exit_guide),
                                new MaterialDialog.SingleButtonCallback() {
                                    @Override
                                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                        startLoading();
                                        mDownloadManager.forceFinishDownload(false, new OnDownloadDataListener<String>() {
                                            @Override
                                            public void onDataProgress(int progress) {
                                            }

                                            @Override
                                            public void onDataComplete(String result) {
                                                stopLoading();
                                                callBackPressed();
                                            }

                                            @Override
                                            public void onDataError(int error) {
                                                stopLoading();
                                                BraveUtils.showLoadOnFailToast(MainActivity.this);
                                            }
                                        });
                                    }
                                }, null);
                    } else {
                        BackPressedHandler.onBackPressed(MainActivity.this, getString(R.string.toast_common_guide_finish));
                    }
                }
            }else{
                BackPressedHandler.onBackPressed(MainActivity.this, getString(R.string.toast_common_guide_finish));
            }
        }
    }

    private void callBackPressed(){
        super.onBackPressed();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);
    }

    private void goMenuItemSelected(int position){
        mAdapter.setSelect(position);

        setToolbar(position);
        setFragment(position);
        if(!btnMenuRefresh.isActivated()) {
            closeDrawer();
        }else{
            btnMenuRefresh.setActivated(false);
        }
    }

    private void closeDrawer(){
        if(mDrawer.isDrawerOpen(GravityCompat.START)) {
            mDrawer.closeDrawer(GravityCompat.START);
        }
    }

    private void setToolbar(int position){
        if(mTitles == null){
            return;
        }
        //set Toolbar
        TextView title = (TextView)mToolbar.findViewById(R.id.toolbar_title);
        title.setText(mTitles.get(position));

        if(checkNetwork(position) && checkLogin(position)) {
            if(position == Tags.MENU_INDEX.MENU_STUDY_QA ||
                    position == Tags.MENU_INDEX.MENU_ONE_TO_ONE_QA){
                btnOk.setVisibility(View.VISIBLE);
            }else{
                btnOk.setVisibility(View.GONE);
            }
        }else{
            btnOk.setVisibility(View.GONE);
        }
    }

    public String getToolbarTitle(){
        if(mTitles == null || mAdapter.getSelect() == -1){
            return null;
        }
        return mTitles.get(mAdapter.getSelect());
    }

    private void setFragment(int position){
        //set Fragment
        Fragment menu = null;
        CustomEvent event = new CustomEvent(AnalysisTags.MAIN);
        if(!checkNetwork(position)){
            if(position == Tags.MENU_INDEX.MENU_DOWN_LECTURE){
                menu = LocalLectureFragment.newInstance();

                event.putCustomAttribute(AnalysisTags.ACTION, "go_local_lecture_no_network");
            }else{
                menu = NoNetworkFragment.newInstance();

                event.putCustomAttribute(AnalysisTags.ACTION, "go_no_network");
            }
        }else if(!checkLogin(position)){
            if(position == Tags.MENU_INDEX.MENU_DOWN_LECTURE){
                menu = LocalLectureFragment.newInstance();

                event.putCustomAttribute(AnalysisTags.ACTION, "go_local_lecture_no_login");
            }else{
                menu = NoLoginFragment.newInstance();

                event.putCustomAttribute(AnalysisTags.ACTION, "go_no_login");
            }
        }else{
            if(mMenus != null) {

                menu = mMenus.get(position);

                String action;
                if (position == Tags.MENU_INDEX.MENU_LECTURE) {
                    action = "go_lecture";
                } else if (position == Tags.MENU_INDEX.MENU_DOWN_LECTURE) {
                    action = "go_down_lecture";
                } else if (position == Tags.MENU_INDEX.MENU_STUDY_QA) {
                    action = "go_study_qa";
                }else if(position == Tags.MENU_INDEX.MENU_STUDY_FILE){
                    action = "go_study_file";
                }else if(position == Tags.MENU_INDEX.MENU_NOTICE){
                    action = "go_notice";
                }else if(position == Tags.MENU_INDEX.MENU_ONE_TO_ONE_QA){
                    action = "go_one_to_one_qa";
                }else{
                    action = "go_free_lecture";
                }

                event.putCustomAttribute(AnalysisTags.ACTION, action);
            }
        }
        Answers.getInstance().logCustom(event);

        if(menu != null) {
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.container, menu)
                    .commitAllowingStateLoss();
                    //.commit();
        }
    }

    public void resetFragment(){
        if(mMainResult == null){
            initData();
        }else {
            if(mLectureResult == null){
                loadLectureData();
            }else{
                resetFragment(mAdapter.getSelect());
            }
        }
    }

    public void resetFragment(int position){
        initUser();
        if(position == -1){
            position = Tags.MENU_INDEX.MENU_LECTURE;
        }
        goMenuItemSelected(position);
    }

    private boolean checkLogin(int position){

        if(!BraveUtils.checkUserInfo()) {
            if(position == Tags.MENU_INDEX.MENU_LECTURE ||
                    position == Tags.MENU_INDEX.MENU_DOWN_LECTURE ||
                    position == Tags.MENU_INDEX.MENU_STUDY_QA ||
                    position == Tags.MENU_INDEX.MENU_STUDY_FILE ||
                    position == Tags.MENU_INDEX.MENU_ONE_TO_ONE_QA){
                return false;
            }else{
                return true;
            }
        }

        return true;
    }

    private boolean checkNetwork(int position){

        if(!SystemUtils.isNetworkConnected(getApplicationContext())) {
            return false;
            /*
            if(position == Tags.MENU_INDEX.MENU_DOWN_LECTURE){
                return true;
            }else{
                return false;
            }
            */
        }

        return true;
    }

    private void setNavigationHeaderState(int state){
        switch (state){
            case Tags.NAV_HEADER_TYPE.NAV_HEADER_LOGIN:
                layoutLogin.setVisibility(View.VISIBLE);
                layoutNoLogin.setVisibility(View.GONE);
                layoutMenuRefresh.setVisibility(View.GONE);

                txtName.setText(PropertyManager.getInstance().getUserName());
                btnSetting.setVisibility(View.VISIBLE);
                break;
            case Tags.NAV_HEADER_TYPE.NAV_HEADER_NO_LOGIN:
                layoutLogin.setVisibility(View.GONE);
                layoutNoLogin.setVisibility(View.VISIBLE);
                layoutMenuRefresh.setVisibility(View.GONE);
                btnSetting.setVisibility(View.GONE);
                break;
            case Tags.NAV_HEADER_TYPE.NAV_HEADER_MENU_REFRESH:
                layoutLogin.setVisibility(View.GONE);
                layoutNoLogin.setVisibility(View.GONE);
                layoutMenuRefresh.setVisibility(View.VISIBLE);
                btnSetting.setVisibility(View.GONE);
                break;
        }
    }

    public void setVisibilityOkButton(int visible){
        btnOk.setVisibility(visible);
    }

    private View.OnClickListener mOnOkBtnClickListener;
    public void setOnOkBtnClickListener(View.OnClickListener listener) {
        mOnOkBtnClickListener = listener;
    }

    @Override
    protected void updateDownloadViews(String studyKey, int state, int percent, int errorCode) {
    }

}

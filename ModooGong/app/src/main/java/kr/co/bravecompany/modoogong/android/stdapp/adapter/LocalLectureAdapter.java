package kr.co.bravecompany.modoogong.android.stdapp.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import kr.co.bravecompany.modoogong.android.stdapp.R;
import kr.co.bravecompany.modoogong.android.stdapp.db.model.Lecture;
import kr.co.bravecompany.modoogong.android.stdapp.viewholder.LocalLectureItemViewHolder;
import kr.co.bravecompany.modoogong.android.stdapp.viewholder.OnItemClickListener;
import kr.co.bravecompany.modoogong.android.stdapp.data.LocalLectureData;

/**
 * Created by BraveCompany on 2016. 10. 24..
 */

public class LocalLectureAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>{

    private List<LocalLectureData> items = new ArrayList<LocalLectureData>();

    public LocalLectureData getItem(int position){
        return items.get(position);
    }

    public int getItemWithStudyLectureNo(int studyLectureNo){
        for(int i=0; i<items.size(); i++){
            Lecture item = items.get(i).getLectureVO();
            if(studyLectureNo == item.getStudyLectureNo()){
                return i;
            }
        }
        return -1;
    }

    public void clear() {
        items.clear();
        notifyDataSetChanged();
    }

    public void add(LocalLectureData LocalLectureData) {
        items.add(LocalLectureData);
        notifyDataSetChanged();
    }

    public void addAll(List<LocalLectureData> items) {
        this.items.addAll(items);
        notifyDataSetChanged();
    }


    private OnItemClickListener mListener;
    public void setOnItemClickListener(OnItemClickListener listener) {
        mListener = listener;
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.view_lecture_item, parent, false);
        return new LocalLectureItemViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        LocalLectureItemViewHolder h = (LocalLectureItemViewHolder)holder;
        h.setLocalLectureItem(items.get(position));
        h.setOnItemClickListener(mListener);
    }

    @Override
    public int getItemCount() {
        return items.size();
    }
}

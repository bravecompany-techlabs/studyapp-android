package kr.co.bravecompany.modoogong.android.stdapp.utils;

import android.animation.ValueAnimator;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.LinearInterpolator;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;

import kr.co.bravecompany.modoogong.android.stdapp.R;
import kr.co.bravecompany.modoogong.android.stdapp.activity.MainActivity;

public class UIUtils {

    public static void startAnimation(ProgressBar progressBar, int start, int end, int time)
    {
        int width = progressBar.getWidth();
        progressBar.setMax(100);
        ValueAnimator animator = ValueAnimator.ofInt(start, end);
        animator.setInterpolator(new LinearInterpolator());
        animator.setStartDelay(0);
        animator.setDuration(time);

        ProgressAnimator aniUpdater = new ProgressAnimator();
        aniUpdater.progressBar = progressBar;

        animator.addUpdateListener(aniUpdater);

        animator.start();
    }

    public static class ProgressAnimator implements  ValueAnimator.AnimatorUpdateListener
    {
        public  ProgressBar progressBar;

        @Override
        public void onAnimationUpdate(ValueAnimator valueAnimator) {
            int value = (int) valueAnimator.getAnimatedValue();
            progressBar.setProgress(value);
        }
    }

    public static void InputText(final Activity context, String title,String desc, String hintText, final InputResultListener listener)
    {
        // Set up the input
        final EditText input =new EditText(context);
        input.setHint(hintText);

        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(title);
        builder.setMessage(desc);
        builder.setView(input);



        // Set up the buttons
        builder.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {


                InputMethodManager imm = (InputMethodManager)context.getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(input.getWindowToken(), 0);
                dialog.dismiss();
                String result =  input.getText().toString();
                MainActivity mainActivity = new MainActivity(); //MainActivity에 있는 mTodayMessageButton 과 mViewTodayMessageDivider를 전역으로 불러오기 위해서 생성.

                if(result.length() == 0) //입력된 텍스트가 하나도 없다면
                {
                    //result = "오늘의 화이팅 한마디";
                    //mainActivity.txtTodayMessage.setTextColor(Color.parseColor("#c0c0c0"));
                    mainActivity.mViewTodayMessageDivider.setVisibility(View.VISIBLE);
                    mainActivity.mTodayMessageButton.setVisibility(View.VISIBLE); // 오늘의 한미디 편집 아이콘 상시 노출로 수정[2019.10.17 테일러]
                }
                else //입력된 텍스트가 하나라도 있다면
                {
                    mainActivity.txtTodayMessage.setTextColor(Color.parseColor("#ffffff")); // 텍스트의 색상을 하얀색으로 변경
                    mainActivity.mViewTodayMessageDivider.setVisibility(View.INVISIBLE);
                    mainActivity.mTodayMessageButton.setVisibility(View.VISIBLE);
                }

                listener.onInputText(result);

                //input.clearFocus();
                //SystemUtils.hideSoftKeyboard(context);

            }
        });

        builder.setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                InputMethodManager imm = (InputMethodManager)context.getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(input.getWindowToken(), 0);

                dialog.cancel();

                //input.clearFocus();
                //SystemUtils.hideSoftKeyboard(context);
            }
        });

        builder.show();
    }

    public interface InputResultListener
    {
        void onInputText(String result);
    }

}

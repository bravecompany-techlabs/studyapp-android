package kr.co.bravecompany.modoogong.android.stdapp.activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import android.os.Bundle;

import com.google.android.material.navigation.NavigationView;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;

import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.crashlytics.android.answers.Answers;
import com.crashlytics.android.answers.CustomEvent;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

import de.hdodenhof.circleimageview.CircleImageView;
import kr.co.bravecompany.modoogong.android.stdapp.R;
import kr.co.bravecompany.modoogong.android.stdapp.adapter.MenuAdapter;
import kr.co.bravecompany.api.android.stdapp.api.OnResultListener;
import kr.co.bravecompany.api.android.stdapp.api.config.APIConfig;
import kr.co.bravecompany.api.android.stdapp.api.requests.StudyRequests;
import kr.co.bravecompany.api.android.stdapp.api.data.LectureItemVO;
import kr.co.bravecompany.api.android.stdapp.api.data.LectureResult;
import kr.co.bravecompany.api.android.stdapp.api.data.MainResult;
import kr.co.bravecompany.api.android.stdapp.api.data.NoticeItemVO;
import kr.co.bravecompany.modoogong.android.stdapp.application.ModooGong;
import kr.co.bravecompany.modoogong.android.stdapp.application.MyFirebaseMessagingService;
import kr.co.bravecompany.modoogong.android.stdapp.config.AnalysisTags;
import kr.co.bravecompany.modoogong.android.stdapp.config.RequestCode;
import kr.co.bravecompany.modoogong.android.stdapp.config.Tags;
import kr.co.bravecompany.modoogong.android.stdapp.download.DownloadService;
import kr.co.bravecompany.modoogong.android.stdapp.download.OnDownloadDataListener;
import kr.co.bravecompany.modoogong.android.stdapp.download.OnDownloadBindListener;
import kr.co.bravecompany.modoogong.android.stdapp.fragment.BaseFragment;
import kr.co.bravecompany.modoogong.android.stdapp.fragment.BookSalesFragment;
import kr.co.bravecompany.modoogong.android.stdapp.fragment.FreeExplainLectureFragment;
import kr.co.bravecompany.modoogong.android.stdapp.fragment.FreeExplainStudyFragment;
import kr.co.bravecompany.modoogong.android.stdapp.fragment.FreeStudyFragment;
import kr.co.bravecompany.modoogong.android.stdapp.fragment.LocalLectureFragment;
import kr.co.bravecompany.modoogong.android.stdapp.fragment.FreeLectureFragment;
import kr.co.bravecompany.modoogong.android.stdapp.pageLectureList.LectureFragment;
import kr.co.bravecompany.modoogong.android.stdapp.fragment.NoLoginFragment;
import kr.co.bravecompany.modoogong.android.stdapp.fragment.NoNetworkFragment;
import kr.co.bravecompany.modoogong.android.stdapp.fragment.NoticeFragment;
import kr.co.bravecompany.modoogong.android.stdapp.fragment.OneToOneQAFragment;
import kr.co.bravecompany.modoogong.android.stdapp.fragment.StudyFileFragment;
import kr.co.bravecompany.modoogong.android.stdapp.fragment.StudyQAFragment;
import kr.co.bravecompany.modoogong.android.stdapp.manager.PropertyManager;
import kr.co.bravecompany.modoogong.android.stdapp.utils.BackPressedHandler;
import kr.co.bravecompany.modoogong.android.stdapp.utils.PhotoSelector;
import kr.co.bravecompany.modoogong.android.stdapp.utils.SystemUtils;
import kr.co.bravecompany.modoogong.android.stdapp.utils.BraveUtils;
import kr.co.bravecompany.modoogong.android.stdapp.utils.UIUtils;
import kr.co.bravecompany.modoogong.android.stdapp.viewholder.OnItemClickListener;
import kr.co.bravecompany.modoogong.android.stdapp.data.MenuData;
import okhttp3.Request;

public class MainActivity extends DownBaseActivity {

    ArrayList<Fragment> mMenus;
    ArrayList<String> mTitles;

    private Toolbar mToolbar;
    private TextView btnOk;
    private TextView btnLecReg;

    private DrawerLayout mDrawer;

    private NavigationView mNavigationView;
    private TextView txtName;
    private LinearLayout layoutLogin;
    private LinearLayout layoutNoLogin;
    private TextView btnLogin;
    private LinearLayout layoutMenuRefresh;
    private ImageView btnMenuRefresh;

    private RecyclerView mListView;
    private MenuAdapter mAdapter;
    private LinearLayoutManager mLayoutManager;

    private FrameLayout btnSetting;

    private MainResult mMainResult = null;
    private LectureResult mLectureResult = null;


    private CircleImageView mProfileImageView;
    private Button mProfileEditorButton;
    public static TextView txtTodayMessage;
    public static Button mTodayMessageButton; //UIUtils 클래스에서 사용하기 위해서 static으로 선언.
    public static View mViewTodayMessageDivider; //UIUtils 클래스에서 사용하기 위해서 static으로 선언.

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);

        mPhotoSelector.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {
            if(requestCode == RequestCode.REQUEST_LOGIN){
                boolean isNewId = data.getBooleanExtra(Tags.TAG_NEW_ID, false);
                if(isNewId) {
                    startLoading();
                    mDownloadManager.removeDownloadAll(new OnDownloadDataListener<String>() {
                        @Override
                        public void onDataProgress(int progress) {
                        }

                        @Override
                        public void onDataComplete(String result) {
                            stopLoading();
                            PropertyManager.getInstance().removeSettings();
                            resetFragment();
                        }

                        @Override
                        public void onDataError(int error) {
                            stopLoading();
                            BraveUtils.showLoadOnFailToast(MainActivity.this);
                        }
                    });
                }else{
                    resetFragment();
                }
            }
            else
            {
                List<Fragment> fragmentList = getSupportFragmentManager().getFragments();
                if (fragmentList != null)
                {
                    for(Fragment fragment : fragmentList){
                        fragment.onActivityResult(requestCode, resultCode, data);
                    }
                }
            }
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        Intent intent = new Intent(this, DownloadService.class);
        startService(intent);

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (mDownloadManager != null) {
            mDownloadManager.setOnDownloadBindListener(new OnDownloadBindListener() {
                @Override
                public void onBindComplete() {
                    boolean taskRemoved = PropertyManager.getInstance().isTaskRemoved();
                    if(taskRemoved) {
                        startLoading();
                        mDownloadManager.forceFinishDownload(true, new OnDownloadDataListener<String>() {
                            @Override
                            public void onDataProgress(int progress) {
                            }

                            @Override
                            public void onDataComplete(String result) {
                                stopLoading();
                                PropertyManager.getInstance().setTaskRemoved(false);

                                initLayout();
                                initListener();
                                //initData();
                            }

                            @Override
                            public void onDataError(int error) {
                                stopLoading();
                                BraveUtils.showLoadOnFailToast(MainActivity.this);
                            }
                        });
                    }else{
                        initLayout();
                        initListener();
                        //initData();
                    }
                }
            });
        }
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();

        Intent intent = new Intent(this, DownloadService.class);
        stopService(intent);
    }

    @Override
    protected void initLayout() {

        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        mToolbar.setTitle("");

        mToolbar.setOnClickListener(mToolbarClickListener);

        setSupportActionBar(mToolbar);

        btnOk = (TextView)mToolbar.findViewById(R.id.btnOk);
        btnLecReg = (TextView)mToolbar.findViewById(R.id.btnLecReg);

        mDrawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, mDrawer, mToolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        mDrawer.addDrawerListener(toggle);
        toggle.syncState();

        mNavigationView = (NavigationView) findViewById(R.id.nav_view);
        mNavigationView.setItemIconTintList(null);
        txtName = (TextView)mNavigationView.findViewById(R.id.txtName);
        txtTodayMessage = (TextView)mNavigationView.findViewById(R.id.todayMessage);
        layoutLogin = (LinearLayout)mNavigationView.findViewById(R.id.layoutLogin);
        layoutNoLogin = (LinearLayout)mNavigationView.findViewById(R.id.layoutNoLogin);
        btnLogin = (TextView)mNavigationView.findViewById(R.id.btnLogin);
        layoutMenuRefresh = (LinearLayout)mNavigationView.findViewById(R.id.layoutMenuRefresh);
        btnMenuRefresh = (ImageView)mNavigationView.findViewById(R.id.btnMenuRefresh);
        btnMenuRefresh.setActivated(false);

        mAdapter = new MenuAdapter();
        mListView = (RecyclerView)mNavigationView.findViewById(R.id.recyclerMenu);
        mListView.setAdapter(mAdapter);
        mLayoutManager = new LinearLayoutManager(getApplicationContext()){
            @Override
            public boolean canScrollVertically() {
                return false;
            }
        };
        mListView.setLayoutManager(mLayoutManager);

        btnSetting = (FrameLayout)mNavigationView.findViewById(R.id.btnSetting);

        mProfileImageView = (CircleImageView) mNavigationView.findViewById(R.id.profile_image);
        mProfileEditorButton = (Button) mNavigationView.findViewById(R.id.profile_image_editor);
        mTodayMessageButton = (Button) mNavigationView.findViewById(R.id.profile_todayMessage_image_editor); // 오늘의 메시지 펜 버튼
        mViewTodayMessageDivider = (View) mNavigationView.findViewById(R.id.todayMessageDividerID); // 오늘의 메시지 구분선
    }

    @Override
    protected void initListener() {

        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("TOOLBARCLICK","메뉴열림...");
                //hide keyboard
                SystemUtils.hideSoftKeyboard(MainActivity.this);

                mDrawer.openDrawer(GravityCompat.START);
            }
        });

        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mOnOkBtnClickListener.onClick(v);
            }
        });

        btnLecReg.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                onLegRegClicked(v);
            }
        });



        txtName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                doEditTodayMessage();
            }
        });

        //펜 버튼 누르면 오늘의 메시지 수정할수있게 설정

        mTodayMessageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                doEditTodayMessage();
            }
        });

        txtTodayMessage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               doEditTodayMessage();
            }
        });

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                BraveUtils.goLogin(MainActivity.this);
                closeDrawer();

                Answers.getInstance().logCustom(new CustomEvent(AnalysisTags.MAIN)
                        .putCustomAttribute(AnalysisTags.ACTION, "go_login"));
            }
        });

        btnMenuRefresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btnMenuRefresh.setActivated(true);
                resetFragment();

                Answers.getInstance().logCustom(new CustomEvent(AnalysisTags.MAIN)
                        .putCustomAttribute(AnalysisTags.ACTION, "refresh_menu"));
            }
        });

        mAdapter.setOnItemClickListener(new OnItemClickListener(){

            @Override
            public void onItemClick(View view, int position) {
                goMenuItemSelected(position);
            }
        });

        btnSetting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(BraveUtils.checkUserInfo()) {
                    startActivity(new Intent(MainActivity.this, SettingActivity.class));
                    closeDrawer();

                    Answers.getInstance().logCustom(new CustomEvent(AnalysisTags.MAIN)
                            .putCustomAttribute(AnalysisTags.ACTION, "go_setting"));
                }else{
                    BraveUtils.showToast(MainActivity.this, getString(R.string.toast_no_login));
                }
            }
        });

        if(mDownloadManager != null) {
            boolean isNewId = getIntent().getBooleanExtra(Tags.TAG_NEW_ID, false);
            if(isNewId) {
                startLoading();
                mDownloadManager.removeDownloadAll(new OnDownloadDataListener<String>() {
                    @Override
                    public void onDataProgress(int progress) {
                    }

                    @Override
                    public void onDataComplete(String result) {
                        stopLoading();
                        PropertyManager.getInstance().removeSettings();
                        initData();
                    }

                    @Override
                    public void onDataError(int error) {
                        stopLoading();
                        BraveUtils.showLoadOnFailToast(MainActivity.this);
                    }
                });
            }else {
                initData();
            }
        }

        mProfileEditorButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                doEditProfile();
            }
        });

        mProfileImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                doEditProfile();
            }
        });
    }

    private void initData(){
        loadData();
    }

    private void initNavigationMenu(){
        ArrayList<MenuData> menus = new ArrayList<MenuData>();
        ArrayList<String> menu_icon = new ArrayList<>(Arrays.asList(getResources().getStringArray(R.array.menu_icon)));

        if(ModooGong.hasStudyQA){
            menu_icon.add(getString(R.string.menu_study_qa));
        }

        if(ModooGong.hasStudyFile){
            menu_icon.add(getString(R.string.menu_study_file));
        }

        for(int i=0; i<menu_icon.size(); i++)
        {
            String menu = menu_icon.get(i);
            MenuData m = new MenuData();
            m.setType(Tags.MENU_VIEW_TYPE.MENU_ICON);
            m.setName(menu);

            if(i == Tags.MENU_INDEX.MENU_DOWN_LECTURE){
                m.setImage(R.drawable.down_icon);
            }else if(ModooGong.hasStudyFile && i == Tags.MENU_INDEX.MENU_STUDY_FILE){
                m.setImage(R.drawable.file_icon);
            }else{
                m.setImage(R.drawable.pen_icon);
            }

            m.menuIndex = menus.size();
            menus.add(m);
        }

        //Lee, 교재판매 페이지!
        {
            String menuName = "교재구매"; //용감한 북스 -> 교재구매로 텍스트명 변경[2019.10.24 테일러]
            MenuData m = new MenuData();
            m.setType(Tags.MENU_VIEW_TYPE.MENU_ICON);
            m.setName(menuName);
            m.setImage(R.drawable.ic_menu_book);
            m.menuIndex = menus.size();
            Tags.MENU_INDEX.MENU_BOOK_SALES =  m.menuIndex;
            menus.add(m);
        }


        ArrayList<String> menu_text = new ArrayList<>(Arrays.asList(getResources().getStringArray(R.array.menu_text)));

        if(ModooGong.hasExplainStudy){
            menu_text.add(0, getString(R.string.menu_explain_study));
            if(ModooGong.hasExplainStudyBottom) {
                Tags.FREE_MENU_ADD_POSITION = menus.size();
            }else{
                Tags.FREE_MENU_ADD_POSITION = menus.size() + 1;
            }
        }else{
            Tags.FREE_MENU_ADD_POSITION = menus.size();
        }

        for(String menu : menu_text){
            MenuData m = new MenuData();
            m.setType(Tags.MENU_VIEW_TYPE.MENU_TEXT);
            m.setName(menu);
            m.menuIndex = menus.size();
            menus.add(m);
        }

        mAdapter.addAll(menus);
    }

    private void initNavigationMenu(ArrayList<MainResult.FreeLectureCate> cates, ArrayList<NoticeItemVO> notices)
    {
        if(mAdapter.getItemCount() == 0 || Tags.FREE_MENU_ADD_POSITION == -1){
            return;
        }

        int freeCnt = 0;
        if(cates != null && cates.size() != 0) {
            ArrayList<MenuData> free_menus = new ArrayList<>();
            for (MainResult.FreeLectureCate cate : cates) {
                MenuData m = new MenuData();
                m.setType(Tags.MENU_VIEW_TYPE.MENU_TEXT);
                m.setName(cate.getFvodCateNm());
                m.menuIndex = Tags.FREE_MENU_ADD_POSITION + free_menus.size();
                free_menus.add(m);
            }
            mAdapter.addAll(Tags.FREE_MENU_ADD_POSITION, free_menus);
            freeCnt = free_menus.size();
        }

        if(ModooGong.hasExplainStudyBottom){
            Tags.MENU_INDEX.MENU_NOTICE = Tags.FREE_MENU_ADD_POSITION + freeCnt + 1;
        }else{
            Tags.MENU_INDEX.MENU_NOTICE = Tags.FREE_MENU_ADD_POSITION + freeCnt;
        }

        Tags.MENU_INDEX.MENU_ONE_TO_ONE_QA = Tags.MENU_INDEX.MENU_NOTICE + 1;
       // Tags.MENU_INDEX.MENU_BOOK_SALES = Tags.MENU_INDEX.MENU_ONE_TO_ONE_QA + 1;

        int select = mAdapter.getSelect();
        if(select != -1 && select >= Tags.FREE_MENU_ADD_POSITION){
            mAdapter.setSelect(select + freeCnt);
        }

        if(notices != null && notices.size() != 0){
            mAdapter.updateNew(Tags.MENU_INDEX.MENU_NOTICE,
                                    BraveUtils.isNewBoard(notices.get(0).getWriteDate()));
        }
    }

    private void initMenuFragment()
    {
        if(mMenus == null){
            mMenus = new ArrayList<>();
        }else{
            mMenus.clear();
        }

        mMenus.add(LectureFragment.newInstance());
        mMenus.add(LocalLectureFragment.newInstance());
        mMenus.add(BookSalesFragment.newInstance());

        if(ModooGong.hasStudyQA) {
            mMenus.add(StudyQAFragment.newInstance());
        }

        if(ModooGong.hasStudyFile){
            mMenus.add(StudyFileFragment.newInstance());
        }

        if(ModooGong.hasExplainStudy){
            if(ModooGong.hasExplainStudyNoTab){
                mMenus.add(FreeExplainStudyFragment.newInstance());
            }else{
                mMenus.add(FreeExplainLectureFragment.newInstance());
            }
        }

        mMenus.add(NoticeFragment.newInstance());
        mMenus.add(OneToOneQAFragment.newInstance());

    }

    private void initMenuFragment(ArrayList<MainResult.FreeLectureCate> cates){

        if(mMenus == null || Tags.FREE_MENU_ADD_POSITION == -1){
            return;
        }

        if(cates != null && cates.size() != 0) {
            ArrayList<Fragment> free_menus = new ArrayList<>();
            for (MainResult.FreeLectureCate cate : cates) {
                Fragment f;
                Bundle b = new Bundle();
                if(cate.getFvodScates() != null && cate.getFvodScates().size() != 0){
                    f = FreeLectureFragment.newInstance();
                    b.putString(Tags.TAG_CATE, BraveUtils.toJson(cate));
                }else{
                    f = FreeStudyFragment.newInstance();
                    b.putInt(Tags.TAG_CATE, cate.getFvodCate());
                }
                f.setArguments(b);
                free_menus.add(f);
            }
            mMenus.addAll(Tags.FREE_MENU_ADD_POSITION, free_menus);
        }
    }

    private void initTitle(){
        if(mTitles == null){
            mTitles = new ArrayList<>();
        }else{
            mTitles.clear();
        }
        ArrayList<String> toolbar_title = new ArrayList<>();
        for(int i=0; i<mAdapter.getItemCount(); i++){
            MenuData menu = mAdapter.getItem(i);
            toolbar_title.add(menu.getName());
        }
        mTitles.addAll(toolbar_title);
    }

    private void initTitle(ArrayList<MainResult.FreeLectureCate> cates){
        if(mTitles == null || Tags.FREE_MENU_ADD_POSITION == -1){
            return;
        }
        if(cates != null && cates.size() != 0) {
            ArrayList<String> free_titles = new ArrayList<>();
            for (MainResult.FreeLectureCate cate : cates) {
                free_titles.add(cate.getFvodCateNm());
            }
            mTitles.addAll(Tags.FREE_MENU_ADD_POSITION, free_titles);
        }
    }

    private void initUser(){
        if(mMainResult == null){
            setNavigationHeaderState(Tags.NAV_HEADER_TYPE.NAV_HEADER_MENU_REFRESH);
        }else{
            if(!BraveUtils.checkUserInfo()){
                setNavigationHeaderState(Tags.NAV_HEADER_TYPE.NAV_HEADER_NO_LOGIN);
            }else{
                setNavigationHeaderState(Tags.NAV_HEADER_TYPE.NAV_HEADER_LOGIN);
            }
        }
    }

    private void loadData(){
        startLoading();
        StudyRequests.getInstance().requestFreeLectureCateList(new OnResultListener<MainResult>(){

            @Override
            public void onSuccess(Request request, MainResult result)
            {
                stopLoading();
                setData(result);

                MyFirebaseMessagingService.initPushSystem(getApplicationContext());
            }

            @Override
            public void onFail(Request request, Exception exception) {
                stopLoading();
                BraveUtils.showRequestOnFailToast(MainActivity.this, exception);

                if(exception != null && exception.getMessage() != null) {
                    Answers.getInstance().logCustom(new CustomEvent(AnalysisTags.REQUEST)
                            .putCustomAttribute(AnalysisTags.ERROR, AnalysisTags.MAIN + ":: " + exception.getMessage()));
                }

                setData();
            }
        });
    }

    private void setData()
    {

        if(mAdapter.getItemCount() == 0) {
            initNavigationMenu();
        }

        if(mMenus == null) {
            initMenuFragment();
        }

        initTitle();

        /*
        if(mTitles == null) {
            initTitle();
        }
        */

        resetFragment(mAdapter.getSelect());

        MyFirebaseMessagingService.PushActionData startData =  MyFirebaseMessagingService.PopPushStartData();
        if(startData !=null)
            handlePushStartData(startData);
    }

    void handlePushStartData(MyFirebaseMessagingService.PushActionData startData)
    {
        Log.d("Push", String.format("handlePushStartData action:%s, param:%s, body:%s", startData.action, startData.param, startData.body));

        if(startData.action !=null) {
            if (startData.action.equals("open")) {

            } else if (startData.action.equals("store")) {
                BookSalesFragment.msGotoBookCode = startData.param;
                resetFragment( Tags.MENU_INDEX.MENU_BOOK_SALES);
            }
        }
    }

    private void setData(MainResult result){
        if(result == null){
            return;
        }
        mMainResult = result;
        ArrayList<NoticeItemVO> notices = result.getNotiLists().getEpilogues();
        ArrayList<MainResult.FreeLectureCate> cates = result.getFvodCates();

        if(mAdapter.getItemCount() == 0){
            initNavigationMenu();
        }
        initNavigationMenu(cates, notices);
        if(mMenus == null || btnMenuRefresh.isActivated()){
            initMenuFragment();
        }
        initMenuFragment(cates);
        initTitle();
        /*
        if(mTitles == null){
            initTitle();
        }
        initTitle(cates);
        */

        loadLectureData();

        MyFirebaseMessagingService.PushActionData startData =  MyFirebaseMessagingService.PopPushStartData();
        if(startData !=null)
            handlePushStartData(startData);
    }

    private void loadLectureData(){
        if(!BraveUtils.checkUserInfo()){
            resetFragment(mAdapter.getSelect());
            return;
        }
        startLoading();
        StudyRequests.getInstance().requestLectureList(1, APIConfig.LECTURE_STATE_TYPE.LECTURE_ING_ITEM,
                1000, new OnResultListener<LectureResult>(){

            @Override
            public void onSuccess(Request request, LectureResult result) {
                stopLoading();
                setLectureData(result);
            }

            @Override
            public void onFail(Request request, Exception exception) {
                stopLoading();
                //BraveUtils.showToast(MainActivity.this, getString(R.string.toast_common_network_fail));
                if(exception != null && exception.getMessage() != null) {
                    Answers.getInstance().logCustom(new CustomEvent(AnalysisTags.REQUEST)
                            .putCustomAttribute(AnalysisTags.ERROR, AnalysisTags.MAIN + ":: " + exception.getMessage()));
                }

                resetFragment(mAdapter.getSelect());
            }
        });
    }

    private void setLectureData(LectureResult result) {
        if(result == null){
            return;
        }
        mLectureResult = result;
        if(mDownloadManager != null) {
            ArrayList<LectureItemVO> lectures = result.getStudies();
            mDownloadManager.updateDownloadLecture(lectures);
            updateDownloadViews();
        }

        resetFragment(mAdapter.getSelect());

        reStartDownloadWithDialog();
    }

    private void reStartDownloadWithDialog(){
        if(mDrawer.isDrawerOpen(GravityCompat.START)){
            return;
        }
        int position = mAdapter.getSelect();
        if(position == Tags.MENU_INDEX.MENU_LECTURE || position == Tags.MENU_INDEX.MENU_DOWN_LECTURE) {
            if (mDownloadManager != null) {
                int count = mDownloadManager.getDownloadPauseStudyCount();
                if (count != -1) {
                    String message = getString(R.string.dialog_restart_download);
                    BraveUtils.showAlertDialogOkCancel(MainActivity.this, message,
                            new MaterialDialog.SingleButtonCallback() {
                                @Override
                                public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                    mDownloadManager.reStartDownload();

                                    Answers.getInstance().logCustom(new CustomEvent(AnalysisTags.MAIN)
                                            .putCustomAttribute(AnalysisTags.ACTION, "restart_download"));
                                }
                            }, null);
                }
            }
        }
    }

    @Override
    public void onBackPressed()
    {
        //! 교재판매 페이지 웹뷰에 백키 이벤트를 전달하기 위한 장치!
        List<Fragment> fragmentList = getSupportFragmentManager().getFragments();
        if (fragmentList != null)
        {
            //TODO: Perform your logic to pass back press here
            for(Fragment fragment : fragmentList){
                if(fragment instanceof OnHomeActivityEventListener){
                    if(((OnHomeActivityEventListener)fragment).onHomeBackPressed())
                    {
                       // super.onHomeBackPressed();
                        return;
                    }
                }
            }
        }

        if (mDrawer.isDrawerOpen(GravityCompat.START)) {
            mDrawer.closeDrawer(GravityCompat.START);
        }else {
            if(BraveUtils.checkUserInfo()) {
                if (mDownloadManager != null) {
                    int count = mDownloadManager.getDownloadingStudyCount();
                    if (count != -1) {
                        BraveUtils.showAlertDialogOkCancel(MainActivity.this, getString(R.string.dialog_download_exit_guide),
                                new MaterialDialog.SingleButtonCallback() {
                                    @Override
                                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                        startLoading();
                                        mDownloadManager.forceFinishDownload(false, new OnDownloadDataListener<String>() {
                                            @Override
                                            public void onDataProgress(int progress) {
                                            }

                                            @Override
                                            public void onDataComplete(String result) {
                                                stopLoading();
                                                callBackPressed();
                                            }

                                            @Override
                                            public void onDataError(int error) {
                                                stopLoading();
                                                BraveUtils.showLoadOnFailToast(MainActivity.this);
                                            }
                                        });
                                    }
                                }, null);
                    } else {
                        BackPressedHandler.onBackPressed(MainActivity.this, getString(R.string.toast_common_guide_finish));
                    }
                }
            }else{
                BackPressedHandler.onBackPressed(MainActivity.this, getString(R.string.toast_common_guide_finish));
            }
        }
    }

    private void callBackPressed(){
        super.onBackPressed();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);
    }

    private void goMenuItemSelected(int position){
        mAdapter.setSelect(position);

        setToolbar(position);
        setFragment(position);
        if(!btnMenuRefresh.isActivated()) {
            closeDrawer();
        }else{
            btnMenuRefresh.setActivated(false);
        }
    }

    private void closeDrawer(){
        if(mDrawer.isDrawerOpen(GravityCompat.START)) {
            mDrawer.closeDrawer(GravityCompat.START);
        }
    }

    private void setToolbar(int position){
        if(mTitles == null){
            return;
        }
        //set Toolbar
        TextView title = (TextView)mToolbar.findViewById(R.id.toolbar_title);
        title.setText(mTitles.get(position));

        /*
        if(checkNetwork(position) && checkLogin(position)) {
            if(position == Tags.MENU_INDEX.MENU_STUDY_QA ||
                    position == Tags.MENU_INDEX.MENU_ONE_TO_ONE_QA){
                btnOk.setVisibility(View.VISIBLE);
            }else{
                btnOk.setVisibility(View.GONE);
            }
        }else{
            btnOk.setVisibility(View.GONE);
        }
        */
    }

    public String getToolbarTitle(){
        if(mTitles == null || mAdapter.getSelect() == -1){
            return null;
        }
        return mTitles.get(mAdapter.getSelect());
    }

    private void setFragment(int position)
    {
        setToolbarAuxButton(0);

        //set Fragment
        Fragment menuFragment = null;
        CustomEvent event = new CustomEvent(AnalysisTags.MAIN);

        if(!checkNetwork(position))
        {
            if(position == Tags.MENU_INDEX.MENU_DOWN_LECTURE){
                menuFragment = LocalLectureFragment.newInstance();

                event.putCustomAttribute(AnalysisTags.ACTION, "go_local_lecture_no_network");
            }else{
                menuFragment = NoNetworkFragment.newInstance();

                event.putCustomAttribute(AnalysisTags.ACTION, "go_no_network");
            }
        }
        else if(!checkLogin(position))
        {
            if(position == Tags.MENU_INDEX.MENU_DOWN_LECTURE){
                menuFragment = LocalLectureFragment.newInstance();

                event.putCustomAttribute(AnalysisTags.ACTION, "go_local_lecture_no_login");
            }else{
                menuFragment = NoLoginFragment.newInstance();

                event.putCustomAttribute(AnalysisTags.ACTION, "go_no_login");
            }
        }
        else
        {

            if(mMenus != null)
            {
                menuFragment = mMenus.get(position);

                String action;

                if (position == Tags.MENU_INDEX.MENU_LECTURE) {
                    action = "go_lecture";
                } else if (position == Tags.MENU_INDEX.MENU_DOWN_LECTURE) {
                    action = "go_down_lecture";
                } else if (position == Tags.MENU_INDEX.MENU_STUDY_QA) {
                    action = "go_study_qa";
                }else if(position == Tags.MENU_INDEX.MENU_STUDY_FILE){
                    action = "go_study_file";
                }else if(position == Tags.MENU_INDEX.MENU_NOTICE){
                    action = "go_notice";
                }else if(position == Tags.MENU_INDEX.MENU_ONE_TO_ONE_QA) {
                    action = "go_one_to_one_qa";
                }else if(position == Tags.MENU_INDEX.MENU_BOOK_SALES) {
                    action = "go_book_sales";
                }else{
                    action = "go_free_lecture";
                }

                event.putCustomAttribute(AnalysisTags.ACTION, action);
            }
        }

        Answers.getInstance().logCustom(event);

        if(menuFragment != null) {
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.container, menuFragment)
                    .commitAllowingStateLoss();
                    //.commit();

            //Adhoc. Jongok, 이미 활성화된 프래그먼트에 대한 리프래쉬 이벤트를 발생시키기 위한 장치
            if( menuFragment instanceof BaseFragment ) {
                if(menuFragment.getHost() !=null)
                    ((BaseFragment) menuFragment).refreshFragment();
            }
        }
    }

    public void resetFragment(){
        if(mMainResult == null){
            initData();
        }else {
            if(mLectureResult == null){
                loadLectureData();
            }else{
                resetFragment(mAdapter.getSelect());
            }
        }
    }

    public void resetFragment(int position){
        initUser();
        if(position == -1){
            position = Tags.MENU_INDEX.MENU_LECTURE;
        }
        goMenuItemSelected(position);
    }

    private boolean checkLogin(int position){
        // 사용자의 로그인 정보를 체크하여서 로그인이 안되있다면 로그인 후 이용하라는 페이지 나오도록 하는 부분[2019.10.18 테일러]
        if(!BraveUtils.checkUserInfo()) {
            if(position == Tags.MENU_INDEX.MENU_LECTURE ||
                    position == Tags.MENU_INDEX.MENU_DOWN_LECTURE ||
                    position == Tags.MENU_INDEX.MENU_STUDY_QA ||
                    position == Tags.MENU_INDEX.MENU_STUDY_FILE ||
                    position == Tags.MENU_INDEX.MENU_BOOK_SALES ||
                    position == Tags.MENU_INDEX.MENU_ONE_TO_ONE_QA){
                return false;
            }else{
                return true;
            }
        }

        return true;
    }

    private boolean checkNetwork(int position){

        if(!SystemUtils.isNetworkConnected(getApplicationContext())) {
            return false;
            /*
            if(position == Tags.MENU_INDEX.MENU_DOWN_LECTURE){
                return true;
            }else{
                return false;
            }
            */
        }

        return true;
    }

    private void setNavigationHeaderState(int state){
        switch (state){
            case Tags.NAV_HEADER_TYPE.NAV_HEADER_LOGIN:
                layoutLogin.setVisibility(View.VISIBLE);
                layoutNoLogin.setVisibility(View.GONE);
                layoutMenuRefresh.setVisibility(View.GONE);

                txtName.setText(PropertyManager.getInstance().getUserName());
                btnSetting.setVisibility(View.VISIBLE);
                Log.d("menuState","ok.");
                initProfilePhoto();

                break;
            case Tags.NAV_HEADER_TYPE.NAV_HEADER_NO_LOGIN:
                layoutLogin.setVisibility(View.GONE);
                layoutNoLogin.setVisibility(View.VISIBLE);
                layoutMenuRefresh.setVisibility(View.GONE);
                btnSetting.setVisibility(View.GONE);
                break;
            case Tags.NAV_HEADER_TYPE.NAV_HEADER_MENU_REFRESH:
                layoutLogin.setVisibility(View.GONE);
                layoutNoLogin.setVisibility(View.GONE);
                layoutMenuRefresh.setVisibility(View.VISIBLE);
                btnSetting.setVisibility(View.GONE);
                break;
        }
    }

    /********************************************************************
     *
     * ToolBar 네비케이션 아이템 변경
     *
     ********************************************************************/

    View.OnClickListener mToolbarClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v)
        {
            List<Fragment> fragmentList = getSupportFragmentManager().getFragments();
            if (fragmentList != null)
            {
                //TODO: Perform your logic to pass back press here
                for(Fragment fragment : fragmentList){
                    if(fragment instanceof OnHomeActivityEventListener){ ((OnHomeActivityEventListener)fragment).onHomeTitleBarPressed();
                    }
                }
            }
        }
    };

    public void setToolbarTitle(String title)
    {
        TextView titleWidget = (TextView)mToolbar.findViewById(R.id.toolbar_title);
        titleWidget.setText(title);
    }

    public void setVisibilityOkButton(int visible){
        btnOk.setVisibility(visible);
    }

    private View.OnClickListener mOnOkBtnClickListener;

    public void setOnOkBtnClickListener(View.OnClickListener listener) {
        mOnOkBtnClickListener = listener;
    }

    @Override
    protected void updateDownloadViews(String studyKey, int state, int percent, int errorCode)
    {

    }

       private void onLegRegClicked(View view)
       {
           List<Fragment> fragmentList = getSupportFragmentManager().getFragments();
           if (fragmentList != null)
           {
               //TODO: Perform your logic to pass back press here
               for(Fragment fragment : fragmentList){
                   if(fragment instanceof OnHomeActivityEventListener){ ((OnHomeActivityEventListener)fragment).onHomeMenuLecRegClicked();
                   }
               }
           }
       }

    public void setToolbarAuxButton(int idx)
    {
        if(idx ==0)
        {
            btnOk.setVisibility(View.GONE);
            btnLecReg.setVisibility(View.GONE);
        }
        else if(idx ==1)
        {
            btnOk.setVisibility(View.VISIBLE);
            btnLecReg.setVisibility(View.GONE);
        }
        else if(idx ==2)
        {
            btnOk.setVisibility(View.GONE);
            btnLecReg.setVisibility(View.VISIBLE);
        }
    }

    /********************************************
     *
     *
     *
     ********************************************/

    PhotoSelector mPhotoSelector=new PhotoSelector();

    String [] todayMsgHints = {
//            "오늘만 살자",
//            "오늘은 국어 완강하자",
//            "12시간 공부하고 치맥하자",
//            "오늘은 영어 완강하자",
//            "대충 삽시다~네!",
//            "공부해서 남 주자",
//            "미션 완료하고 선물받자"
            "당신의 용감한 한마디를 적어주세요." // 메시지 힌트 내용 변경 [19.10.14 테일러]
    };

    String getTodayHint()
    {
        Random r = new Random();
        int i= r.nextInt(todayMsgHints.length);
        return todayMsgHints[i];
    }

    void initProfilePhoto() {
        txtName.setText(PropertyManager.getInstance().getUserName());

        String msg = PropertyManager.getInstance().getTodayMsg();
        if (msg.length() == 0) // 메시지 내용이 없으면
        {
            msg = "오늘의 용감한 한마디"; // "오늘의 화이팅 한마디" -> "오늘의 용감한 한마디"으로 변경 [19.10.14 테일러]
            txtTodayMessage.setTextColor(Color.parseColor("#c0c0c0")); // 접속시, "오늘의 화이팅 한마디"가 입력되어
                                                                                // 회색으로 색상 처리 하고 오늘의 한마디 작성 유도
            mViewTodayMessageDivider.setVisibility(View.VISIBLE); // 구분선만 보이게 하여 오늘의 한마디 작성하게 유도하는 바탕 표현
            mTodayMessageButton.setVisibility(View.VISIBLE);// 오늘의 한미디 편집 아이콘 상시 노출로 수정[2019.10.17 테일러]
        }
        else
            {
                mViewTodayMessageDivider.setVisibility(View.INVISIBLE); // 오늘의 한마디에 한글자라도 작성되면 구분선 안나오도록 처리
                mTodayMessageButton.setVisibility(View.VISIBLE);
            }


        txtTodayMessage.setText(msg);

        String photoPath =   PropertyManager.getInstance().getProfilePhotoPath();
        if(photoPath != null && photoPath.length() > 0 ) {
            mProfileImageView.setImageURI(Uri.parse(photoPath));
        }
    }

    void doEditTodayMessage()
    {
      //  String prevMsg = txtTodayMessage.getText().toString();
        String prevMsg = getTodayHint();

        UIUtils.InputText(this, "오늘의 용감한 한마디", "", prevMsg, new UIUtils.InputResultListener(){
            @Override
            public void onInputText(String msg)
            {
                txtTodayMessage.setText(msg);
                PropertyManager.getInstance().setTodayMsg(msg);

            }
        });
    }

    void doEditProfile()
    {
        mPhotoSelector.requestPhoto(this, mOnPhotoSelectedListener);
    }

    PhotoSelector.OnPhotoSelectedListener mOnPhotoSelectedListener = new PhotoSelector.OnPhotoSelectedListener()
    {
        @Override
        public void onPhotoSelected(String path, Bitmap selectedBitmap)
        {
            if(path != null && path.length() > 0 ) {
                mProfileImageView.setImageURI(Uri.parse(path));
                PropertyManager.getInstance().setProfilePhotoPath(path);


            }
        }
    };


    /*************************************************************
     *
     * Push!
     *
     *************************************************************/
//
//    void registerPushTokenToServer()
//    {
//        final String Tag = "Push";
//
//        if(BraveUtils.checkUserInfo()) {
//
//            FirebaseInstanceId.getInstance().getInstanceId()
//                    .addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
//                        @Override
//                        public void onComplete(@NonNull Task<InstanceIdResult> task) {
//                            if (!task.isSuccessful()) {
//                                Log.w(Tag, "getInstanceId failed", task.getException());
//                                return;
//                            }
//
//                            // Get new Instance ID token
//                            String token = task.getResult().getToken();
//                            String mobileKey = SystemUtils.getAndroidID(getApplicationContext());
//
//                            // Toast.makeText(MainActivity.this, msg, Toast.LENGTH_SHORT).show();
//
//                            ProfileRequests.getInstance().requestSetPushKey(token, mobileKey, new OnResultListener<Packet.ResponseResult>() {
//                                @Override
//                                public void onSuccess(Request request, Packet.ResponseResult result) {
//                                    Log.d(Tag, "push key store completed");
//                                }
//
//                                @Override
//                                public void onFail(Request request, Exception exception) {
//                                    Log.d(Tag, "push key store failed:" + exception.getMessage());
//                                }
//                            });
//
//                        }
//                    });
//        }
//        else
//        {
//
//        }
//    }


}

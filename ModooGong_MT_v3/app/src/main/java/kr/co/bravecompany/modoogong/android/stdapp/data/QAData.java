package kr.co.bravecompany.modoogong.android.stdapp.data;

import kr.co.bravecompany.api.android.stdapp.api.data.QAItemVO;

/**
 * Created by BraveCompany on 2016. 11. 11..
 */

public class QAData {
    private QAItemVO qaItemVO;

    public QAItemVO getQaItemVO() {
        return qaItemVO;
    }

    public void setQaItemVO(QAItemVO qaItemVO) {
        this.qaItemVO = qaItemVO;
    }
}

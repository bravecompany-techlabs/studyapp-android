package kr.co.bravecompany.modoogong.android.stdapp.data;

import kr.co.bravecompany.modoogong.android.stdapp.db.model.Lecture;

/**
 * Created by BraveCompany on 2016. 10. 24..
 */

public class LocalLectureData {

    private Lecture lectureVO;

    public Lecture getLectureVO() {
        return lectureVO;
    }

    public void setLectureVO(Lecture lectureVO) {
        this.lectureVO = lectureVO;
    }

}

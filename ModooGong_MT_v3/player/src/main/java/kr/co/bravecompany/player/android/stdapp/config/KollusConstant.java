package kr.co.bravecompany.player.android.stdapp.config;


public class KollusConstant {
    public static String KEY = "key";
//    public static final String EXPIRE_DATE = "2019/10/01";
    public static final String EXPIRE_DATE = "2020/10/31";

    public static int PORT = 0;

    /**
     * true : develop, false : release
     */
    public static boolean isDebug = false;

    public static void init(String key, int port, boolean debug){
        KollusConstant.KEY = key;
        KollusConstant.PORT = port;
        KollusConstant.isDebug = debug;
    }
}

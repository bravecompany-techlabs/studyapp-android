package kr.co.bravecompany.player.android.stdapp.video;

import android.app.Activity;
import android.content.res.Configuration;
import android.os.Handler;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import com.kollus.sdk.media.util.Utils;

import kr.co.bravecompany.player.android.stdapp.R;
import kr.co.bravecompany.player.android.stdapp.adapter.BookmarkAdapter;
import kr.co.bravecompany.player.android.stdapp.utils.BraveUtils;
import kr.co.bravecompany.player.android.stdapp.view.CustomSeekBar;
import kr.co.bravecompany.player.android.stdapp.view.CustomVerticalSeekBar;

/**
 * Created by BraveCompany on 2017. 3. 28..
 */

public class VideoControllerView {
    private Activity mContext;
    private View mRootView;

    private View mInitLayer;

    private TextView mTitle;
    private View mControlLayer;

    private ImageView mPlayPause;
    private ImageView mMute;
    private TextView mCurrentTime;
    private TextView mDurationTime;
    private SeekBar mSeekBar;
    private TextView mCurrentTimeSeek;
    private TextView mDurationTimeSeek;

    private ImageView btnRotationLock;
    private ImageView btnRotation;
    private ImageView btnFullScreen;
    private ImageView btnRateDown;
    private ImageView btnRateUp;
    private TextView txtRate;

    private SeekBar seekVolume;
    private ImageView skipPrev;
    private ImageView skipNext;
    private View layoutPlaySkip;
    private ImageView btnBooks;
    private ImageView btnBooksAdd;

    private View mBookmarkLayer;
    private View layoutBookWrite;
    private TextView bookCount;
    private TextView bookAdd;
    private TextView bookDelete;
    private TextView txtBookDefault;
    private View bookLoading;

    private RecyclerView mListView;
    private LinearLayoutManager mLayoutManager;
    private OnVideoPlayerListener mListener;

    private LinearLayout layoutSeek;
    private ImageView imgSeek;
    private TextView txtSeekTime;
    private LinearLayout layoutVolume;
    private TextView txtVolume;
    private LinearLayout layoutBright;
    private TextView txtBright;

    private Handler mHandler;
    public static final int FADE_OUT_TIME_SEC_CONTROLLER = 10000;
    public static final int FADE_OUT_TIME_SEC = 1000;
    private Animation mControllerHideAnimation;
    private Animation mSeekHideAnimation;
    private Animation mVolumeHideAnimation;
    private Animation mBrightHideAnimation;


    public VideoControllerView(Activity context, View rootView) {
        mContext = context;
        mRootView = rootView;

        initLayout();
        initListener();

        mHandler = new Handler();
        mControllerHideAnimation = AnimationUtils.loadAnimation(context, R.anim.fade_out_anim);
        mSeekHideAnimation = AnimationUtils.loadAnimation(context, R.anim.fade_out_anim);
        mVolumeHideAnimation = AnimationUtils.loadAnimation(context, R.anim.fade_out_anim);
        mBrightHideAnimation = AnimationUtils.loadAnimation(context, R.anim.fade_out_anim);
        mControllerHideAnimation.setAnimationListener(mAnimationListener);
        mSeekHideAnimation.setAnimationListener(mAnimationListener);
        mVolumeHideAnimation.setAnimationListener(mAnimationListener);
        mBrightHideAnimation.setAnimationListener(mAnimationListener);
    }

    private void initLayout() {
        mInitLayer = mRootView.findViewById(R.id.layoutLoading);

        mTitle = (TextView)mRootView.findViewById(R.id.title);
        mControlLayer = mRootView.findViewById(R.id.control_layer);
        mControlLayer.setSelected(false);

        mPlayPause = (ImageView)mRootView.findViewById(R.id.play_pause);
        mMute = (ImageView)mRootView.findViewById(R.id.mute);

        mCurrentTime = (TextView)mRootView.findViewById(R.id.currentTime);
        mDurationTime = (TextView)mRootView.findViewById(R.id.durationTime);
        mSeekBar = (CustomSeekBar)mRootView.findViewById(R.id.seek_bar);
        mCurrentTimeSeek = (TextView)mRootView.findViewById(R.id.currentTimeSeek);
        mDurationTimeSeek = (TextView)mRootView.findViewById(R.id.durationTimeSeek);

        btnRotationLock = (ImageView)mRootView.findViewById(R.id.btnRotationLock);
        //ver x.2.6
        btnRotationLock.setVisibility(View.GONE);
        btnRotation = (ImageView)mRootView.findViewById(R.id.btnRotation);
        btnFullScreen = (ImageView)mRootView.findViewById(R.id.btnFullScreen);
        btnRateDown = (ImageView)mRootView.findViewById(R.id.btnRateDown);
        btnRateUp = (ImageView)mRootView.findViewById(R.id.btnRateUp);
        txtRate = (TextView)mRootView.findViewById(R.id.txtRate);

        seekVolume = (CustomVerticalSeekBar)mRootView.findViewById(R.id.seekVolume);
        skipPrev = (ImageView)mRootView.findViewById(R.id.skipPrev);
        skipNext = (ImageView)mRootView.findViewById(R.id.skipNext);
        layoutPlaySkip = mRootView.findViewById(R.id.layoutPlaySkip);
        btnBooks = (ImageView)mRootView.findViewById(R.id.btnBooks);
        btnBooksAdd = (ImageView)mRootView.findViewById(R.id.btnBooksAdd);
        //ver x.1.12
        btnBooks.setVisibility(View.GONE);
        btnBooksAdd.setVisibility(View.GONE);

        mBookmarkLayer = mRootView.findViewById(R.id.bookmark_layer);
        layoutBookWrite = mRootView.findViewById(R.id.layoutBookWrite);
        bookCount = (TextView)mRootView.findViewById(R.id.bookCount);
        bookDelete = (TextView)mRootView.findViewById(R.id.bookDelete);
        bookAdd = (TextView)mRootView.findViewById(R.id.bookAdd);
        txtBookDefault = (TextView)mRootView.findViewById(R.id.txtBookDefault);
        bookLoading = mRootView.findViewById(R.id.bookLoading);

        mListView = (RecyclerView)mRootView.findViewById(R.id.recyclerBookmark);
        mLayoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false);
        mListView.setLayoutManager(mLayoutManager);

        layoutSeek = (LinearLayout)mRootView.findViewById(R.id.layoutSeek);
        imgSeek = (ImageView)mRootView.findViewById(R.id.imgSeek);
        txtSeekTime = (TextView)mRootView.findViewById(R.id.txtSeekTime);
        layoutVolume = (LinearLayout)mRootView.findViewById(R.id.layoutVolume);
        txtVolume = (TextView)mRootView.findViewById(R.id.txtVolume);
        layoutBright = (LinearLayout)mRootView.findViewById(R.id.layoutBright);
        txtBright = (TextView)mRootView.findViewById(R.id.txtBright);
    }

    private void initListener() {
        mPlayPause.setOnClickListener(mOnControllerClickListener);
        mMute.setOnClickListener(mOnControllerClickListener);
        btnRotationLock.setOnClickListener(mOnControllerClickListener);
        btnRotation.setOnClickListener(mOnControllerClickListener);
        btnFullScreen.setOnClickListener(mOnControllerClickListener);
        btnRateDown.setOnClickListener(mOnControllerClickListener);
        btnRateUp.setOnClickListener(mOnControllerClickListener);
        skipPrev.setOnClickListener(mOnControllerClickListener);
        skipNext.setOnClickListener(mOnControllerClickListener);

        btnBooks.setOnClickListener(mOnBookmarkClickListener);
        btnBooksAdd.setOnClickListener(mOnBookmarkClickListener);
        bookAdd.setOnClickListener(mOnBookmarkClickListener);
        bookDelete.setOnClickListener(mOnBookmarkClickListener);
    }

    public void setOnSeekBarChangeListener(SeekBar.OnSeekBarChangeListener listener){
        if(listener != null) {
            mSeekBar.setOnSeekBarChangeListener(listener);
        }
    }

    public void setOnVolumeSeekBarChangeListener(SeekBar.OnSeekBarChangeListener listener){
        if(listener != null) {
            seekVolume.setOnSeekBarChangeListener(listener);
        }
    }

    public void setOnVideoPlayerListener(OnVideoPlayerListener listener){
        mListener = listener;
    }

    public void startLoading(){
        mInitLayer.setVisibility(View.VISIBLE);
    }

    public void stopLoading(){
        mInitLayer.setVisibility(View.GONE);
    }

    public boolean isLoading(){
        return (mInitLayer.getVisibility() == View.VISIBLE);
    }

    // =============================================================================
    // Control Play
    // =============================================================================

    public boolean isSelectedControlLayer(){
        return mControlLayer.isSelected();
    }

    public void toggleControlView(){
        mControlLayer.setSelected(!mControlLayer.isSelected());
        if(mControlLayer.isSelected()){
            if(mListener != null && isSelectedBookmarklLayer()){
                mListener.onShowBookmarkView(false);
            }
            mControlLayer.setVisibility(View.VISIBLE);
            startControllerHiding();
        }else{
            mControlLayer.setVisibility(View.GONE);
            stopControllerHiding();
        }
    }

    public void setTitle(String title){
        mTitle.setText(title);
    }

    public void setSeekBarMax(int max){
        mSeekBar.setMax(max);
    }

    public void setSeekBarProgress(int progress){
        mSeekBar.setProgress(progress);
    }

    public void setPlayPauseActivated(boolean activated){
        mPlayPause.setActivated(activated);
    }

    public void setMuteActivated(boolean activated){
        mMute.setActivated(activated);
    }

    public void setRotationLockActivated(boolean activated){
        btnRotationLock.setActivated(activated);
    }

    public void setFullScreenActivated(boolean activated){
        btnFullScreen.setActivated(activated);
    }

    public void setTextRate(String rate){
        txtRate.setText(rate);
    }

    public void setCurrentTime(int current, int duration){
        int c = current/1000;
        int r = (duration - current)/1000;
        int d = duration/1000;
        String sCurrent = BraveUtils.formattingTime(c);
        String sDuration = String.format(mContext.getString(R.string.kollus_playing_time),
                BraveUtils.formattingTime(d), BraveUtils.formattingTime(r));
        mCurrentTime.setText(sCurrent);
        mDurationTime.setText(sDuration);
        mCurrentTimeSeek.setText(sCurrent);
        mDurationTimeSeek.setText(sDuration);
    }

    public void setSeekLabel(int seekTimeMs, int mountMs) {
        String time;
        if(seekTimeMs < 60*60*1000) {
            time = Utils.stringForTimeMMSS(seekTimeMs);
        }else {
            time = Utils.stringForTimeHMMSS(seekTimeMs);
        }
        String mount = Utils.stringForTimeMMSS(Math.abs(mountMs));

        txtSeekTime.setText(String.format(mContext.getString(R.string.kollus_playing_seek), time, mount));
        imgSeek.setActivated(mountMs < 0);

        startSeekHiding();

        if(layoutSeek.getVisibility() != View.VISIBLE) {
            layoutSeek.setVisibility(View.VISIBLE);
        }
    }

    public void setVolumeLabel(int level) {
        startVolumeHiding();

        if (layoutVolume.getVisibility() != View.VISIBLE) {
            layoutVolume.setVisibility(View.VISIBLE);
        }

        setCurrentVolumeSeek(level);
    }

    public void setTextVolume(int level){
        txtVolume.setText(String.valueOf(level));
    }

    public void setBrightnessLabel(int level) {
        txtBright.setText(String.valueOf(level));

        startBrightHiding();

        if (layoutBright.getVisibility() != View.VISIBLE) {
            layoutBright.setVisibility(View.VISIBLE);
        }
    }

    public void setMaxVolumeSeek(int max){
        seekVolume.setMax(max);
    }

    public void setCurrentVolumeSeek(int current){
        seekVolume.setProgress(current);
    }

    public void setVisibilityTimeView(int orientation){
        switch (orientation){
            case Configuration.ORIENTATION_LANDSCAPE:
                mCurrentTime.setVisibility(View.GONE);
                mDurationTime.setVisibility(View.GONE);
                mCurrentTimeSeek.setVisibility(View.VISIBLE);
                mDurationTimeSeek.setVisibility(View.VISIBLE);
                break;
            case Configuration.ORIENTATION_PORTRAIT:
                mCurrentTime.setVisibility(View.VISIBLE);
                mDurationTime.setVisibility(View.VISIBLE);
                mCurrentTimeSeek.setVisibility(View.GONE);
                mDurationTimeSeek.setVisibility(View.GONE);
                break;
        }
    }

    // =============================================================================
    // Bookmark
    // =============================================================================

    public void setBookmarkAdapter(BookmarkAdapter adapter){
        if(mListView != null) {
            mListView.setAdapter(adapter);
        }
    }

    public boolean isSelectedBookmarklLayer(){
        return (mBookmarkLayer.getVisibility() == View.VISIBLE);
    }

    public void showBookmarkView(boolean show){
        if(show){
            if(isSelectedControlLayer()){
                toggleControlView();
            }
            BraveUtils.setVisibilityBottomViewLong(mBookmarkLayer, View.VISIBLE);
        }else{
            if(!isSelectedControlLayer()){
                toggleControlView();
            }
            if(bookDelete.isActivated()){
                toggleBookmarkDelete();
            }
            BraveUtils.setVisibilityBottomViewLong(mBookmarkLayer, View.GONE);
        }
    }

    public void setBookmarkWritable(boolean writable){
        if(writable){
            layoutBookWrite.setVisibility(View.VISIBLE);
        }else{
            layoutBookWrite.setVisibility(View.GONE);
        }
    }

    public void setBookmarkCount(int count){
        bookCount.setText(String.format(mContext.getString(R.string.kollus_bookmark_count), count));
        boolean validCount = (count > 0);
        showBookmarkDefault(!validCount, -1);
        if(!validCount && bookDelete.isActivated()){
            toggleBookmarkDelete();
        }
        bookDelete.setEnabled(validCount);
    }

    public void setBookmarkSelected(int index) {
        if(index == -1){
            return;
        }
        mListView.smoothScrollToPosition(index);
    }

    public void showBookmarkDefault(boolean show, int nErrorCode){
        if(show){
            setTextBookDefault(nErrorCode);
            txtBookDefault.setVisibility(View.VISIBLE);
            mListView.setVisibility(View.GONE);
        }else{
            txtBookDefault.setVisibility(View.GONE);
            mListView.setVisibility(View.VISIBLE);
        }
    }

    private void toggleBookmarkDelete(){
        bookDelete.setActivated(!bookDelete.isActivated());
        boolean activate = bookDelete.isActivated();
        mListener.onBookmarkDelete(activate);
        bookAdd.setEnabled(!activate);
    }

    private void setTextBookDefault(int nErrorCode){
        if(nErrorCode == -1) {
            txtBookDefault.setText(mContext.getString(R.string.no_kollus_bookmark));
        }else{
            txtBookDefault.setText(String.format(mContext.getString(R.string.kollus_bookmark_setup_failed), nErrorCode));
        }
    }

    public void startBookLoading(){
        bookLoading.setVisibility(View.VISIBLE);
    }

    public void stopBookLoading(){
        bookLoading.setVisibility(View.GONE);
    }

    public boolean isBookLoading(){
        return (bookLoading.getVisibility() == View.VISIBLE);
    }

    // =============================================================================
    // Hiding
    // =============================================================================

    public void startControllerHiding() {
        stopControllerHiding();
        mHandler.postDelayed(mStartControllerHidingRunnable, FADE_OUT_TIME_SEC_CONTROLLER);
    }

    private void stopControllerHiding() {
        mHandler.removeCallbacks(mStartControllerHidingRunnable);
        mControlLayer.setAnimation(null);
    }

    private void startSeekHiding() {
        stopSeekHiding();

        if(layoutPlaySkip.getVisibility() != View.GONE){
            layoutPlaySkip.setVisibility(View.GONE);
        }
        if(layoutVolume.getVisibility() != View.GONE) {
            stopVolumeHiding();
            layoutVolume.setVisibility(View.GONE);
        }
        if(layoutBright.getVisibility() != View.GONE) {
            stopBrightHiding();
            layoutBright.setVisibility(View.GONE);
        }

        mHandler.postDelayed(mStartSeekHidingRunnable, FADE_OUT_TIME_SEC);
    }

    private void stopSeekHiding() {
        mHandler.removeCallbacks(mStartSeekHidingRunnable);
        layoutSeek.setAnimation(null);
    }

    private void startVolumeHiding() {
        stopVolumeHiding();

        if(layoutPlaySkip.getVisibility() != View.GONE){
            layoutPlaySkip.setVisibility(View.GONE);
        }
        if(layoutSeek.getVisibility() != View.GONE) {
            stopSeekHiding();
            layoutSeek.setVisibility(View.GONE);
        }
        if(layoutBright.getVisibility() != View.GONE) {
            stopBrightHiding();
            layoutBright.setVisibility(View.GONE);
        }

        mHandler.postDelayed(mStartVolumeHidingRunnable, FADE_OUT_TIME_SEC);
    }

    private void stopVolumeHiding() {
        mHandler.removeCallbacks(mStartVolumeHidingRunnable);
        layoutVolume.setAnimation(null);
    }

    private void startBrightHiding() {
        stopBrightHiding();

        if(layoutPlaySkip.getVisibility() != View.GONE){
            layoutPlaySkip.setVisibility(View.GONE);
        }
        if(layoutSeek.getVisibility() != View.GONE) {
            stopSeekHiding();
            layoutSeek.setVisibility(View.GONE);
        }
        if(layoutVolume.getVisibility() != View.GONE) {
            stopVolumeHiding();
            layoutVolume.setVisibility(View.GONE);
        }

        mHandler.postDelayed(mStartBrightHidingRunnable, FADE_OUT_TIME_SEC);
    }

    private void stopBrightHiding() {
        mHandler.removeCallbacks(mStartBrightHidingRunnable);
        layoutBright.setAnimation(null);
    }

    // =============================================================================
    // Runnables
    // =============================================================================

    private final Runnable mStartControllerHidingRunnable = new Runnable() {
        @Override
        public void run() {
            if (mControlLayer.getVisibility() == View.VISIBLE) {
                mControlLayer.startAnimation(mControllerHideAnimation);
            }
        }
    };

    private final Runnable mStartSeekHidingRunnable = new Runnable() {
        @Override
        public void run() {
            if (layoutSeek.getVisibility() == View.VISIBLE) {
                layoutSeek.startAnimation(mSeekHideAnimation);
            }
        }
    };

    private final Runnable mStartVolumeHidingRunnable = new Runnable() {
        @Override
        public void run() {
            if (layoutVolume.getVisibility() == View.VISIBLE) {
                layoutVolume.startAnimation(mVolumeHideAnimation);
            }
        }
    };

    private final Runnable mStartBrightHidingRunnable = new Runnable() {
        @Override
        public void run() {
            if (layoutBright.getVisibility() == View.VISIBLE) {
                layoutBright.startAnimation(mBrightHideAnimation);
            }
        }
    };

    // =============================================================================
    // View Listener
    // =============================================================================

    private View.OnClickListener mOnControllerClickListener = new View.OnClickListener() {

        @Override
        public void onClick(View view) {
            if(mListener == null){
                return;
            }
            //Controller
            if(view == mPlayPause) {
                mListener.onPlay();
            }else if(view == mMute) {
                mListener.onMute(!view.isActivated());
            }else if(view == btnRotationLock){
                mListener.onScreenRotateLock(!view.isActivated());
            }else if(view == btnRotation){
                mListener.onScreenRotate();
            }else if(view == btnFullScreen){
                mListener.onFullScreen(!view.isActivated());
            }else if(view == btnRateDown){
                mListener.onPlayingRate(false);
            }else if(view == btnRateUp){
                mListener.onPlayingRate(true);
            }else if(view == skipPrev){
                mListener.onPlaySkipPrev();
            }else if(view == skipNext) {
                mListener.onPlaySkipNext();
            }
            startControllerHiding();
        }
    };

    private View.OnClickListener mOnBookmarkClickListener = new View.OnClickListener() {

        @Override
        public void onClick(View view) {
            if(mListener == null){
                return;
            }
            //Bookmark
            if(view == btnBooks){
                mListener.onShowBookmarkView(true);
            }else if(view == btnBooksAdd){
                mListener.onBookmarkAdd();
            }else if(view == bookDelete){
                if(!isBookLoading()) {
                    toggleBookmarkDelete();
                }
            }else if(view == bookAdd){
                if(!isBookLoading()) {
                    mListener.onBookmarkAdd();
                }
            }
        }
    };

    private Animation.AnimationListener mAnimationListener = new Animation.AnimationListener() {
        @Override
        public void onAnimationStart(Animation animation) {
        }

        @Override
        public void onAnimationEnd(Animation animation) {
            if(animation == mControllerHideAnimation){
                mControlLayer.setVisibility(View.GONE);
                mControlLayer.setSelected(false);
            }else if(animation == mSeekHideAnimation){
                layoutSeek.setVisibility(View.GONE);
                layoutPlaySkip.setVisibility(View.VISIBLE);
            }else if(animation == mVolumeHideAnimation){
                layoutVolume.setVisibility(View.GONE);
                layoutPlaySkip.setVisibility(View.VISIBLE);
            }else if(animation == mBrightHideAnimation){
                layoutBright.setVisibility(View.GONE);
                layoutPlaySkip.setVisibility(View.VISIBLE);
            }
        }

        @Override
        public void onAnimationRepeat(Animation animation) {
        }
    };

    // =============================================================================
    // Activity State
    // =============================================================================

    public void onDestroy(){
        stopControllerHiding();
        stopSeekHiding();
        stopVolumeHiding();
        stopBrightHiding();
    }

    public void onUserLeaveHint(){
        stopControllerHiding();
        stopSeekHiding();
        stopVolumeHiding();
        stopBrightHiding();
    }

}
